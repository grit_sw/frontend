import { Typography, FormControlLabel, Switch, ListSubheader } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
// import TextField from "@material-ui/core/TextField";
import { green500 } from "material-ui/styles/colors";
import * as React from "react";
import "./OnBoarding.css";
import Button from "@material-ui/core/Button";
import {
  GetAllBillingMethods,
  GetAllBillingUnits,
  NewFacilities
} from "../../services/api-requests/api-requests";
import MenuItem from "@material-ui/core/MenuItem";
// import countrycitystatejson from "countrycitystatejson";
import ListOfRoles from "../SharedComponent/ListOfRoles/ListOfRoles";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import FacilityAddUserDialog from "../SharedComponent/FacilityAddUserDialog/FacilityAddUserDialog";
// import ListOfRoles from '../ListOfRoles/ListOfRoles';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    width: 500,
    color: green500
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  },
  multilineColor: {
    color: "red"
  },
  formControl: {
    // margin: theme.spacing.unit,
    minWidth: 120,
    color: green500,
    margin: "0 auto 20px",
    width: "80%",
    maxWidth: "500px",
    textAlign: "left" as "left"
  },
  addEmailsButton: {
    backgroundColor: "#9c1212",
    color: "white"
  },
  closeButton: {
    color: "red",
    margin: "1em",
    display: "flex",
    justifyContent: "flex-end"
  }
});

const paperStyle = {
  width: "50%",
  minWidth: "340px",
  height: "auto",
  minHeight: 100,
  marginLeft: "20%",
  marginRight: "10%",
  marginTop: "3%",
  // textAlign: 'left',
  display: "inline-block",
  backgroundColor: "white",
  color: "black",
  padding: "5%",
  align: "center"
};

// const style = {
//   inputField: {
//     margin: "0 auto 20px",
//     width: "80%",
//     maxWidth: "500px",
//     textAlign: "left" as "left"
//   }
// };

interface OnBoardingProps extends WithStyles<typeof styles> { }

interface Istate {
  facilitytypes: string;
  country: string;
  state: string;
  houseno: number;
  city: string;
  street: string;
  name: string;
  billingmethod: string;
  invite: string;
  billingunit: string;
  all_billing_methods: any[];
  all_billing_units: any[];
  others: boolean;
  otherfacility: string;
  selected_role: { id: string; name: string } | null;
  open_add_emails: boolean;
  selected_facility: { facility_id: string; name: string } | null;
  mfa_required: boolean;
  keypad_user: boolean;
  email_list: Array<{ email: string }>;
  createdisabled: boolean
  invitedisabled: boolean
}

class OnBoarding extends React.Component<OnBoardingProps, Istate> {
  constructor(props: OnBoardingProps) {
    super(props);

    this.state = {
      facilitytypes: "",
      country: "",
      state: "",
      houseno: 0,
      city: "",
      street: "",
      name: "",
      billingmethod: "",
      invite: "",
      billingunit: "",
      all_billing_methods: [],
      all_billing_units: [],
      open_add_emails: false,
      others: false,
      otherfacility: "",
      selected_facility: null,
      selected_role: null,
      mfa_required: false,
      keypad_user: false,
      email_list: [],
      createdisabled: true,
      invitedisabled: true
    };
  }

  public componentWillMount() {
    GetAllBillingMethods()
      .then(response => {
        console.log(response);
        let data = response.data.data;
        this.setState({
          all_billing_methods: data
        });
      })
      .catch(err => {
        console.log(err);
      });

    GetAllBillingUnits()
      .then(response => {
        console.log(response);
        let data = response.data.data;
        this.setState({
          all_billing_units: data
        });
      })
      .catch(err => {
        console.log(err);
      });

    // Countries()
    //   .then(response => {
    //     console.log(response);
    //     // let data = response.data.data;
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });

    // console.log(countrycitystatejson.getCountries());
    // console.log(countrycitystatejson.getStatesByShort(this.state.country));
  }

  // tslint:disable-next-line:no-empty
  public handleChange = (name, funcName = () => { }) => event => {
    // @ts-ignore works
    this.setState({ ...this.state, [name]: event.target.value }, () => {
      if (funcName) {
        funcName();
      }
      if (
        this.state.name &&
        this.state.facilitytypes &&
        this.state.country &&
        this.state.state &&
        this.state.billingmethod &&
        this.state.billingunit
      ) {
        this.setState({ createdisabled: false });
      }
      if (
        this.state.name &&
        this.state.facilitytypes &&
        this.state.country &&
        this.state.state &&
        this.state.billingmethod &&
        this.state.billingunit &&
        this.state.selected_role &&
        this.state.email_list
      ) {
        this.setState({ invitedisabled: false, createdisabled: true });
      }
    });
  };

  public setResidetialFacility = () => {
    if (this.state.facilitytypes === "Others") {
      this.setState({ others: true });
    } else {
      this.setState({ others: false });
    }
  };

  public handleRolesChange = (event, index, value) => {
    console.log(value);
    this.setState({
      selected_role: value
    });
  };

  public handleOpenModal = () => {
    this.setState({ open_add_emails: true });
  };

  public handleCloseModal = () => {
    this.setState({ open_add_emails: false });
  };

  public handleMfaChange = (event, isInputChecked: boolean) => {
    this.setState({
      mfa_required: isInputChecked
    });
  };

  public handleKeypadUserChange = (event, isInputChecked: boolean) => {
    this.setState({
      keypad_user: isInputChecked
    });
  };

  public User = useremail => {
    // tslint:disable
    console.log(useremail);
    this.setState({ email_list: useremail });
    console.log(this.state.email_list);
  };

  public handleSubmit = event => {
    event.preventDefault();

    const body = {
      // tslint:disable
      name: this.state.name,
      country: this.state.country,
      state: this.state.state,
      city: this.state.city,
      street: this.state.street,
      house_no: this.state.houseno,
      facility_type: this.state.facilitytypes,
      billing_method: this.state.billingmethod,
      billing_unit: this.state.billingunit,
      invite_clients: this.state.email_list
      // [
      // this.state.invite,
      // this.state.selected_role
      // ]
    };
    console.log(typeof this.state.email_list)
    console.log(body);
    console.log("here");
    // @ts-ignore
    NewFacilities(body)
      .then(res => {
        console.log(res);
        // this.setState({ open1: true });
      })
      .catch(err => {
        console.log(err.response);
        alert("Fuel not created. Please try again!");
      });
  };

  public render() {
    const { classes } = this.props;

    // console.log(countrycitystatejson.getStatesByShort(this.state.country));
    // console.log(
    //   countrycitystatejson.getCities(this.state.country, this.state.state)
    // );

    const BillingMethods = this.state.all_billing_methods.map(
      (BillingMethod, id) => (
        <MenuItem key={id} value={BillingMethod.billing_method_id}>
          {BillingMethod.billing_method_name}
        </MenuItem>
      )
    );

    const BillingUnits = this.state.all_billing_units.map((BillingUnit, id) => (
      <MenuItem key={id} value={BillingUnit.billing_unit_id}>
        {BillingUnit.billing_unit_name}
      </MenuItem>
    ));

    // const Countries = countrycitystatejson.getCountries().map((Country, id) => (
    //   <MenuItem key={id} value={Country.shortName}>
    //     {Country.name}
    //   </MenuItem>
    // ));

    // const coun =
    //   countrycitystatejson.getStatesByShort(this.state.country) || [];

    // const States = coun.map((State, id) => (
    //   <MenuItem key={id} value={State}>
    //     {State}
    //   </MenuItem>
    // ));

    // const cit =
    //   countrycitystatejson.getCities(this.state.country, this.state.state) ||
    //   [];

    // const Cities = cit.map((City, id) => (
    //   <MenuItem key={id} value={City}>
    //     {City}
    //   </MenuItem>
    // ));

    return (
      <div>
        <h2 style={{ textAlign: "center", color: "grey" }}>Create Facilty</h2>
        <div>
          <p style={{ color: "gray", margin: "3%", textAlign: "center" }}>
            <b> Hello, Please fill the form below to create a Facility </b>
          </p>
          {/* <div style={{ display: "flex", width: "100%" }}> */}
          <Paper className={classes.root} elevation={1} style={paperStyle}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                Facility Types
              </InputLabel>
              <Select
                id="Facility Types"
                value={this.state.facilitytypes}
                onChange={this.handleChange(
                  "facilitytypes",
                  this.setResidetialFacility
                )}
                name="facilitytypes"
                className={classes.textField}
                style={{ color: "black" }}
              >
                <MenuItem value="Residential_House">Residential House</MenuItem>
                <MenuItem value="Office">Office</MenuItem>
                <MenuItem value="Shops">Shops</MenuItem>
                <MenuItem value="Mall">Mall</MenuItem>
                <MenuItem value="Others">Others</MenuItem>
              </Select>
            </FormControl>

            {this.state.others ? (
              <TextField
                id="Others"
                label="Others"
                className={classes.textField}
                value={this.state.otherfacility}
                onChange={this.handleChange("otherfacility")}
                margin="normal"
                InputLabelProps={{
                  style: { color: green500 }
                }}
              />
            ) : (
                <div />
              )}

            <div>&nbsp;</div>
            <Divider />
            <div>&nbsp;</div>

            <Typography> Address </Typography>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                Country
              </InputLabel>
              {/* <Select
                name="country"
                // class="countries order-alpha presel-byip "
                id="countryId"
                value={this.state.country}
                onChange={this.handleChange("country")}
                className={classes.textField}
                style={{ color: "black" }}
              >
                {Countries}
              </Select> */}
            </FormControl>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                State
              </InputLabel>
              {/* <Select
                id="State"
                value={this.state.state}
                onChange={this.handleChange("state")}
                name="state"
                className={classes.textField}
                style={{ color: "black" }}
              >
                {States}
              </Select> */}
            </FormControl>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                City
              </InputLabel>
              {/* <Select
                id="City"
                value={this.state.city}
                onChange={this.handleChange("city")}
                name="city"
                className={classes.textField}
                style={{ color: "black" }}
              >
                {Cities}
              </Select> */}
            </FormControl>

            <TextField
              id="Street"
              label="Street"
              className={classes.textField}
              value={this.state.street}
              onChange={this.handleChange("street")}
              margin="normal"
              InputLabelProps={{
                style: { color: green500 }
              }}
            />

            <TextField
              id="House No"
              label="House No"
              type="number"
              className={classes.textField}
              value={this.state.houseno}
              onChange={this.handleChange("houseno")}
              margin="normal"
              InputLabelProps={{
                style: { color: green500 }
              }}
            />

            <div>&nbsp;</div>
            <Divider />
            <div>&nbsp;</div>

            <TextField
              id="Name"
              label="Name"
              className={classes.textField}
              value={this.state.name}
              onChange={this.handleChange("name")}
              margin="normal"
              color="#c35"
              // style={{ color: green500 }}
              InputLabelProps={{
                style: { color: green500 }
              }}
            />

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                Billing Method
              </InputLabel>
              <Select
                id="Billing Method"
                value={this.state.billingmethod}
                onChange={this.handleChange("billingmethod")}
                name="billingmethod"
                className={classes.textField}
                style={{ color: "black" }}
              >
                {BillingMethods}
              </Select>
            </FormControl>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple" className={classes.formControl}>
                Billing Unit
              </InputLabel>
              <Select
                id="hg"
                value={this.state.billingunit}
                onChange={this.handleChange("billingunit")}
                name="billingunit"
                className={classes.textField}
                style={{ color: "black" }}
              >
                {BillingUnits}
              </Select>
            </FormControl>

            <div>&nbsp;</div>
            <Divider />
            <div>&nbsp;</div>

            <ListSubheader className="list-subheader">
              Optional
            </ListSubheader>

            <div
              style={{ textAlign: "center", width: "100%", minWidth: "300px" }}
            >
              <ListOfRoles
                // roles={this.state.roles}
                handleChange={this.handleRolesChange}
                selected_role={this.state.selected_role}
              />
              <p style={{ color: "#808080de", fontSize: "75%" }}>
                Select the role of the users you will be creating
              </p>
            </div>

            <div
              style={{
                margin: "20px",
                width: "auto",
                minWidth: "200px",
                textAlign: "center"
              }}
            >
              <FormControlLabel
                control={<Switch value={this.state.mfa_required} />}
                labelPlacement="start"
                label="Use Multifactor Authentication"
                onChange={this.handleMfaChange}
              />
            </div>

            <div
              style={{
                margin: "20px",
                width: "auto",
                minWidth: "200px",
                textAlign: "center"
              }}
            >
              <FormControlLabel
                control={<Switch value={this.state.keypad_user} />}
                label="Keypad User"
                labelPlacement="start"
                onChange={this.handleKeypadUserChange}
              />
            </div>

            <div>&nbsp;</div>
            <div>&nbsp;</div>

            <div style={{ width: "100%", textAlign: "center" }}>
              <Button
                className={this.props.classes.addEmailsButton}
                onClick={this.handleOpenModal}
              >
                Start adding emails
              </Button>
            </div>

            <Dialog
              fullWidth={true}
              scroll="paper"
              open={this.state.open_add_emails}
            // contentStyle={{ width: '100%', maxWidth: '600px', margin: 'auto' }}
            >
              <DialogTitle>Write out the email Adresses</DialogTitle>
              <DialogContent>
                <div>
                  {this.state.selected_role ? (
                    <FacilityAddUserDialog
                      selected_role_id={this.state.selected_role.id as string}
                      selected_facility_id=""
                      // {
                      //   this.state.selected_facility.facility_id
                      // }
                      mfa_required={this.state.mfa_required}
                      keypad_user={this.state.keypad_user}
                      storeEmails={this.User}

                    />
                  ) : (
                      <div>
                        <p>You have to select a role</p>
                      </div>
                    )}
                </div>

                <Button
                  key="1"
                  onClick={this.handleCloseModal}
                  className={this.props.classes.closeButton}
                >
                  Close
                </Button>
              </DialogContent>
            </Dialog>

            <div style={{ display: 'flex', justifyContent: "center" }}>
              <div style={{ marginRight: '20px' }}>
                <Button variant="contained" style={{ marginTop: "20px" }} onClick={this.handleSubmit} disabled={this.state.createdisabled}>
                  Create
              </Button>
              </div>

              <div>
                <Button variant="contained" style={{ marginTop: "20px" }} onClick={this.handleSubmit} disabled={this.state.invitedisabled}>
                  Invite
              </Button>
              </div>
            </div>
          </Paper>
          <div>&nbsp;</div>
          {/* </div> */}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(OnBoarding);
