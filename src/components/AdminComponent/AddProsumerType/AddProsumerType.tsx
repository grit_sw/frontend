import * as React from "react";
import "./AddProsumerType.css";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import RaisedButton from "material-ui/RaisedButton";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import AddCircle from "material-ui-icons/AddCircle"
import { green500 } from "material-ui/styles/colors"
import Dialog from "material-ui/Dialog";
import AddProsumerClass from "../AddProsumerClass/AddProsumerClass";
import { CreateNewProsumerType, GetProsumerClasses } from "../../../services/api-requests/api-requests";

const styles = {
    formFieldStyles: {
        display: "block",
        width: "90%",
        maxWidth: "300px",
        margin: "0 auto"
    },
    underlineStyles: {
        borderColor: green500
    },
    labelStyles: {
        color: green500
    }
}

interface AddProsumerTypeProps {
    open: boolean
    toggleModal: any
}
interface AddProsumerTypeState {
    new_type: string
    class: { prosumer_class_name: string, prosumer_class_id: string }
    prosumer_classes: Array<any>
    open_add_class: boolean;
}
class AddProsumerType extends React.Component<AddProsumerTypeProps, AddProsumerTypeState> {

    constructor(props: AddProsumerTypeProps) {
        super(props);
        this.state = {
            new_type: "",
            class: { prosumer_class_name: "", prosumer_class_id: "" },
            prosumer_classes: [],
            open_add_class: false
        }
    }

    componentWillMount() {
        this.getProsumerClasses();
    }

    toggleAddClassModal = () => {
        this.setState({
            open_add_class: !this.state.open_add_class
        }, () => {
            this.getProsumerClasses();
        })
    }

    getProsumerClasses = () => {
        GetProsumerClasses()
            .then((response) => {
                console.log(response);
                let data = response.data;
                this.setState({
                    prosumer_classes: data.data
                })
            }, err => {
                console.log(err.response);
            })

    }

    handleNameChange = (event, value) => {
        this.setState({
            new_type: value
        })
    }

    handleClassChange = (event, index, value) => {
        if (value) {
            this.setState({
                class: value
            }, () => {
                console.log(this.state.class)
            })
    
        } else {
            this.toggleAddClassModal();
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let body = {
            name: this.state.new_type,
            class_id: this.state.class.prosumer_class_id
        }

        CreateNewProsumerType(body)
            .then((response) => {
                console.log(response);
                this.props.toggleModal();
            }, err => {
                console.log(err);
            })
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
            >
                <h3> Create New Meter Type </h3>
                <ValidatorForm
                    onSubmit={this.handleSubmit}
                >
                    <TextValidator
                        className="form-input"
                        floatingLabelText="Name"
                        inputStyle={{ color: 'black' }}
                        name="new-meter-type"
                        value={this.state.new_type}
                        validators={["required"]}
                        errorMessages={["This field is required"]}
                        onChange={this.handleNameChange}
                        style={styles.formFieldStyles}
                        floatingLabelFocusStyle={styles.labelStyles}
                        underlineStyle={styles.underlineStyles}
                        underlineFocusStyle={styles.underlineStyles}
                    />

                    <SelectField
                        onChange={this.handleClassChange}
                        value={this.state.class}
                        floatingLabelText="Choose Meter Class"
                        style={styles.formFieldStyles}
                        underlineStyle={styles.underlineStyles}
                        underlineFocusStyle={styles.underlineStyles}
                    >
                        {
                            this.state.prosumer_classes.map((classes, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        value={classes}
                                        primaryText={classes.prosumer_class_name}
                                    />
                                );
                            })
                        }

                        <MenuItem>
                            <div
                                style={{ width: "2%", margin: "0 auto", paddingTop: "3.5%" }}
                            >
                                <AddCircle />
                            </div>

                        </MenuItem>

                    </SelectField>

                    <div className="right-button">
                        <RaisedButton
                            type="submit"
                            labelColor="#3CB878"
                            label="Create"
                            style={{ margin: "0.5%", boxShadow: "0" }}
                        />

                        <RaisedButton
                            labelColor="#3CB878"
                            label="Cancel"
                            onClick={this.props.toggleModal}
                            style={{ margin: "0.5%", boxShadow: "0" }}
                        />
                    </div>

                    {
                        (this.state.open_add_class) ?
                            <AddProsumerClass
                                open={this.state.open_add_class}
                                toggleModal={this.toggleAddClassModal}
                            /> : <div />
                    }

                </ValidatorForm>
            </Dialog>
        );
    }

}

export default AddProsumerType;
