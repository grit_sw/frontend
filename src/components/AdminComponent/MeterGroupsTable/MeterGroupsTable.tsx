import * as React from "react";
import "./MeterGroupsTable.css";
import { Table, TableHeader, TableHeaderColumn, TableRow, TableBody, TableRowColumn } from "material-ui/Table";

const styles = {
    formFieldStyles: {
        display: "block",
        width: "100%",
        maxWidth: "800px",
    },
    underlineStyles: {
        borderColor: "#3CB878"
    },
    LabelStyles: {
        color: "#3CB878"
    },
    modalButtonStyles: {
        textAlign: "right",
        marginTop: "3%"
    },
    addClientAdmin: {
        border: "1px solid #3CB878",
        color: "rgb(99, 90, 90)",
        borderRadius: "5px",
    }
}

interface MeterGroupsTableProps {
    prosumer_groups: Array<any>
    facility: any
    onRowClick: any
    handleAddAdminClick: any
}
interface MeterGroupsTableState {
}

class MeterGroupsTable extends React.Component<MeterGroupsTableProps, MeterGroupsTableState> {

    render() {
        return (
            <div className="table">
                <Table
                    onRowHover={this.props.onRowClick}
                >
                    <TableHeader displaySelectAll={false}>

                        <TableRow>
                            <TableHeaderColumn colSpan={3} >
                                <h2 className="table-header">
                                    FACILITY: {this.props.facility.facility_name}
                                </h2>
                            </TableHeaderColumn>
                        </TableRow>

                    </TableHeader>

                    <TableBody
                        displayRowCheckbox={false}
                        showRowHover={true}
                    >
                        {
                            this.props.prosumer_groups.map((groups, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        displayBorder={true}
                                        hoverable={true}
                                    >
                                        <TableRowColumn style={{ width: "5%" }}>
                                            {index + 1}
                                        </TableRowColumn>

                                        <TableRowColumn colSpan={2}>
                                            {groups.prosumer_group_name}
                                        </TableRowColumn>

                                        <TableRowColumn colSpan={2} >
                                            <button 
                                                style={styles.addClientAdmin}
                                                onClick={this.props.handleAddAdminClick}
                                            >
                                                <span style={{ fontSize: "1.2em" }}> + </span>
                                                Client Admin
                                            </button>
                                        </TableRowColumn>

                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

}

export default MeterGroupsTable;