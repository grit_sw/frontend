import * as React from 'react';
import './CreateEnergyPlan.css';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { green500, blue500 } from 'material-ui/styles/colors';
// import RaisedButton from 'material-ui/RaisedButton';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { RaisedButton } from 'material-ui';
import { PostCreateNewPlans } from '../../../services/api-requests/api-requests';
import ListOfFacilities from '../../SharedComponent/ListOfFacilities/ListOfFacilities';

const paperStyle = {
    width: '80%',
    minWidth: '340px',
    height: 'auto',
    minHeight: 100,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '3%',
    // textAlign: 'left',
    display: 'inline-block',
    backgroundColor: 'white',
    color: 'black',
    padding: '5%'
};

const styles = {
    errorStyle: {
        color : green500,
    },
    underlineStyle: {
        borderColor : green500,
    },
    floatingLabelStyle: {
        color : green500,
    },
    toggle: {
        marginBottom : 16,
    },
    floatingLabelFocusStyle: {
        color : blue500,
    },
};

interface CreateEnergyPlanProps {
}
interface CreateEnergyPlanState {
    name: string;
    selected_facility: { facility_id: string; name: string } | null;
    rate: string;
    power_limit: string;
    energy_limit: string;
}

class CreateEnergyPlanPlan extends React.Component<CreateEnergyPlanProps, CreateEnergyPlanState> {
    constructor(props: CreateEnergyPlanProps) {
        super(props);
        this.state = {
            name: '',
            rate: '',
            power_limit: '',
            energy_limit: '',
            selected_facility: null,

        };
    }

    handleNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({ name: e.currentTarget.value });
    }

    handleRateChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({ rate: e.currentTarget.value });
    }

    handlePowerLimitChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({ power_limit: e.currentTarget.value });
    }

    handleEnergyLimitChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({ energy_limit: e.currentTarget.value });
    }

    submitPlan = () => {
        if (this.state.selected_facility
            && this.state.selected_facility.facility_id
            && this.state.name && this.state.rate
            && this.state.power_limit
            && this.state.energy_limit
        ) {
            const body = {
                name: this.state.name,
                rate: this.state.rate,
                power_limit: this.state.power_limit,
                energy_limit: this.state.energy_limit,
                facility_id: this.state.selected_facility.facility_id
            };
            PostCreateNewPlans(body)
                .then((response) => {
                    console.log(response);
                    let data = response['data'];
                    if (data.success && data.data) {
                        console.log(response);
                        this.setState({
                            name: '',
                            power_limit: '',
                            energy_limit: '',
                            rate: '',
                            selected_facility: null
                        }, () => {
                            alert("Plan Created successfully")
                        })
                    }
                }, err => {
                    console.log(err);
                });
        }
    }

    handleFacilityChange = (event, index, value) => {
        console.log(value);
        this.setState({
            selected_facility: value
        });
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <div>
                    <div>
                        <h2 style={{ textAlign: 'center', color: 'grey' }}>Create Energy Plan</h2>
                        <div>
                            <p style={{ color: 'gray', margin: '3%', textAlign: 'center' }}>
                                <b> Hello, Please fill the form below to create an energy plan </b>
                            </p>
                            <div style={{ display: 'flex', width: '100%' }}>
                                <Paper style={paperStyle} zDepth={2}>
                                    <TextField
                                        floatingLabelStyle={styles.floatingLabelStyle}
                                        floatingLabelShrinkStyle={styles.floatingLabelFocusStyle}
                                        underlineStyle={styles.underlineStyle}
                                        floatingLabelText="Name"
                                        inputStyle={{ color: 'black' }}
                                        className="text-input"
                                        onChange={this.handleNameChange}
                                    />
                                    <TextField
                                        floatingLabelStyle={styles.floatingLabelStyle}
                                        floatingLabelShrinkStyle={styles.floatingLabelFocusStyle}
                                        underlineStyle={styles.underlineStyle}
                                        floatingLabelText="Rate"
                                        inputStyle={{ color: 'black' }}
                                        className="text-input"
                                        type="number"
                                        onChange={this.handleRateChange}
                                    />
                                    <TextField
                                        floatingLabelStyle={styles.floatingLabelStyle}
                                        floatingLabelShrinkStyle={styles.floatingLabelFocusStyle}
                                        underlineStyle={styles.underlineStyle}
                                        floatingLabelText="Power Limit"
                                        inputStyle={{ color: 'black' }}
                                        className="text-input"
                                        type="number"
                                        onChange={this.handlePowerLimitChange}
                                    />
                                    <TextField
                                        floatingLabelStyle={styles.floatingLabelStyle}
                                        floatingLabelShrinkStyle={styles.floatingLabelFocusStyle}
                                        underlineStyle={styles.underlineStyle}
                                        floatingLabelText="Energy Limit"
                                        inputStyle={{ color: 'black' }}
                                        className="text-input"
                                        type="number"
                                        onChange={this.handleEnergyLimitChange}
                                    />
                                    <div style={{ textAlign: 'center' }}>
                                        <ListOfFacilities
                                            handleChange={this.handleFacilityChange}
                                            selected_facility={this.state.selected_facility}
                                        />
                                    </div>
                                    <br />
                                    <div style={{ textAlign: 'center' }}>
                                        <RaisedButton
                                            backgroundColor={green500}
                                            labelColor="white"
                                            label="Submit"
                                            onClick={this.submitPlan}
                                        />
                                    </div>
                                </Paper>
                                {/* </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default CreateEnergyPlanPlan;