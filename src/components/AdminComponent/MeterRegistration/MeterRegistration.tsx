import * as React from "react";
import "./MeterRegistration.css";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import RaisedButton from "material-ui/RaisedButton";
import { green500 } from "material-ui/styles/colors"
import ListOfFacilities from "../../SharedComponent/ListOfFacilities/ListOfFacilities";
import ListOfUsers from "../../SharedComponent/ListOfUsers/ListOfUsers";
import ListOfProsumerTypes from "../../SharedComponent/ListOfProsumerTypes/ListOfProsumerTypes";
import ListOfProsumerGroups from "../../SharedComponent/ListOfProsumerGroups/ListOfProsumerGroups";
import ListOfMeters from "../../SharedComponent/ListOfMeters/ListOfMeters";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from "@material-ui/core/Paper";
import {
    GetUsersofRoleinFacility,
    GetProsumersTypes,
    GetProsumerGroups,
    registerNewProsumer,
    GetMetersByType,
    GetSubnetsByFacility
} from "../../../services/api-requests/api-requests";

const styles = {
    tabColor: '#4caf50',
    formFieldStyles: {
        // display: "block",
        width: "50%",
        // maxWidth: "800px",
    },
    underlineStyles: {
        borderColor: green500
    },
    labelStyles: {
        color: green500
    },
    cardStyles: {
        padding: "6%",
        width: "80%",
        maxWidth: "800px",
        boxShadow: "0 6px 20px 0 rgba(0, 0, 0, 0.19)",
        margin: "20px auto",
        // textAlign: "left" as "left",
        textAlign: "center"
    } as React.CSSProperties
}

interface MeterRegistrationProps {
}
interface MeterRegistrationState {
    meter_name: string
    selected_user: any
    selected_prosumer_type: any
    selected_prosumer_group: any
    selected_meter: any
    selected_facility: { facility_id: string, name: string }
    users: Array<any>
    prosumer_types: Array<any>
    meters: Array<any>
    prosumer_groups: Array<any>
    submit_disabled: boolean
    open_modal: boolean
    user_request_error: boolean
    meter_request_error: boolean
    prosumer_type_request_error: boolean
    prosumer_group_request_error: boolean
    subnet_ids: Array<string>
    show_device_id: boolean
    device_id: string
    selected_tab: number
}

class MeterRegistration extends React.Component<MeterRegistrationProps, MeterRegistrationState> {
    constructor(props: MeterRegistrationProps) {
        super(props)
        this.state = {
            meter_name: "",
            selected_user: {},
            selected_prosumer_type: {},
            selected_prosumer_group: {},
            selected_meter: {},
            users: [],
            prosumer_types: [],
            meters: [],
            prosumer_groups: [],
            selected_facility: { facility_id: "", name: "" },
            submit_disabled: true,
            open_modal: false,
            user_request_error: false,
            meter_request_error: false,
            prosumer_type_request_error: false,
            prosumer_group_request_error: false,
            subnet_ids: [],
            show_device_id: false,
            device_id: "",
            selected_tab: 0
        }
    }

    componentWillMount() {
        this.GetRoleTwoUsers();
        this.GetAllProsumerTypes();
    }

    GetRoleTwoUsers = () => {
        // let facility_id = this.props.facility_id;
        GetUsersofRoleinFacility("2", this.state.selected_facility.facility_id)
            .then(response => {
                let data = response["data"];
                if (data.success && data.data) {
                    console.log("USERS by role in facility");
                    console.log(data);
                    this.setState({
                        users: data.data
                    });
                } else {
                    this.setState({
                        user_request_error: true
                    });
                }
                console.log(response);
            })
            .catch(err => {
                console.log('ROLE TWO USERS ERROR: ', err.response);
            });
    };

    GetAllProsumerTypes = () => {
        GetProsumersTypes()
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log(data);
                    this.setState({
                        prosumer_types: data.data,
                    });
                } else {
                    this.setState({
                        prosumer_type_request_error: true
                    });
                }
                console.log(response);
            }, err => {
                console.log(err.response);
                this.setState({
                    prosumer_type_request_error: true
                });
            });
    }

    GetAllMetersByType = (prosumerTypeUrlName) => {
        GetMetersByType(prosumerTypeUrlName)
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log('meter: ', data);
                    this.setState({
                        meters: data.data,
                    });
                } else {
                    this.setState({
                        meter_request_error: true
                    });
                }
                console.log(response);
            }, err => {
                console.log(err.response);
                this.setState({
                    meter_request_error: true
                });
            });
    }

    GetAllProsumerGroupsInFacility = () => {
        console.log("Getting prosumer groups with facility:");
        console.log(this.state.selected_facility);
        GetProsumerGroups(this.state.selected_facility.facility_id)
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log(data);
                    this.setState({
                        prosumer_groups: data.data,
                    });
                } else {
                    this.setState({
                        prosumer_group_request_error: true
                    });
                }
                console.log(response);
            }, err => {
                this.setState({
                    prosumer_group_request_error: true
                });
                console.log(err.response);
            });
    }

    GetAllSubnetsByFacility = (facilityId) => {
        GetSubnetsByFacility(facilityId)
            .then(response => {
                let data = response.data;
                if (data.success && data.data) {
                    let subnets = data.data;
                    let subnetIds = subnets.map((subnet: any) => subnet.subnet_id);
                    this.setState({ subnet_ids: subnetIds });
                }
            })
            .catch(err => { console.log('subnet error: ', err) })
    }

    createNewProsumer = () => {
        let newProsumerData = {
            "prosumer_name": this.state.meter_name,
            "user_group_id": this.state.selected_user.group_id,
            "device_id":
                this.state.selected_tab === 0 ?
                    this.state.selected_meter.meter_id
                    : this.state.device_id,
            "prosumer_type_id": this.state.selected_prosumer_type.prosumer_type_id,
            "facility_id": this.state.selected_facility.facility_id,
            "prosumer_group_id": this.state.selected_prosumer_group.group_id,
            "subnet_ids": this.state.subnet_ids,
            "prosumer_url": this.state.selected_meter.meter_url,
            "probe_names": this.state.selected_meter.probe_names
        }
        registerNewProsumer(newProsumerData)
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    alert("Meter registered successfully.")
                }
            }, err => {
                console.log(err.response);
                alert("Meter not registered successfully.")
            }
            );
    }

    enableSubmit = () => {
        console.log(this.state.selected_tab === 0 ? this.state.selected_meter.meter_id : this.state.device_id)
        console.log(this.state.meter_name);
        console.log(this.state.selected_facility.facility_id);
        console.log(this.state.selected_user.group_id);
        console.log(this.state.selected_prosumer_type.prosumer_type_id);
        console.log(this.state.selected_prosumer_group.group_id);

        if (
            this.state.selected_tab === 0 &&
            this.state.meter_name &&
            this.state.selected_facility.facility_id &&
            this.state.selected_user.group_id &&
            this.state.selected_prosumer_type.prosumer_type_id &&
            this.state.selected_prosumer_group.group_id &&
            this.state.selected_meter.meter_id
        ) {
            this.setState({
                submit_disabled: false
            });
        } else if (
            this.state.selected_tab === 1 &&
            this.state.device_id &&
            this.state.meter_name &&
            this.state.selected_facility.facility_id &&
            this.state.selected_user.group_id &&
            this.state.selected_prosumer_type.prosumer_type_id &&
            this.state.selected_prosumer_group.group_id
        ) {
            this.setState({
                submit_disabled: false
            });
        }

    }

    toggleModal = () => {
        this.setState({
            open_modal: !this.state.open_modal
        })
    }

    handleTabSelect = (event, value) => {
        console.log(value);
        this.setState({ selected_tab: value })
    }

    handleDeviceIdChange = (event, value) => {
        this.setState({
            device_id: value
        }, () => {
            this.enableSubmit();
        })
    }

    handleMeterNameChange = (event, value) => {
        this.setState({
            meter_name: value
        }, () => {
            this.enableSubmit();
        })
    }

    handleUserChange = (event, index, value) => {
        // Make debouncing query here
        let user = value;
        this.setState({
            selected_user: user
        }, () => {
            this.enableSubmit();
        })
    }

    handleFacilityChange = (event, index, value) => {
        this.setState({
            selected_facility: value
        }, () => {
            this.GetAllProsumerGroupsInFacility();
            this.GetRoleTwoUsers();
            this.GetAllSubnetsByFacility(value.facility_id);
        })
    }

    handleProsumerTypeChange = (event, index, value) => {
        this.setState({
            selected_prosumer_type: value,
            meters: []
        }, () => {
            this.GetAllMetersByType(value.prosumer_type_url_name);
        })
    }

    handleMeterChange = (event, index, value) => {
        this.setState({
            selected_meter: value
        }, () => {
            this.enableSubmit();
        })
    }

    handleProsumerGroupChange = (event, index, value) => {
        this.setState({
            selected_prosumer_group: value
        }, () => {
            this.enableSubmit();
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.createNewProsumer();
        this.setState({
            submit_disabled: true
        })

    }

    render() {
        const { selected_tab } = this.state;

        const thirdPartyMeter = (
            <ValidatorForm
                onSubmit={this.handleSubmit}
            >
                <div>
                    <TextValidator
                        className="form-input"
                        floatingLabelText="Device Id"
                        inputStyle={{ color: 'black' }}
                        name="device-id"
                        value={this.state.device_id}
                        validators={["required"]}
                        errorMessages={["This field is required"]}
                        onChange={this.handleDeviceIdChange}
                        style={styles.formFieldStyles}
                        floatingLabelFocusStyle={styles.labelStyles}
                        underlineStyle={styles.underlineStyles}
                        underlineFocusStyle={styles.underlineStyles}
                    />
                </div>

                <div>
                    <TextValidator
                        className="form-input"
                        floatingLabelText="Meter Name"
                        inputStyle={{ color: 'black' }}
                        name="meter-name"
                        value={this.state.meter_name}
                        validators={["required"]}
                        errorMessages={["This field is required"]}
                        onChange={this.handleMeterNameChange}
                        style={styles.formFieldStyles}
                        floatingLabelFocusStyle={styles.labelStyles}
                        underlineStyle={styles.underlineStyles}
                        underlineFocusStyle={styles.underlineStyles}
                    />
                </div>

                <ListOfFacilities
                    handleChange={this.handleFacilityChange}
                    selected_facility={this.state.selected_facility}
                />

                <ListOfProsumerTypes
                    prosumer_types={this.state.prosumer_types}
                    handleChange={this.handleProsumerTypeChange}
                    selected_prosumer_type={this.state.selected_prosumer_type}
                />

                <ListOfUsers
                    handleChange={this.handleUserChange}
                    users={this.state.users}
                    selected_user={this.state.selected_user}
                />

                <ListOfProsumerGroups
                    prosumer_groups={this.state.prosumer_groups}
                    handleChange={this.handleProsumerGroupChange}
                    selected_prosumer_group={this.state.selected_prosumer_group}
                />

                <div className="center-button">
                    <RaisedButton
                        type="submit"
                        backgroundColor={green500}
                        labelColor="#ffffff"
                        label="Add Meters"
                        disabled={this.state.submit_disabled}
                    />
                </div>
            </ValidatorForm>
        )

        const GO = (
            <ValidatorForm
                onSubmit={this.handleSubmit}
            >
                <TextValidator
                    className="form-input"
                    floatingLabelText="Meter Name"
                    inputStyle={{ color: 'black' }}
                    name="meter-name"
                    value={this.state.meter_name}
                    validators={["required"]}
                    errorMessages={["This field is required"]}
                    onChange={this.handleMeterNameChange}
                    style={styles.formFieldStyles}
                    floatingLabelFocusStyle={styles.labelStyles}
                    underlineStyle={styles.underlineStyles}
                    underlineFocusStyle={styles.underlineStyles}
                />

                <ListOfFacilities
                    handleChange={this.handleFacilityChange}
                    selected_facility={this.state.selected_facility}
                />

                <ListOfProsumerTypes
                    prosumer_types={this.state.prosumer_types}
                    handleChange={this.handleProsumerTypeChange}
                    selected_prosumer_type={this.state.selected_prosumer_type}
                />

                <ListOfUsers
                    handleChange={this.handleUserChange}
                    users={this.state.users}
                    selected_user={this.state.selected_user}
                />

                <ListOfProsumerGroups
                    prosumer_groups={this.state.prosumer_groups}
                    handleChange={this.handleProsumerGroupChange}
                    selected_prosumer_group={this.state.selected_prosumer_group}
                />

                <ListOfMeters
                    meters={this.state.meters}
                    handleChange={this.handleMeterChange}
                    selected_meter={this.state.selected_meter}
                />

                <div className="center-button">
                    <RaisedButton
                        type="submit"
                        backgroundColor={green500}
                        labelColor="#ffffff"
                        label="Add Meters"
                        disabled={this.state.submit_disabled}
                    />
                </div>
            </ValidatorForm>
        )

        const formToRender = this.state.selected_tab === 0 ? GO : thirdPartyMeter;

        return (
            <div className="meter-registration">

                <div
                    className="card"
                    style={styles.cardStyles}
                >
                    <div className="heading">
                        <h1> Meter Registration </h1>
                        <h3> Please fill the form below to register a GRIT meter </h3>
                    </div>
                    <Paper>
                        <Tabs
                            value={selected_tab}
                            onChange={this.handleTabSelect}
                            fullWidth={true}
                            indicatorColor="primary"
                            textColor="primary"
                        >
                            <Tab label="G0" />
                            <Tab label="Third Party Meter" />
                        </Tabs>
                    </Paper>
                    {formToRender}
                </div>
            </div>
        );
    }
}

export default MeterRegistration;