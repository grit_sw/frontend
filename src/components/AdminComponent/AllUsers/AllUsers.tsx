import { Tab, Tabs } from 'material-ui/Tabs';
import { blue500, orange500 } from 'material-ui/styles/colors';
import * as React from 'react';
import SwipeableViews from 'react-swipeable-views';
import AllUsersTable from '../AllUsersTable/AllUsersTable';
import './AllUsers.css';
import { RadioButtonGroup, RadioButton, RaisedButton } from 'material-ui';
import { GetAllUsers, GetAllUsersInFacility, GetAllUsersInRoles } from '../../../services/api-requests/api-requests';
import ListOfRoles from '../../SharedComponent/ListOfRoles/ListOfRoles';
import ListOfFacilities from '../../SharedComponent/ListOfFacilities/ListOfFacilities';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400 as 400
  },
  slide: {
    padding: 10,
    minHeight: '200px'
  },
  underlineStyle: {
    borderColor: orange500
  },
  floatingLabelStyle: {
    color: orange500
  },
  groupInput: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap' as 'wrap',
    justifyContent: 'center',
    textAlign: 'center' as 'center'
  },
  floatingLabelFocusStyle: {
    color: blue500
  },
  radioButton: {
    marginBottom: 16,
  },
  radioLabel: {
    color: 'grey'
  }
};

interface AllUsersProps { }
interface AllUsersState {
  tabSlideIndex: number;
  roles: Array<any>;
  // facilities: Array<any>;
  selected_facility: { facility_id: string; name: string } | null;
  selected_role: { id: string; name: string } | null;
  error_loading_facilities: boolean;
  pagination: number;
  user_criteria: 'all' | 'facility' | 'role';
  error_loading_users: Boolean;
  users: Array<any>;
  user_has_previous: boolean;
  user_has_next: boolean;
}

class AllUsers extends React.Component<AllUsersProps, AllUsersState> {
  constructor(props: AllUsersProps) {
    super(props);
    this.state = {
      tabSlideIndex: 0,
      roles: [],
      // facilities: [],
      selected_facility: null,
      selected_role: null,
      error_loading_facilities: false,
      pagination: 1,
      user_criteria: 'all',
      error_loading_users: false,
      users: [],
      user_has_next: false,
      user_has_previous: false
    };
  }

  handleTabChange = value => {
    this.setState({
      tabSlideIndex: value
    });
  }

  handleFacilityChange = (event, index, value) => {
    console.log(value);
    this.setState({
      selected_facility: value
    }, () => {
      this.getUser();
    });
  }

  handleRolesChange = (event, index, value) => {
    console.log(value);
    this.setState({
      selected_role: value
    }, () => {
      this.getUser();
    });
  }

  handleUserCriteriaChange = (event, value) => {
    console.log(value);
    this.setState({
      user_criteria: value
    }, () => {
      if (this.state.user_criteria === 'all') {
        this.getUser();
      }
    });
  }

  componentDidMount() {
    this.getUser();
  }

  getUser = () => {
    let req
    if (this.state.user_criteria === 'all') {
      req = GetAllUsers()
    } else if ((this.state.user_criteria === 'facility') && this.state.selected_facility) {
      req = GetAllUsersInFacility(this.state.selected_facility.facility_id);
    } else if ((this.state.user_criteria === 'role') && this.state.selected_role) {
      req = GetAllUsersInRoles(this.state.selected_role.id)
    } else {
      req = GetAllUsers();
    }

    req.then((response) => {
      console.log(response);
      let data = response['data'];
      if (data.success && data.data) {
        console.log(data);
        this.setState({
          users: data.data,
          user_has_previous: data.has_prev,
          user_has_next: data.has_next
        });
      } else {
        this.setState({
          error_loading_users: true
        });
      }
      console.log(response);
    }, err => {
      console.log(err);
    });
  }

  render() {
    return (
      <div>
        <br />
        <br />
        <div className="userInviteDetails">
          <div style={styles.groupInput}>
            <div style={{ margin: '20px', width: '85%', minWidth: '300px' }}>
              <div>
                <RadioButtonGroup onChange={this.handleUserCriteriaChange} name="user_criteria" defaultSelected="all">
                  <RadioButton
                    value="all"
                    label="Show All"
                    style={styles.radioButton}
                    labelStyle={styles.radioLabel}
                  />
                  <RadioButton
                    value="facility"
                    label="Facility"
                    style={styles.radioButton}
                    labelStyle={styles.radioLabel}
                  />
                  <RadioButton
                    value="role"
                    label="Role"
                    style={styles.radioButton}
                    labelStyle={styles.radioLabel}
                  />
                </RadioButtonGroup>

                <Criteria
                  handleFaclityChange={this.handleFacilityChange}
                  handleRolesChange={this.handleRolesChange}
                  user_criteria={this.state.user_criteria}
                  selected_facility={this.state.selected_facility}
                  selected_role={this.state.selected_role}
                />

              </div>
            </div>
          </div>
        </div>

        <Tabs
          onChange={this.handleTabChange}
          value={this.state.tabSlideIndex}
          // tabItemContainerStyle={{ backgroundColor: '#808080' }}
          inkBarStyle={{ backgroundColor: '#bb0c0c' }}
          // inkBarStyle={{ backgroundColor: 'white' }}
          tabItemContainerStyle={{ backgroundColor: 'white' }}
        >
          <Tab label="All Users" style={{ color: '#bb0c0c' }} value={0} />
          <Tab label="Invited Users" style={{ color: '#bb0c0c' }} value={1} />
        </Tabs>

        <SwipeableViews
          index={this.state.tabSlideIndex}
          onChangeIndex={this.handleTabChange}
        >
          <div style={styles.slide}>
            <AllUsersTable
              users={this.state.users}
            />
            <div style={{ border: '1px solid #8080807a', textAlign: 'center', margin: '10px' }}>
              <RaisedButton disabled={!(this.state.user_has_previous)}>
                Previous
              </RaisedButton>
              <RaisedButton disabled={!(this.state.user_has_next)}>
                Next
              </RaisedButton>
            </div>
          </div>
          <div style={styles.slide}>
            <AllUsersTable
              users={this.state.users}
            />
            <div style={{ border: '1px solid #8080807a', textAlign: 'center', margin: '10px' }}>
              <RaisedButton disabled={!(this.state.user_has_previous)}>
                Previous
              </RaisedButton>
              <RaisedButton disabled={!(this.state.user_has_next)}>
                Next
              </RaisedButton>
            </div>
          </div>
        </SwipeableViews>
      </div>
    );
  }
}

interface CriteriaProps {
  handleFaclityChange: any;
  handleRolesChange: any;
  user_criteria: string;
  selected_facility: { facility_id: string, name: string } | null;
  selected_role: { id: string, name: string } | null;
}
function Criteria(props: CriteriaProps) {
  let crit = (
    <div />
  );

  if (props.user_criteria === 'facility') {
    crit = (
      <ListOfFacilities
        handleChange={props.handleFaclityChange}
        selected_facility={props.selected_facility}
      />
    );
  } else if (props.user_criteria === 'role') {
    crit = (
      <ListOfRoles
        handleChange={props.handleRolesChange}
        selected_role={props.selected_role}
      />
    );
  }
  return crit;
}

export default AllUsers;
