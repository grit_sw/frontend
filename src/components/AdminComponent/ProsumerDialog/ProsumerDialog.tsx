import { Dialog, FlatButton } from "material-ui";
import * as React from "react";
import { 
  DeleteProsumerRequest, ProsumerTurnOffRequest, ProsumerTurnOnRequest 
} from "../../../services/api-requests/api-requests";
import ProsumerDetailsTable from "../ProsumerTable/ProsumerTable";

interface FullScreenDialogProps {
  classes?: any;
  prosumer: any;
  open: boolean;
  closeFunc: any;
}
interface FullScreenDialogState { }

class FullScreenDialog extends React.Component<
  FullScreenDialogProps,
  FullScreenDialogState
  > {

  deleteProsumer = () => {
    DeleteProsumerRequest(this.props.prosumer.prosumer_name, this.props.prosumer.facility_id)
      .then((axiosResponse) => {
        console.log(axiosResponse)
        alert('Prosumer Deleted')
        this.props.closeFunc()
      }, err => {
        console.log(err)
      })
  }

  turnOffProsumer = () => {
    ProsumerTurnOffRequest(this.props.prosumer.prosumer_name)
      .then((axiosResponse) => {
        console.log(axiosResponse)
        alert('Prosumer Deleted')
        this.props.closeFunc()
      }, err => {
        console.log(err)
      })
  }

  turnOnProsumer = () => {
    ProsumerTurnOnRequest(this.props.prosumer.prosumer_name)
      .then((axiosResponse) => {
        console.log(axiosResponse)
        alert('Prosumer Deleted')
        this.props.closeFunc()
      }, err => {
        console.log(err)
      })
  }

  render() {
    let prosumer = this.props.prosumer;

    const dialogActions = [
      (
        <FlatButton
          label="Delete prosumer"
          primary={true}
          key="1"
          onClick={this.deleteProsumer}
        />
      ),
      (
        <FlatButton
          label="Switch ON"
          primary={true}
          key="1"
          onClick={this.turnOnProsumer}
        />
      ),
      (
        <FlatButton
          label="Switch Off"
          primary={true}
          key="1"
          onClick={this.turnOffProsumer}
        />
      ),
      (
        <FlatButton
          label="Cancel"
          primary={true}
          key="1"
          onClick={this.props.closeFunc}
        />
      )
    ];

    return (
      <div>
        <Dialog
          // title="Prosumer Details"
          actions={dialogActions}
          modal={true}
          autoScrollBodyContent={true}
          contentStyle={{ width: "100%", maxWidth: "600px", margin: "auto" }}
          // onRequestClose={this.handleClose}
          open={this.props.open}
        //   onClose={this.handleClose}
        //   TransitionComponent={Transition}
        >
          <ProsumerDetailsTable prosumer={prosumer} />
        </Dialog>
      </div>
    );
  }
}

export default FullScreenDialog;
