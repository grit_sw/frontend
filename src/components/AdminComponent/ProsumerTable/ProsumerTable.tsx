import * as React from "react";
import * as moment from "moment";
import { ListItem, List } from "material-ui";

const Style = {
  data: {
    label: {
      marginRight: "10px"
    }
  }
};

interface ProsumerDetailsProps {
  classes?: any;
  prosumer: any;
}
interface ProsumerDetailsState {
  // data: any[];
}
class ProsumerDetailsTable extends React.Component<
  ProsumerDetailsProps,
  ProsumerDetailsState
  > {
  constructor(props: ProsumerDetailsProps) {
    super(props);
    // let prosumer = this.props.prosumer;
    // this.state = {
    //   data: [
    //     createData("Facility Name", prosumer.facility_name),
    //     createData("Prosumer Name", prosumer.prosumer_name),
    //     createData("Assigned User", prosumer.prosumer_user),
    //     createData("Prosumer State", prosumer.prosumer_state),
    //     createData("State Duration", moment(prosumer.last_message).fromNow()),
    //     createData("Power Consumed", prosumer.measured_power),
    //     createData("Energy Today", prosumer.energy_today),
    //     createData("Last Energy", prosumer.measured_energy),
    //     createData("Supply Duration Today", prosumer.time_today)
    //   ]
    // };
  }

  render() {
    return (
      <div>
        <List>
          <ListItem>
            <p>
              <span style={Style.data.label}>Facility Name : </span>
              <b>{this.props.prosumer.facility_name}</b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Prosumer Name : </span>
              <b>{this.props.prosumer.prosumer_name}</b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Prosumer User : </span>
              <b> {this.props.prosumer.prosumer_user} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Prosumer State : </span>
              <b> {this.props.prosumer.prosumer_state} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>State Duration : </span>
              <b>  {moment(this.props.prosumer.last_message).fromNow()} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Power Consumed : </span>
              <b> {this.props.prosumer.measured_power} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Energy Today : </span>
              <b> {this.props.prosumer.energy_today} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Last Energy : </span>
              <b> {this.props.prosumer.measured_energy} </b>
            </p>
          </ListItem>
          <ListItem>
            <p>
              <span style={Style.data.label}>Supply Duration Time : </span>
              <b>  {this.props.prosumer.time_today} </b>
            </p>
          </ListItem>
        </List>
      </div>
    );
  }
}

export default ProsumerDetailsTable;
