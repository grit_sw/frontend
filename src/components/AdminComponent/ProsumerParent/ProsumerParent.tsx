import * as React from "react";
import axios from "axios";
import * as openSocket from "socket.io-client";
import "./ProsumerParent.css";
import ProsumerChild from "../ProsumerChild/ProsumerChild";
import { CircularProgress } from "material-ui";
const socket = openSocket("https://dos.grit.systems:5501/");

const USER_TOKEN = localStorage.getItem("user_token");

let config = {
  baseURL: "https://dos.grit.systems:5500",
  timeout: 5000,
  headers: {
    "X-API-KEY": USER_TOKEN
  }
};

interface ProsumerParentProps { }
interface ProsumerParentState {
  prosumers: any[];
  prosumer_loading: boolean
}

class ProsumerParent extends React.Component<
  ProsumerParentProps,
  ProsumerParentState
  > {
  constructor(props: ProsumerParentProps) {
    super(props);
    this.state = {
      prosumers: [],
      prosumer_loading: false
    };
    this.registerUser = this.registerUser.bind(this);
    this.updateProsumerState = this.updateProsumerState.bind(this);
    this.getMyFacilities = this.getMyFacilities.bind(this);
  }

  componentDidMount() {
    console.log("Connecting ...");
    this.setState({
      prosumer_loading: true
    })
    axios
      .get("/cmdctrl/all/", config)
      .then(response => {
        // TODO: what happens when no prosumer is obtained?????????
        console.log(response.data.data);
        this.setState({
          prosumers: response.data.data,
          prosumer_loading: false
        });
        this.socketSetUp(); // TODO: Only create socket connection when prosumers have been obtained.
      })
      .catch(error => {
        console.log(error); this.setState({
          prosumer_loading: false
        })
      });
  }

  registerUser() {
    console.log("Registering User");
    axios
      .get("/auth/whoami/", config)
      .then(response => {
        socket.emit("register", { data: response.data });
      })
      .catch(error => console.log(error));
  }

  getMyFacilities() {
    console.log("Getting user's facilities.");
    axios
      .get("/facility/me/", config)
      .then(response => {
        socket.emit("my_facility", { data: response.data.data });
      })
      .catch(error => console.log(error));
  }

  updateProsumerState(response: any) {
    // let prosumers = Object.values(this.state.prosumers);
    let prosumers = this.state.prosumers;
    let prosumerName = response.message.prosumer_name;
    let prosumerIndex = prosumers.findIndex(
      obj => obj.prosumer_name === prosumerName
    );
    let updatedProsumers = [
      ...prosumers.slice(0, prosumerIndex),
      response.message,
      ...prosumers.slice(prosumerIndex + 1, prosumers.length)
    ];
    this.setState(
      {
        prosumers: updatedProsumers
      },
      () => {
        console.log(this.state.prosumers);
      }
    );
  }

  createNewProsumer(response: any) {
    let prosumers = this.state.prosumers;
    let newProsumer = response.message;
    prosumers.push(newProsumer);
    this.setState(
      {
        prosumers: prosumers
      },
      () => {
        console.log(this.state.prosumers);
      }
    );
  }

  socketSetUp() {
    console.log("Sending Message to backend");
    socket.emit("handshake_init");
    socket.on("init_registration", () => this.registerUser());
    socket.on("get_facilities", () => this.getMyFacilities());
    socket.on("prosumer_update", response =>
      this.updateProsumerState(response)
    );
    socket.on("new_prosumer", response => this.createNewProsumer(response));
    socket.on("connection_response", msg => {
      console.log(msg);
    });
  }

  render() {
    return (
      <div style={{ display: "flex", flexWrap: "wrap", width: "100%", justifyContent: 'center' }}>
        {!this.state.prosumer_loading ? this.state.prosumers.map(value => (
          <div style={{ margin: "0px 25px 5px 10px", width: '250px' }} key={value.prosumer_name}>
            <ProsumerChild
              key={value.prosumer_name}
              prosumer={value}
            />
          </div>
        )) : <div style={{ margin: '100px', width: '100%', textAlign: 'center' }}><CircularProgress /></div>}
      </div>
    );
  }
}

// ProsumerParent.propTypes = {
//   classes: PropTypes.object.isRequired
// };

// export default withStyles(styles)(ProsumerParent);
export default ProsumerParent;
