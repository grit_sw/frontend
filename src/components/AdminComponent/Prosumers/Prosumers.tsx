import * as React from "react";

import "./Prosumers.css";
import ProsumerParent from "../ProsumerParent/ProsumerParent";

class Prosumers extends React.Component {

  render() {
    return (
      <div className="prosumer-body">
        <div>
          <br />
          <ProsumerParent />
        </div>
      </div>
    );
  }
}

export default Prosumers;
