import * as React from "react";
import "./AddMetersDialog.css";
import { GetProsumersTypes, CreateNewProsumers, GetProsumerGroups } from "../../../services/api-requests/api-requests";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import Button from "@material-ui/core/Button";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import AddCircle from "@material-ui/icons/AddCircle";
import { green, blue } from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import AddProsumerType from "../AddProsumerType/AddProsumerType"
import AddMeterGroup from "../AddMeterGroup/AddMeterGroup"

const styles = {
    formFieldStyles: {
        display: "block",
        width: "90%",
        maxWidth: "300px",
        margin: "0 auto"
    },
    underlineStyles: {
        borderColor: green[500]
    },
    labelStyles: {
        color: green[500]
    },
    cardStyles: {
        padding: "6%",
        width: "80%",
        maxWidth: "800px",
        boxShadow: "0 6px 20px 0 rgba(0, 0, 0, 0.19)",
        margin: "20px auto",
        textAlign: "left" as "left"
    },
    eachMeterGroup: {
        backgroundColor: "#eed",
        width: "100%",
        margin: "2% auto",
        paddingBottom: "10px"
    },
    modalTitle: {
        textAlign: "center" as "center",
        color: "rgb(111, 103, 103)"
    },
    buttonStyles: {
        margin: "0.5%",
        boxShadow: "none",
        color: "#3CB878",
        backgroundColor: "#fff"
    },
    addNewButton: {
        backgroundColor: green[500],
        color: "#ffffff",
        textAlign: "center" as "center"
    }
}

interface AddMetersDialogProps extends WithStyles<typeof styles> {
    open: boolean
    probe_id: string
    selected_facility: { facility_id: string, name: string }
    toggleModal: any
}
interface AddMetersDialogState {
    meter_list: Array<{ name: string, type_id: string, group_id: string }>
    prosumer_types: Array<any>
    prosumer_groups: Array<any>
    selected_types: Array<{ prosumer_type_name: string, prosumer_type_id: string }>
    selected_groups: Array<{ prosumer_group_name: string, prosumer_group_id: string }>
    add_type_modal: boolean,
    add_group_modal: boolean,
    disable_add_new: boolean,
    submit_disabled: boolean
}

class AddMetersDialog extends React.Component<AddMetersDialogProps, AddMetersDialogState> {
    constructor(props: AddMetersDialogProps) {
        super(props)
        this.state = {
            meter_list: [{ name: "", type_id: "", group_id: "" }],
            prosumer_types: [],
            prosumer_groups: [],
            selected_types: [],
            selected_groups: [],
            add_type_modal: false,
            add_group_modal: false,
            disable_add_new: false,
            // disable_add_new: true,
            submit_disabled: true,
        }
    }

    componentWillMount() {
        this.getProsumerTypes();
        this.getProsumerGroups();
    }

    enableAddNew = () => {

        var filled = true; // used var to declare so I could edit it in the loop
        for (let index = 0; index < this.state.meter_list.length; index++) {

            if (
                !(this.state.meter_list[index].name) ||
                !(this.state.meter_list[index].type_id) ||
                !(this.state.meter_list[index].group_id)
            ) {
                filled = false;
                this.setState({
                    disable_add_new: true
                })
                break;
            }
        }
        if (filled) {
            this.setState({
                disable_add_new: false
            })
        }

    }

    enableSubmit = () => {
        if (
            this.state.meter_list &&
            this.props.probe_id &&
            this.props.selected_facility.facility_id
        ) {
            this.setState({
                submit_disabled: false
            })
        }
    }

    toggleAddTypeModal = () => {
        this.setState({
            add_type_modal: !this.state.add_type_modal
        }, () => {
            this.getProsumerTypes();
        })
    }

    toggleAddGroupModal = () => {
        this.setState({
            add_group_modal: !this.state.add_group_modal
        }, () => {
            this.getProsumerGroups();
        })
    }

    addNewMeter = () => {
        console.log(this.state.meter_list);

        this.setState({
            meter_list: this.state.meter_list.concat({ name: "", type_id: "", group_id: "" }),
            selected_types: this.state.selected_types.concat({ prosumer_type_name: "", prosumer_type_id: "" }),
            selected_groups: this.state.selected_groups.concat({ prosumer_group_name: "", prosumer_group_id: "" })
        }, () => {
            this.enableAddNew();
        })

    }

    handleNameChange = (index: number, event: React.FormEvent<{}>, value: string) => {
        let meter_list = this.state.meter_list;
        meter_list[index].name = value;
        this.setState({
            meter_list: meter_list
        }, () => {
            this.enableSubmit();
            this.enableAddNew();
        });
    }

    getProsumerTypes = () => {
        GetProsumersTypes()
            .then((response) => {
                console.log(response);
                let data = response.data
                this.setState({
                    prosumer_types: data.data
                })
            }, err => {
                console.log(err);
            })
    }

    getProsumerGroups = () => {
        GetProsumerGroups(this.props.selected_facility.facility_id)
            .then((response) => {
                console.log(response);
                let data = response.data
                this.setState({
                    prosumer_groups: data.data
                })
            }, err => {
                console.log(err);
            })
    }

    handleTypeChange = (index: number, event: React.SyntheticEvent<{}>, typeIndex: number, value: any) => {

        let selected_types = this.state.selected_types;
        selected_types[index] = value;

        if (value) {
            let meter_list = this.state.meter_list;
            meter_list[index].type_id = value.prosumer_type_id;

            this.setState({
                selected_types: selected_types,
                meter_list: meter_list
            }, () => {
                this.enableSubmit();
                this.enableAddNew();
            })
        } else {
            this.toggleAddTypeModal();
        }
    }

    handleGroupChange = (index: number, event: React.SyntheticEvent<{}>, groupIndex: number, value: any) => {

        let selected_groups = this.state.selected_groups;
        selected_groups[index] = value

        if (value) {
            let meter_list = this.state.meter_list;
            meter_list[index].group_id = value.prosumer_group_id;

            this.setState({
                selected_groups: selected_groups,
                meter_list: meter_list
            }, () => {
                this.enableSubmit();
                this.enableAddNew();
                console.log(this.state.selected_groups)
            })
        } else {
            this.toggleAddGroupModal();
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            submit_disabled: true
        })

        let body = {
            prosumers: this.state.meter_list,
            probeId: this.props.probe_id,
            facilityId: this.props.selected_facility.facility_id
        };
        CreateNewProsumers(body.prosumers, body.probeId, body.facilityId)
            .then((response) => {
                console.log(response);
                alert("Meter Registered Successfully!");
            }, err => {
                console.log(err);
                alert("Error Registering Meter")
            })
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                fullWidth={true}
                className={"add-meters modal"}
            >
                <DialogTitle className={this.props.classes.modalTitle}>
                    Please enter each meter name, type and group
                </DialogTitle>

                <DialogContent>
                    <ValidatorForm
                        onSubmit={this.handleSubmit}
                    >
                        {
                            this.state.meter_list.map((meter, index) => {
                                return (
                                    <div
                                        key={index}
                                        style={styles.eachMeterGroup}
                                    >

                                        <TextValidator
                                            className="form-input"
                                            floatingLabelText="Meter Name"
                                            inputStyle={{ color: 'black' }}
                                            name="meter-name"
                                            value={meter.name}
                                            validators={["required"]}
                                            errorMessages={["This field is required"]}
                                            onChange={this.handleNameChange.bind(this, index)}
                                            style={styles.formFieldStyles}
                                            floatingLabelFocusStyle={styles.labelStyles}
                                            underlineStyle={styles.underlineStyles}
                                            underlineFocusStyle={styles.underlineStyles}
                                        />

                                        <SelectField
                                            onChange={this.handleTypeChange.bind(this, index)}
                                            value={this.state.selected_types[index]}
                                            floatingLabelText="Choose Meter Type"
                                            style={styles.formFieldStyles}
                                            underlineStyle={styles.underlineStyles}
                                            underlineFocusStyle={styles.underlineStyles}
                                            selectedMenuItemStyle={{ color: blue[900] }}
                                        >
                                            {
                                                this.state.prosumer_types.map((type, typeIndex) => {
                                                    return (
                                                        <MenuItem
                                                            key={typeIndex}
                                                            value={type}
                                                            primaryText={type.prosumer_type_name}
                                                        />
                                                    );
                                                })
                                            }

                                            <MenuItem>
                                                <div
                                                    style={{ width: "2%", margin: "0 auto", paddingTop: "3.5%" }}
                                                >
                                                    <AddCircle />
                                                </div>

                                            </MenuItem>

                                        </SelectField>

                                        <SelectField
                                            onChange={this.handleGroupChange.bind(this, index)}
                                            value={this.state.selected_groups[index]}
                                            floatingLabelText="Choose Meter Group"
                                            style={styles.formFieldStyles}
                                            underlineStyle={styles.underlineStyles}
                                            underlineFocusStyle={styles.underlineStyles}
                                            selectedMenuItemStyle={{ color: blue[900] }}
                                        >
                                            {
                                                this.state.prosumer_groups.map((group, groupIndex) => {
                                                    return (
                                                        <MenuItem
                                                            key={groupIndex}
                                                            value={group}
                                                            primaryText={group.prosumer_group_name}
                                                        />
                                                    );
                                                })
                                            }

                                            <MenuItem>
                                                <div
                                                    style={{ width: "2%", margin: "0 auto", paddingTop: "3.5%" }}
                                                >
                                                    <AddCircle />
                                                </div>

                                            </MenuItem>

                                        </SelectField>

                                    </div>
                                );
                            })
                        }

                        <div className="center-button">
                            <Button
                                variant="contained"
                                size="small"
                                className={this.props.classes.addNewButton}
                                onClick={this.addNewMeter}
                                disabled={this.state.disable_add_new}
                            >
                                Add New Meter
                            </Button>

                        </div>

                        <DialogActions className="action-buttons">
                            <Button
                                type="submit"
                                className={this.props.classes.buttonStyles}
                            >
                                Submit
                            </Button>

                            <Button
                                className={this.props.classes.buttonStyles}
                                onClick={this.props.toggleModal}
                            >
                                Cancel
                            </Button>
                        </DialogActions>

                        <AddProsumerType
                            open={this.state.add_type_modal}
                            toggleModal={this.toggleAddTypeModal}
                        />

                        {
                            (this.state.add_group_modal) ?
                                <AddMeterGroup
                                    open={this.state.add_group_modal}
                                    facility={this.props.selected_facility}
                                    toggleModal={this.toggleAddGroupModal}
                                /> : <div />
                        }

                    </ValidatorForm>

                </DialogContent>

            </Dialog>
        );
    }
}

export default withStyles(styles)(AddMetersDialog);