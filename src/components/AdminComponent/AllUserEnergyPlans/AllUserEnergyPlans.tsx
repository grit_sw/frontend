import * as React from 'react';
import Divider from 'material-ui/Divider';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import './AllUserEnergyPlans.css';
import { GetAllUsersEnergyPlans } from '../../../services/api-requests/api-requests';
import { RaisedButton } from 'material-ui';

interface AllUserEnergyPlansProps {

}
interface AllUserEnergyPlansState {
    plans: any;
}

class AllUserEnergyPlans extends React.Component<AllUserEnergyPlansProps, AllUserEnergyPlansState> {
    constructor(props: AllUserEnergyPlansProps) {
        super(props);
        this.state = {
            plans: null,
        };
    }

    componentWillMount() {
        this.getPlansList()
    }

    getPlansList = () => {
        GetAllUsersEnergyPlans()
            .then(
                res => {
                    console.log(res);
                    if (res['data']) {
                        this.setState({
                            plans: res['data'].data
                        }, () => {
                            console.log(this.state.plans)
                        })
                    }
                }
            );
    }

    render() {

        return (
            <div className="planRoute">
                <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {(this.state.plans) ?
                            this.state.plans.map((plan) => {
                                return (
                                    <Card key={plan.id} className="plan">
                                        <CardHeader
                                            title={plan.name}
                                        />
                                        <Divider />
                                        <CardText>
                                            <div className="feature-list">
                                                <ul>
                                                    <li>facility: {plan.facility}</li>
                                                    <li> power limit: {plan.power_limit}</li>
                                                    <li> energy limit: {plan.energy_limit}</li>
                                                    <li>rate: {plan.rate}</li>
                                                </ul>
                                            </div>
                                            <RaisedButton className="plan-button" label="Edit" />
                                        </CardText>
                                    </Card>
                                )
                            }) : <div />
                        }
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default AllUserEnergyPlans;