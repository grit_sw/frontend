import * as React from "react";
import * as moment from "moment";
import ProsumerDialog from "../ProsumerDialog/ProsumerDialog";
import "./ProsumerChild.css";
import { prosumerStatusColor } from "./prosumerStatusColor";

interface ProsumerChildProps {
  prosumer: any;
}
interface ProsumerChildState {
  lastMessage: String;
  openModal: boolean;
}

class ProsumerChild extends React.Component<
  ProsumerChildProps,
  ProsumerChildState
  > {
  timerID: any;
  constructor(props: ProsumerChildProps) {
    super(props);
    let prosumer = this.props.prosumer;
    this.state = {
      lastMessage: prosumer.last_message,
      openModal: false
    };
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    let prosumer = this.props.prosumer;
    let lastMessage = prosumer.last_message;
    this.setState({
      lastMessage: moment(lastMessage).fromNow()
    });
  }

  handleClickOpen() {
    this.setState({
      openModal: true
    });
  }

  handleClose() {
    this.setState({
      openModal: false
    });
  }

  render() {
    return (
      <div>
        {/* <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            width: "100%",
            border: "1px solid #b19ab5",
            borderRadius: "3px",
            minHeight: "200px",
            padding: "5px",
            color: "#3e3c3c",
            boxShadow: '1px 1px 1px #d0c7c7',
            cursor: 'pointer'
          }}
          onClick={this.handleClickOpen}
        >
          <div className="child-row1">
            <p className="child-feature-label">Prosumer User</p>
            {this.props.prosumer.prosumer_user}
          </div>
          <div className="child-row1">
            <p className="child-feature-label">Facility name</p>
            {this.props.prosumer.facility_name}
          </div>
          <div className="child-row2">
            <p className="child-feature-label">Prosumer name</p>
            <b style={{ color: '#2d2c2c' }}> {this.props.prosumer.prosumer_name}</b>
          </div>
          <div className="child-row1">
            <p className="child-feature-label">Prosumer state</p>
            {this.props.prosumer.prosumer_state}
          </div>
          <div className="child-row1">
            <p className="child-feature-label">last message</p>
            {this.state.lastMessage}
          </div>
        </div> */}
        <div
          style={{
            width: "100%",
            border: "1px solid #b19ab5",
            borderRadius: "3px",
            padding: "10px",
            color: "#3e3c3c",
            boxShadow: '1px 1px 1px #d0c7c7',
            cursor: 'pointer',
            backgroundColor: prosumerStatusColor[this.props.prosumer.prosumer_state]
          }}
          onClick={this.handleClickOpen}
        >
          <div>
            <b style={{ color: '#2d2c2c' }}> {this.props.prosumer.prosumer_name}</b>
            <p>
              {this.state.lastMessage}
            </p>
          </div>
        </div>
        <ProsumerDialog
          prosumer={this.props.prosumer}
          open={this.state.openModal}
          closeFunc={this.handleClose}
        />
      </div>
    );
  }
}

export default ProsumerChild;
