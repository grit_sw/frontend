export const prosumerStatusColor = {
    'METER ON': 'green',
    'BUFFERING': 'green',
    'OFFLINE': 'red',
    'METER OFF': 'black',
    'PROBE OFF': 'black',
    'NEW METER': '',
    'UNASSIGNED': '',
    'NEW USER': '',
    'POWER LIMIT EXCEEDED': 'purple',
    'ACCOUNT CREDIT EXHAUSTED': 'purple',
    'NEW CLIENT ADMIN': ''
}