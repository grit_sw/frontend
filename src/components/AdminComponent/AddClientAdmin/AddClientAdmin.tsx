import * as React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { GetUsersofRoleinFacility, AddClientAdminToProsumerGroup } from "../../../services/api-requests/api-requests";
import "./AddClientAdmin.css";

const styles = {
  modalTitle: {
    textAlign: "center" as "center"
  },
  closeButton: {
    color: "red",
    fontWeight: 700 as 700,
    margin: "1%",
    boxShadow: "0"
  },
  cardStyles: {
    border: `1.2px solid #999`,
    width: "80%",
    maxWidth: "200px",
    margin: "1% auto"
  }
}

interface AddClientAdminProps extends WithStyles<typeof styles> {
  facility: { facility_id: string, name: string }
  open: boolean
  selected_group: {
    prosumer_group_name: string,
    prosumer_group_id: string
  }
  toggleModal: any
}
interface AddClientAdminState {
  client_admins: Array<any>;
  selected_user: any;
  role_id: string;
}

class AddClientAdmin extends React.Component<AddClientAdminProps, AddClientAdminState> {
  constructor(props: AddClientAdminProps) {
    super(props);
    this.state = {
      client_admins: [],
      selected_user: null,
      role_id: "3"
    };
  }

  componentWillMount() {
    this.getClientAdmins();
  }

  addUser = selectedUser => {
    this.setState({
      selected_user: selectedUser
    }, () => {

      let body = {
        admin_facility: this.state.selected_user.facilities[0].facility_id,
        group_facility: this.props.facility.facility_id,
        client_admin_id: this.state.selected_user.user_id,
        prosumer_group_id: this.props.selected_group.prosumer_group_id
      }
      console.log(body);
      if (body.admin_facility === body.group_facility) {
        AddClientAdminToProsumerGroup(body)
          .then((response) => {

            console.log(response);
            alert("Added");

          }, err => {
            console.log(err.response);
            if (err.response.status === 409) {
              alert("Already been added");
            }
          })
      } else {
        alert("The two facilities need to be the same")
      }

    });
  };

  getClientAdmins = () => {

    GetUsersofRoleinFacility(this.state.role_id, this.props.facility.facility_id)
      .then((response) => {
        let data = response.data
        console.log(response.data.data);

        this.setState({
          client_admins: data.data
        })

      }, err => {
        console.log(err);
      })
  }

  ClientAdminCard = (props: { client_admin: any }) => {

    return (
      <div style={styles.cardStyles} className="client-admin-card">
        <h4> {props.client_admin.first_name + " " + props.client_admin.last_name} </h4>
        <p> {props.client_admin.email} </p>
        <p> {props.client_admin.phone_number}</p>

        <Button
          style={{ border: "1px solid grey", margin: "5px" }}
          onClick={this.addUser.bind(this, props.client_admin)}
          variant="flat"
        >
          Add
        </Button>
      </div>
    );
  }

  render() {
    return (

      <Dialog
        open={this.props.open}
        className="add-client-admin modal"
        scroll="paper"
        fullWidth={true}
      >
        <DialogTitle className={this.props.classes.modalTitle}> 
          ADD CLIENT ADMIN TO {(this.props.selected_group.prosumer_group_name).toUpperCase()} 
        </DialogTitle>

        <DialogContent className="cards-container">
          {
            this.state.client_admins.map((clientAdmin, index) => {
              return (
                <this.ClientAdminCard
                  key={index}
                  client_admin={clientAdmin}
                />
              );
            })
          }
        </DialogContent>

        <DialogActions className="close-button">
          <Button
            classes={{ root: this.props.classes.closeButton }}
            onClick={this.props.toggleModal}
            variant="flat"
          >
            Close
          </Button>
        </DialogActions>

      </Dialog>

    );
  }
}

export default withStyles(styles)(AddClientAdmin);
