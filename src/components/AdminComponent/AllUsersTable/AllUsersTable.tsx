import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import * as React from 'react';
import './AllUsersTable.css';

interface AllUsersTableProps {
    users: Array<any>;
}

interface AllUsersTableState {
    pagination: number;
    users: Array<any>;
    error_loading_users: Boolean;
}

class AllUsersTable extends React.Component<AllUsersTableProps, AllUsersTableState> {
    constructor(props: AllUsersTableProps) {
        super(props);
        this.state = {
            pagination: 1,
            users: [],
            error_loading_users: false
        };
    }

    render() {
        console.log(this.props.users);
        return (
            <div>
                <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>No</TableHeaderColumn>
                            <TableHeaderColumn>First Name</TableHeaderColumn>
                            <TableHeaderColumn>Last Name</TableHeaderColumn>
                            <TableHeaderColumn>Role</TableHeaderColumn>
                            <TableHeaderColumn>Email</TableHeaderColumn>
                            <TableHeaderColumn>Invite Approved</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false} >
                        {
                            this.props.users.map((user, index) => {
                                return (
                                    <TableRow key={user.email}>
                                        <TableRowColumn>{index}</TableRowColumn>
                                        <TableRowColumn>{user.first_name}</TableRowColumn>
                                        <TableRowColumn>{user.last_name}</TableRowColumn>
                                        <TableRowColumn>{user.role_name}</TableRowColumn>
                                        <TableRowColumn>{user.email}</TableRowColumn>
                                        <TableRowColumn>{(user.invite_clicked) ? 'true' : 'false'}</TableRowColumn>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default AllUsersTable;