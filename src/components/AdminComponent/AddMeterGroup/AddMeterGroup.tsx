import * as React from "react";
import "./AddMeterGroup.css";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { CreateNewProsumerGroup } from "../../../services/api-requests/api-requests";

const styles = {
    formFieldStyles: {
        display: "block",
        width: "100%",
        maxWidth: "800px",
    },
    underlineStyles: {
        borderColor: "#3CB878"
    },
    LabelStyles: {
        color: "#3CB878"
    },
    modalTitle: {
        textAlign: "center" as "center",
        color: "rgb(111, 103, 103)"
    },
    buttonStyles: {
        margin: "0.5%",
        boxShadow: "none",
        color: "#3CB878",
        backgroundColor: "#fff"
    }
}

interface AddMeterGroupProps extends WithStyles<typeof styles> {
    open: boolean
    facility: any
    toggleModal: any
}
interface AddMeterGroupState {
    new_group: string
}
class AddMeterGroup extends React.Component<AddMeterGroupProps, AddMeterGroupState> {

    constructor(props: AddMeterGroupProps) {
        super(props);
        this.state = {
            new_group: ""
        }
    }

    handleChange = (event, value) => {
        this.setState({
            new_group: value
        })
    }

    handleSubmit = () => {
        let body = {
            name: this.state.new_group,
            facility_id: this.props.facility.facility_id
        }

        CreateNewProsumerGroup(body)
            .then((response) => {

                console.log(response);
                this.props.toggleModal();
                alert("Added Successfully");

            }, err => {
                console.log(err);
                alert("Error adding group");
            })
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                scroll="paper"
                fullWidth={true}
                className="add-meter-group modal"
            >
                {
                    (this.props.facility.facility_name) ?
                        <div>
                            <DialogTitle className={this.props.classes.modalTitle}>
                                Add New Meter Group to {this.props.facility.facility_name}
                            </DialogTitle>

                            <DialogContent>
                                <ValidatorForm
                                    onSubmit={this.handleSubmit}
                                >
                                    <TextValidator
                                        className="form-input"
                                        floatingLabelText="Input New Meter Group"
                                        inputStyle={{ color: 'black' }}
                                        name="new-meter-group"
                                        value={this.state.new_group}
                                        validators={["required"]}
                                        errorMessages={["This field is required"]}
                                        onChange={this.handleChange}
                                        style={styles.formFieldStyles}
                                        floatingLabelFocusStyle={styles.LabelStyles}
                                        underlineStyle={styles.underlineStyles}
                                        underlineFocusStyle={styles.underlineStyles}
                                    />

                                    <DialogActions className="action-buttons">
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            className={this.props.classes.buttonStyles}
                                        >
                                            Add
                                        </Button>

                                        <Button
                                            variant="contained"
                                            className={this.props.classes.buttonStyles}
                                            onClick={this.props.toggleModal}
                                        >
                                            Cancel
                                        </Button>
                                    </DialogActions>

                                </ValidatorForm>
                            </DialogContent>

                        </div> :
                        <div>
                            <p className="error-text"> Please choose a facility to add the group to first.</p>

                            <div className="action-buttons">
                                <Button
                                    variant="contained"
                                    className={this.props.classes.buttonStyles}
                                    onClick={this.props.toggleModal}
                                >
                                    Close
                                </Button>
                            </div>
                        </div>

                }

            </Dialog>
        );
    }
}

export default withStyles(styles)(AddMeterGroup);