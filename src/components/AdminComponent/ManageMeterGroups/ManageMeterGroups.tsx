import * as React from "react";
import "./ManageMeterGroups.css";
import RaisedButton from "material-ui/RaisedButton";
import { GetProsumerGroups } from "../../../services/api-requests/api-requests";
import AddNewMeterGroup from "../AddMeterGroup/AddMeterGroup"
import AddClientAdmin from '../AddClientAdmin/AddClientAdmin';
import MeterGroupsTable from "../MeterGroupsTable/MeterGroupsTable"
import ListOfFacilities from "../../SharedComponent/ListOfFacilities/ListOfFacilities";

interface ManageMeterGroupsProps {
}
interface ManageMeterGroupsState {
    selected_facility: { facility_id: string, name: string };
    prosumer_groups: Array<any>
    error_message: string
    open_add_new: boolean
    open_add_admin: boolean
    new_group: string
    selected_group: {
        prosumer_group_name: string,
        prosumer_group_id: string
    }
}

class ManageMeterGroups extends React.Component<ManageMeterGroupsProps, ManageMeterGroupsState> {
    constructor(props: ManageMeterGroupsProps) {
        super(props);
        this.state = {
            selected_facility: { facility_id: "", name: "" },
            prosumer_groups: [],
            error_message: "",
            open_add_new: false,
            open_add_admin: false,
            new_group: "",
            selected_group: {
                prosumer_group_name: "",
                prosumer_group_id: ""
            }
        }
    }

    ToggleAddNew = () => {
        this.setState({
            open_add_new: !this.state.open_add_new
        }, () => {
            this.getProsumerGroups();
        });
    }

    ToggleAddAdmin = () => {
        this.setState({
            open_add_admin: !this.state.open_add_admin
        });
    }

    handleFacilityChange = (event, index, value) => {
        this.setState({
            selected_facility: value
        }, () => {
            this.getProsumerGroups();
        })

    }

    handleRowClick = (rowNo) => {
        this.setState({
            selected_group: this.state.prosumer_groups[rowNo]
        }, () => {
            console.log(this.state.selected_group);
        })
    }

    handleAddAdminClick = () => {
        this.setState({
            open_add_admin: true
        })
    }

    getProsumerGroups = () => {

        GetProsumerGroups(this.state.selected_facility.facility_id)
            .then((response) => {

                console.log(response);
                let data = response.data;
                this.setState({
                    prosumer_groups: data.data
                }, () => {
                    console.log(this.state.prosumer_groups);
                })

            }, err => {
                console.log(err.response);
                this.setState({
                    prosumer_groups: []
                })
                if (err.response.status === 404) {
                    this.setState({
                        error_message: "No meter groups have been added to this facility."
                    })
                } else {
                    this.setState({
                        error_message: "Something went wrong"
                    })
                }
            });
    }

    render() {
        return (
            <div className="body">
                <div className="heading">
                    <h1> Manage Meter Groups </h1>
                </div>

                <div className="content">
                    <div className="add-new">
                        <RaisedButton
                            className="button"
                            labelColor="#ffffff"
                            backgroundColor="#3CB878"
                            label="New Group"
                            // style={{ width: "30%" }}
                            onClick={this.ToggleAddNew}
                        />
                    </div>

                    <AddNewMeterGroup
                        open={this.state.open_add_new}
                        facility={this.state.selected_facility}
                        toggleModal={this.ToggleAddNew}
                    />

                    <ListOfFacilities
                        handleChange={this.handleFacilityChange}
                        selected_facility={this.state.selected_facility}
                    />

                    {
                        (this.state.prosumer_groups.length) ?

                            <MeterGroupsTable
                                prosumer_groups={this.state.prosumer_groups}
                                facility={this.state.selected_facility}
                                onRowClick={this.handleRowClick}
                                handleAddAdminClick={this.handleAddAdminClick}
                            /> :

                            <p className="error-text"> {this.state.error_message}</p>
                    }

                    {
                        (this.state.open_add_admin) ?
                            <AddClientAdmin
                                facility={this.state.selected_facility}
                                open={this.state.open_add_admin}
                                selected_group={this.state.selected_group}
                                toggleModal={this.ToggleAddAdmin}
                            /> : <div />

                    }

                </div>

            </div>

        );
    }
}

export default ManageMeterGroups;