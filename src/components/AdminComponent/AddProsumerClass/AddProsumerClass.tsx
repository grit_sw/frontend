import * as React from "react";
import "./AddProsumerClass.css";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { green500 } from "material-ui/styles/colors"
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { CreateNewProsumerClass } from "../../../services/api-requests/api-requests";

const styles = {
    formFieldStyles: {
        display: "block",
        width: "90%",
        maxWidth: "300px",
        margin: "0 auto"
    },
    underlineStyles: {
        borderColor: green500
    },
    labelStyles: {
        color: green500
    },
    modalTitle: {
        textAlign: "center" as "center",
        color: "rgb(111, 103, 103)"
    },
    buttonStyles: {
        margin: "0.5%",
        boxShadow: "none",
        color: "#3CB878",
        backgroundColor: "#fff"
    }
}

interface AddProsumerClassProps extends WithStyles<typeof styles> {
    open: boolean
    toggleModal: any
}
interface AddProsumerClassState {
    name: string
}
class AddProsumerClass extends React.Component<AddProsumerClassProps, AddProsumerClassState> {

    constructor(props: AddProsumerClassProps) {
        super(props);
        this.state = {
            name: ""
        }
    }

    handleNameChange = (event, value) => {
        this.setState({
            name: value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let body = {
            name: this.state.name
        }

        CreateNewProsumerClass(body)
            .then((response) => {
                console.log(response);
                this.props.toggleModal();
            }, err => {
                console.log(err);
            })
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                scroll="paper"
                fullWidth={true}
                className="add-prosumer-class modal"
            >
                <DialogTitle className={this.props.classes.modalTitle}>
                    Create New Meter Class
                </DialogTitle>

                <DialogContent>
                    <ValidatorForm
                        onSubmit={this.handleSubmit}
                    >
                        <TextValidator
                            className="form-input"
                            floatingLabelText="Name"
                            inputStyle={{ color: 'black' }}
                            name="new-meter-class"
                            value={this.state.name}
                            validators={["required"]}
                            errorMessages={["This field is required"]}
                            onChange={this.handleNameChange}
                            style={styles.formFieldStyles}
                            floatingLabelFocusStyle={styles.labelStyles}
                            underlineStyle={styles.underlineStyles}
                            underlineFocusStyle={styles.underlineStyles}
                        />

                        <DialogActions className="action-buttons">
                            <Button
                                variant="contained"
                                className={this.props.classes.buttonStyles}
                                onClick={this.props.toggleModal}
                            >
                                Create
                            </Button>

                            <Button
                                variant="contained"
                                className={this.props.classes.buttonStyles}
                                onClick={this.props.toggleModal}
                            >
                                Cancel
                            </Button>
                        </DialogActions>

                    </ValidatorForm>
                </DialogContent>

            </Dialog>
        );
    }

}

export default withStyles(styles)(AddProsumerClass);
