import * as React from "react";
import MenuItem from "material-ui/MenuItem";
import Avatar from "material-ui/Avatar";
import List from "material-ui/List/List";
import RaisedButton from "material-ui/RaisedButton";
import ListItem from "material-ui/List/ListItem";
import HomeIcon from "material-ui-icons/Home";
import HistoryIcon from "material-ui-icons/History";
import InfoIcon from "material-ui-icons/Info";
import PersonAddIcon from "material-ui-icons/PersonAdd";
import PeopleIcon from "material-ui-icons/People";
import PaymentIcon from "material-ui-icons/Payment";
import AccountBalanceIcon from "material-ui-icons/AccountBalance";
import Divider from "material-ui/Divider";
import { Link } from "react-router-dom";
import "./ManagementSidebar.css";

const sidebarSubtitleStyle = {
  margin: "20px",
  color: "#8080808c",
  fontSize: "85%"
};

interface ManagementSidebarProps {
  close: Function;
}
interface ManagementSidebarState {}

class ManagementSidebar extends React.Component<
  ManagementSidebarProps,
  ManagementSidebarState
> {
  constructor(props: ManagementSidebarProps) {
    super(props);
    this.state = {};
  }

  closeMenu = () => this.props.close();

    logout = () => {
        localStorage.clear();
        window.location.href = `${window.location.protocol}//${window.location.host}/`
    }

  render() {
    return (
      <div className="sidebar">
        <div style={{ display: "flex" }}>
          <img
            style={{ margin: "auto", marginTop: "10px", marginBottom: "10px" }}
            src={window.location.origin + "/img/logo.png"}
          />
        </div>
        <Divider className="sidebar-divider" />
        <List>
          <ListItem
            className="avatarList"
            disabled={true}
            leftAvatar={
              <Avatar
                // tslint:disable-next-line:max-line-length
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Missing_avatar.svg/425px-Missing_avatar.svg.png"
                size={80}
                className="profileAvatar"
              />
            }
          />
        </List>
        <Link to="/" className="sidebar-item-link" onClick={this.closeMenu}>
          <MenuItem className="sidebar-item" leftIcon={<HomeIcon />}>
            Home
          </MenuItem>
        </Link>
        <Divider className="sidebar-divider" />

        <div>
          <p style={sidebarSubtitleStyle}>
            <b>Energy Plans</b>
          </p>
          <Link to="/view-plans" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<HistoryIcon />}
              onClick={this.closeMenu}
            >
              View Energy Plans
            </MenuItem>
          </Link>
          <Link to="/buy-plan" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<PaymentIcon />}
              onClick={this.closeMenu}
            >
              All Sell Orders
            </MenuItem>
          </Link>
          <Link to="/create-plans" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<PaymentIcon />}
              onClick={this.closeMenu}
            >
              Create new Sell orders
            </MenuItem>
          </Link>
        </div>
        <Divider className="sidebar-divider" />

        <div>
          <p style={sidebarSubtitleStyle}>
            <b>Prosumers</b>
          </p>
          <Link to="/create-plans" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<PaymentIcon />}
              onClick={this.closeMenu}
            >
              Create Prosumers
            </MenuItem>
          </Link>
        </div>
        <Divider className="sidebar-divider" />

        <div>
          <p style={sidebarSubtitleStyle}>
            <b>Account</b>
          </p>

          <Link to="/buy-electricity" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<InfoIcon />}
              onClick={this.closeMenu}
            >
              Buy Electricity Units
            </MenuItem>
          </Link>
          <Link to="/transaction-logs" className="sidebar-item-link">
            <MenuItem className="sidebar-item" leftIcon={<HistoryIcon />}>
              Transaction Logs
            </MenuItem>
          </Link>
        </div>
        <Divider className="sidebar-divider" />

        <div>
          <p style={sidebarSubtitleStyle}>
            <b>Users</b>
          </p>
          <Link to="/invite-users" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<PersonAddIcon />}
              onClick={this.closeMenu}
            >
              Add Users
            </MenuItem>
          </Link>
          <Link to="/all-users" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<PeopleIcon />}
              onClick={this.closeMenu}
            >
              All Users
            </MenuItem>
          </Link>
          <Link to="/facilities" className="sidebar-item-link">
            <MenuItem
              className="sidebar-item"
              leftIcon={<AccountBalanceIcon />}
              onClick={this.closeMenu}
            >
              Facility Management
            </MenuItem>
          </Link>
        </div>
        <Divider className="sidebar-divider" />

        <div className="logout-div">
          <Link to="/login" className="sidebar-item-link">
            <RaisedButton onClick={this.logout} className="logout-button">
              {" "}
              Logout{" "}
            </RaisedButton>
          </Link>
        </div>
      </div>
    );
  }
}

export default ManagementSidebar;
