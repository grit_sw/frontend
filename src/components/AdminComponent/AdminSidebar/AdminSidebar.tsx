import * as React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
// import MenuItem from "material-ui/MenuItem";
// import Avatar from "material-ui/Avatar";
// import List from "material-ui/List/List";
// import RaisedButton from "material-ui/RaisedButton";
// import ListItem from "material-ui/List/ListItem";
import HomeIcon from "@material-ui/icons/Home";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import PeopleIcon from "@material-ui/icons/People";
import PaymentIcon from "@material-ui/icons/Payment";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import "./AdminSidebar.css";
import { GetUserDetails } from "../../../services/api-requests/api-requests";

const styles = {
    sidebarSubtitle: {
        margin: "20px",
        color: "#8080808c",
        fontSize: "85%"
    },
    avatar: {
        width: 70,
        height: 70
    },
    button: {
        color: "#fff",
        backgroundColor: "#373737",
        marginTop: "20px"
    }
};

interface AdminSidebarProps extends WithStyles<typeof styles> {
    close: Function;
}
interface AdminSidebarState {
    grit_admin: boolean;
    config_user: boolean;
}

class AdminSidebar extends React.Component<
    AdminSidebarProps,
    AdminSidebarState
    > {
    constructor(props: AdminSidebarProps) {
        super(props);
        this.state = {
            config_user: false,
            grit_admin: false
        };
    }

    closeMenu = () => this.props.close();

    logout = () => {
        localStorage.clear();
        window.location.href = `${window.location.protocol}//${
            window.location.host
            }/`;
    };

    componentWillMount() {
        let userRoleId = localStorage.getItem("user_role_id");
        console.log(userRoleId);
        if (userRoleId === "5") {
            this.setState({
                grit_admin: true
            });
        }
        GetUserDetails()
            .then((res) => {
                // everything is fine so do nothing
                console.log(res)
                this.setState({ config_user: res.data.data.configuration_user }, () => {
                    console.log(this.state.config_user)
                })
            }).catch(err => {
                localStorage.clear()
                // location.reload()

            })
    }

    render() {
        return (
            <div className="admin-sidebar bodyy">
                <List component="nav">
                    <header style={{ display: "flex" }}>
                        <img
                            style={{ margin: "10px auto" }}
                            src={window.location.origin + "/img/logo.png"}
                        />
                    </header>
                    <Divider className="sidebar-divider" />
                    <section className="avatar-section centered">
                        <Avatar
                            src={window.location.origin + "/img/avatar.png"}
                            alt="Profile avatar"
                            className={this.props.classes.avatar}
                        />
                    </section>
                    <Link to="/" className="list-item-link" onClick={this.closeMenu}>
                        <ListItem button={true}>
                            <ListItemIcon children={<HomeIcon nativeColor="#757575" />} />
                            <ListItemText primary="Home" />
                        </ListItem>
                    </Link>
                    <Divider className="sidebar-divider" />
                    <ListSubheader className="list-subheader"> Create Facilities </ListSubheader>
                    <Link
                        to="/create-facilities"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PersonAddIcon nativeColor="#757575" />} />
                            <ListItemText primary="Create Facility" />
                        </ListItem>
                    </Link>
                    <Divider className="sidebar-divider" />
                    <ListSubheader className="list-subheader"> Users </ListSubheader>
                    <Link
                        to="/all-users"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PeopleIcon nativeColor="#757575" />} />
                            <ListItemText primary="All Users" />
                        </ListItem>
                    </Link>
                    <Link
                        to="/invite-users"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon
                                children={<PersonAddIcon nativeColor="#757575" />}
                            />
                            <ListItemText primary="Add Users" />
                        </ListItem>
                    </Link>
                    <Divider className="sidebar-divider" />
                    <ListSubheader className="list-subheader"> Account </ListSubheader>
                    <Link
                        to="/account"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon
                                children={<AccountBalanceIcon nativeColor="#757575" />}
                            />
                            <ListItemText primary="Account" />
                        </ListItem>
                    </Link>
                    <Link
                        to="/buy-electricity"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Recharge Account" />
                        </ListItem>
                    </Link>
                    <Link
                        to="/pay-for-users"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Pay for Users" />
                        </ListItem>
                    </Link>
                    <Divider className="sidebar-divider" />
                    <ListSubheader className="list-subheader">
                        {" "}
                        Energy Plans{" "}
                    </ListSubheader>
                    <Link
                        to="/buy-plan"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Choose Energy Plan" />
                        </ListItem>
                    </Link>
                    <Link
                        to="/create-plans"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Create New Energy Plan" />
                        </ListItem>
                    </Link>
                    <Divider className="sidebar-divider" />
                    <ListSubheader className="list-subheader"> Prosumers </ListSubheader>
                    <Link
                        to="/prosumers"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Prosumers" />
                        </ListItem>
                    </Link>
                    {this.state.grit_admin ? (
                        <Link
                            to="/register-meter"
                            className="list-item-link"
                            onClick={this.closeMenu}
                        >
                            <ListItem button={true}>
                                <ListItemIcon
                                    children={<AccountBalanceIcon nativeColor="#757575" />}
                                />
                                <ListItemText primary="Register Meter" />
                            </ListItem>
                        </Link>
                    ) : (
                            <div />
                        )}
                    <Link
                        to="/manage-meter-groups"
                        className="list-item-link"
                        onClick={this.closeMenu}
                    >
                        <ListItem button={true}>
                            <ListItemIcon
                                children={<AccountBalanceIcon nativeColor="#757575" />}
                            />
                            <ListItemText primary="Manage Meter Groups" />
                        </ListItem>
                    </Link>
                    {/* <Link to="/create-plans" className="list-item-link" onClick={this.closeMenu}>
            <ListItem button={true}>
              <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
              <ListItemText primary="Create Prosumers" />
            </ListItem>
          </Link> */}
                    {this.state.config_user ? (
                        <div>
                            <Divider className="sidebar-divider" />
                            <ListSubheader className="list-subheader">
                                Configurations
                            </ListSubheader>
                            <a
                                href="http://configuration.gtg.grit.systems/"
                                style={{ textDecoration: "none" }}
                            >
                            {/* <a
                                href="http://mydomain.com:3000/"
                                style={{ textDecoration: "none" }}
                            > */}
                                <ListItem>
                                    <ListItemIcon
                                        children={<AccountBalanceIcon nativeColor="#757575" />}
                                    />
                                    <ListItemText primary="Configurations" />
                                </ListItem>
                            </a>
                        </div>
                    ) : (
                            <div />
                        )
                    }
                    <Divider className="sidebar-divider" />

                    <ListSubheader className="list-subheader"> Command and Control </ListSubheader>
                    <a
                        href="https://cmdctrl.grit.systems"
                        style={{ textDecoration: "none" }}
                    >
                        <ListItem>
                            <ListItemIcon
                                children={<AccountBalanceIcon nativeColor="#757575" />}
                            />
                            <ListItemText primary="Command Control" />
                        </ListItem>
                    </a>
                    <Divider className="sidebar-divider" />

                </List>

                <div className="logout centered">
                    <Button
                        href="/login"
                        onClick={this.logout}
                        variant="contained"
                        size="medium"
                        classes={{ root: this.props.classes.button }}
                    >
                        Log Out
                    </Button>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(AdminSidebar);
