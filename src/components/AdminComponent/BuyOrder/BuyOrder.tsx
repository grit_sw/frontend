import * as React from "react";
import "./BuyOrder.css"
import {
    SelectField,
    MenuItem,
    TimePicker,
    // TextField
} from "material-ui"
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBox from "@material-ui/core/Checkbox";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Cancel from "@material-ui/icons/Cancel";
import Close from "@material-ui/icons/Close";
import Timer from "@material-ui/icons/Timer";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpandLess from "@material-ui/icons/ExpandLess";
import { green, blue } from "@material-ui/core/colors"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import {
    GetAllBillingMethods,
    GetUserProsumers,
    GetAllTimeSpanMeasures,
    GetAllBillingUnits,
    GetSellOrdersInSubnet,
    GetAllRenewFrequencies,
    CreateNewBuyOrderAdmin,
    GetAllUsersInRoles
} from "../../../services/api-requests/api-requests"

const styles = {
    inputField: {
        margin: "0 auto 20px",
        width: "80%",
        maxWidth: "500px",
        textAlign: "left" as "left"
    },
    timeSpan: {
        width: "35%"
    },
    checkBox: {
        margin: "20px auto",
        color: "#173d14",
        '&$checkBoxChecked': {
            color: "#173d14",
        },
    },
    checkBoxChecked: {},
    timeLimit: {
        width: "35%"
    },
    placeOrderButton: {
        marginTop: "20px",
        backgroundColor: green[500],
        color: "#fff"
    },
    orderPlacedButton: {
        margin: "20px"
    },
    circularProgress: {
        margin: "3em auto"
    },
    listTitle: {
        textAlign: "center" as "center"
    },
    radioGroup: {
        margin: "10px auto",
        display: "flex" as "flex",
        justifyContent: "space-evenly"
    },
    textFieldLabel: {
        color: green[500],
        '&$textFieldFocused': {
            color: green[500],
        },
    },
    textFieldFocused: {},
    textFieldUnderline: {
        '&:before': {
            borderBottomColor: "#ddd",
        },
        '&:after': {
            borderBottomColor: green[500],
        },
        '&&&&:hover:before': {
            borderBottomColor: green[500],
        }

    }
}
interface BuyOrderProps extends WithStyles<typeof styles> {
}
interface BuyOrderState {
    billing_method: any | null
    all_billing_methods: Array<any>
    billing_unit: any | null
    all_billing_units: Array<any>
    rate: string
    rates: Array<any>
    user: any | null
    all_users: Array<any>
    consumer: any | null
    all_consumers: Array<any>
    additional_options: boolean
    cost_limit: number | undefined
    limits: boolean
    schedule: boolean
    repeat_schedule: boolean
    repeat_freq: any | null
    all_renew_freq: Array<any>
    time_span: string
    time_span_measure: any | null;
    all_time_span_measures: Array<any>
    start_time: any | undefined
    stop_time: any | undefined
    submit_disabled: boolean
    order_success: boolean | null
    order_placed: boolean
    open_modal: boolean
    error: string
}

class BuyOrder extends React.Component<BuyOrderProps, BuyOrderState> {
    constructor(props: BuyOrderProps) {
        super(props);
        this.state = {
            billing_method: null,
            all_billing_methods: [],
            billing_unit: null,
            all_billing_units: [],
            rate: "",
            rates: [],
            user: null,
            all_users: [],
            consumer: "",
            schedule: false,
            all_consumers: [],
            additional_options: false,
            limits: false,
            cost_limit: undefined,
            repeat_schedule: false,
            repeat_freq: null,
            all_renew_freq: [],
            time_span: "",
            time_span_measure: null,
            all_time_span_measures: [],
            start_time: "",
            stop_time: "",
            submit_disabled: true,
            open_modal: false,
            order_placed: false,
            order_success: null,
            error: ""

        }
    }

    componentWillMount() {
        this.getBillingMethods();
        this.getBillingUnits();
        this.getTimeSpanMeasures();
        this.getSellOrdersInSubnet();
        this.getUsers();
        this.getRenewFreq();
    }

    getBillingMethods = () => {
        GetAllBillingMethods()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_billing_methods: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getSellOrdersInSubnet = () => {
        // string is just a placeholder for subnet id
        GetSellOrdersInSubnet("string")
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    rates: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getBillingUnits = () => {
        GetAllBillingUnits()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_billing_units: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getUsers = () => {
        GetAllUsersInRoles("2")
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_users: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getTimeSpanMeasures = () => {
        GetAllTimeSpanMeasures()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_time_span_measures: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getRenewFreq = () => {
        GetAllRenewFrequencies()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_renew_freq: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    enableSubmit = () => {
        if (this.state.billing_method && this.state.billing_unit && this.state.rate && this.state.consumer) {
            this.setState({
                submit_disabled: false
            })
        } else {
            this.setState({
                submit_disabled: true
            })
        }
    }

    handleBillingMethod = (event, index, value) => {
        this.setState({
            billing_method: value
        }, () => {
            // console.log(value);
            this.enableSubmit();
        })
    }

    handleRateChange = (event, index, value) => {
        this.setState({
            rate: value
        }, () => {
            // console.log(value);
            this.enableSubmit();

        })
    }

    handleUserChange = (event, index, value) => {
        this.setState({
            user: value
        }, () => {
            // console.log(value);
            this.enableSubmit();
            GetUserProsumers(this.state.user.group_id)
                .then(response => {
                    console.log(response);
                    let data = response.data.data;
                    this.setState({
                        all_consumers: data
                    })
                })
                .catch(err => {
                    console.log(err);
                })
        })
    }

    handleConsumerChange = (event, index, value) => {
        this.setState({
            consumer: value
        }, () => {
            // console.log(value);
            this.enableSubmit();

        })
    }

    handleBillingUnit = (event, index, value) => {
        this.setState({
            billing_unit: value
        });

    }

    handleAdditionalOptions = () => {
        this.setState({
            additional_options: !this.state.additional_options
        }, () => {
            if (!this.state.additional_options) {
                this.setState({
                    cost_limit: undefined,
                    repeat_schedule: false,
                    repeat_freq: null,
                    start_time: "",
                    stop_time: "",
                });
            }
        })
    }

    toggleSchedule = () => {
        this.setState({
            schedule: !this.state.schedule
        }, () => {
            // console.log(checked);
        })
    }

    toggleLimits = () => {
        this.setState({
            limits: !this.state.limits
        }, () => {
            // console.log(checked);
        })
    }

    handleCostLimit = (event) => {
        this.setState({
            cost_limit: event.currentTarget.value
        }, () => {
            // console.log(checked);
        })
    }

    handleTimeSpan = (event) => {
        this.setState({
            time_span: event.currentTarget.value
        }, () => {
            // console.log(checked);
        })
    }

    handleTimeSpanMeasure = (event, index, value) => {
        this.setState({
            time_span_measure: value
        })
    }

    handleRepeatSchedule = (event, checked) => {
        this.setState({
            repeat_schedule: checked
        }, () => {
            if (!this.state.repeat_schedule) {
                this.setState({
                    repeat_freq: null
                })
            }
        })
    }

    handleRepeatFreq = (event, selected) => {
        this.setState({
            repeat_freq: selected
        }, () => {
            // console.log(checked);
        })
    }

    handleStartTime = (event, time) => {
        // console.log(time.getTime());
        this.setState({
            start_time: time.toJSON()
        })
    }

    handleStopTime = (event, time) => {
        // console.log(time);
        this.setState({
            stop_time: time.toJSON()
        })
    }

    handleCloseModal = () => {
        this.setState({
            open_modal: false,
            order_placed: false,
            order_success: null
        })
    }

    handleSubmit = () => {
        this.setState({
            open_modal: true,
            order_placed: true
        })

        let body = {
            "billing_method_id": this.state.billing_method.billing_method_id,
            "billing_unit_id": this.state.billing_unit.billing_unit_id,
            "rate": this.state.rate,
            "cost_limit": this.state.cost_limit ?
                (this.state.cost_limit > 0 ? this.state.cost_limit : null)
                : null,
            "time_span": this.state.time_span ?
                this.state.time_span
                : null,
            "time_span_measure_id": this.state.time_span_measure ?
                this.state.time_span_measure.time_span_measure_id
                : null,
            "auto_renew": this.state.repeat_schedule,
            "renew_frequency": this.state.repeat_schedule ?
                (this.state.repeat_freq ? this.state.repeat_freq : null)
                : null,
            "start_date_time": this.state.start_time ?
                this.state.start_time
                : null,
            "end_date_time": this.state.stop_time ?
                this.state.stop_time
                : null,
            "user_group_id": this.state.user.group_id,
            "facility_id": this.state.consumer.facility_id,
            // Placeholder value for subnet id 
            "subnet_id": this.state.consumer.facility_id,
        }

        console.log(body);
        CreateNewBuyOrderAdmin(body)
            .then(response => {
                console.log(response);
                this.setState({
                    order_success: true,
                    order_placed: false
                })
            })
            .catch(err => {
                console.log(err.response);
                this.setState({
                    order_success: false,
                    order_placed: false
                })
            })

    }

    // Singled out this component because its line numbers were exceeding 120
    repeatScheduleComp = () => {
        return (
            <RadioGroup
                onChange={this.handleRepeatFreq}
                name="Repeat Frequency"
                row={true}
                value={this.state.repeat_freq}
                classes={{ row: this.props.classes.radioGroup }}
            >
                {(this.state.all_renew_freq)
                    .map((renewFreq, index) => {
                        let id = renewFreq.renew_frequency_id;
                        return (
                            <FormControlLabel
                                control={
                                    <Radio
                                        key={index}
                                        value={id}
                                        checked={id === this.state.repeat_freq}
                                        color="primary"
                                    />
                                }
                                key={index}
                                label={renewFreq.renew_frequency_name}
                            />

                        );
                    })}

            </RadioGroup>);

    }

    render() {
        return (
            <div className="page" style={{ minHeight: "100vh" }}>
                <header className="heading">
                    <h1> Buy Order </h1>
                    <p> Please fill the form provided below to place a buy order </p>
                </header>

                <main className="card">
                    <p style={{ color: "red" }}> {this.state.error} </p>
                    <section>
                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose a billing method"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleBillingMethod}
                            value={this.state.billing_method}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >

                            {(this.state.all_billing_methods).map((billingMethod, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        primaryText={billingMethod.billing_method_name}
                                        value={billingMethod}
                                    />
                                );
                            })}

                        </SelectField>

                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose a billing unit"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleBillingUnit}
                            value={this.state.billing_unit}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >

                            {(this.state.all_billing_units).map((billingUnit, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        primaryText={billingUnit.billing_unit_name}
                                        value={billingUnit}
                                    />
                                );
                            })}

                        </SelectField>

                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose a sell order rate"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleRateChange}
                            value={this.state.rate}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >
                            {(this.state.billing_unit) ?
                                (this.state.billing_unit.billing_unit_name === "Time") ?
                                    this.state.rates.map((rate, index) => {
                                        return (
                                            <MenuItem
                                                key={index}
                                                primaryText={`${rate.rate} (₦/day)`}
                                                value={rate.rate}
                                            />
                                        );
                                    }) :
                                    this.state.rates.map((rate, index) => {
                                        return (
                                            <MenuItem
                                                key={index}
                                                primaryText={`${rate.rate} (₦/kWh)`}
                                                value={rate.rate}
                                            />
                                        );
                                    })

                                : <div />

                            }
                        </SelectField>

                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose a user"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleUserChange}
                            value={this.state.user}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >
                            {(this.state.all_users).map((user, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        primaryText={user.full_name}
                                        value={user}
                                    />
                                );
                            })}

                        </SelectField>

                        {this.state.user ?
                            <SelectField
                                style={styles.inputField}
                                floatingLabelText="Choose an energy consumer"
                                floatingLabelStyle={{ color: green[500] }}
                                onChange={this.handleConsumerChange}
                                value={this.state.consumer}
                                selectedMenuItemStyle={{ color: blue[900] }}
                                menuItemStyle={{ textAlign: "center" }}
                            >
                                {(this.state.all_consumers).map((consumer, index) => {
                                    return (
                                        <MenuItem
                                            key={index}
                                            primaryText={consumer.prosumer_name}
                                            value={consumer}
                                        />
                                    );
                                })}

                            </SelectField>
                            : <div />
                        }

                    </section>

                    <section>

                        {this.state.submit_disabled ? <div /> :
                            <FormControlLabel
                                control={
                                    <CheckBox
                                        checked={this.state.additional_options}
                                        onChange={this.handleAdditionalOptions}
                                        classes={{
                                            root: this.props.classes.checkBox,
                                            checked: this.props.classes.checkBoxChecked,
                                        }}
                                    />
                                }
                                label="Additional Options"

                            />
                        }

                        {this.state.additional_options ?
                            <section>

                                <List>
                                    <ListItem button={true} onClick={this.toggleSchedule}>
                                        <ListItemIcon>
                                            <Timer />
                                        </ListItemIcon>
                                        <ListItemText
                                            inset={true}
                                            primary="Schedule"
                                            classes={{
                                                primary: this.props.classes.listTitle
                                            }}
                                        />
                                        {this.state.schedule ? <ExpandLess /> : <ExpandMore />}
                                    </ListItem>

                                    <Collapse in={this.state.schedule} timeout="auto" unmountOnExit={true}>
                                        <TimePicker
                                            floatingLabelText="Start Time"
                                            onChange={this.handleStartTime}
                                        />

                                        <TimePicker
                                            floatingLabelText="Stop Time"
                                            onChange={this.handleStopTime}
                                        />

                                        <div>
                                            {(this.state.stop_time && this.state.start_time) ?
                                                <span>
                                                    <FormControlLabel
                                                        control={
                                                            <CheckBox
                                                                checked={this.state.repeat_schedule}
                                                                onChange={this.handleRepeatSchedule}
                                                                classes={{
                                                                    root: this.props.classes.checkBox,
                                                                    checked:
                                                                        this.props.classes.checkBoxChecked,
                                                                }}
                                                            />
                                                        }
                                                        label="Repeat"

                                                    />

                                                    {
                                                        this.state.repeat_schedule ?
                                                            <this.repeatScheduleComp />
                                                            : <div />
                                                    }
                                                </span>

                                                : <div />
                                            }
                                        </div>
                                    </Collapse>

                                    <div>
                                        <ListItem button={true} onClick={this.toggleLimits}>
                                            <ListItemIcon>
                                                <Close />
                                            </ListItemIcon>
                                            <ListItemText
                                                inset={true}
                                                primary="Limits"
                                                classes={{
                                                    primary: this.props.classes.listTitle
                                                }}
                                            />
                                            {this.state.limits ? <ExpandLess /> : <ExpandMore />}
                                        </ListItem>

                                        <Collapse in={this.state.limits} timeout="auto" unmountOnExit={true}>
                                            {this.state.billing_method.billing_method_name === "Pay As You Go" ?

                                                <TextField
                                                    label="Cost Limit (₦)"
                                                    style={styles.inputField}
                                                    disabled={this.state.billing_method !== "Pay As You Go"}
                                                    type="number"
                                                    value={this.state.cost_limit}
                                                    onChange={this.handleCostLimit}
                                                />
                                                : <div />
                                            }

                                            <div
                                                className="time-span"
                                            >
                                                <TextField
                                                    label="Time"
                                                    type="number"
                                                    style={styles.timeSpan}
                                                    className="time-span-field"
                                                    value={this.state.time_span}
                                                    onChange={this.handleTimeSpan}
                                                />
                                                <SelectField
                                                    floatingLabelText="Unit"
                                                    style={styles.timeSpan}
                                                    className="time-span-measure"
                                                    onChange={this.handleTimeSpanMeasure}
                                                    value={this.state.time_span_measure}
                                                    selectedMenuItemStyle={{ color: blue[900] }}
                                                    menuItemStyle={{ textAlign: "center" }}
                                                >

                                                    {(this.state.all_time_span_measures)
                                                        .map((measure, index) => {
                                                            let label,
                                                                name = measure.time_span_measure_name;
                                                            label = name.slice(0, name.length - 1);
                                                            return (
                                                                <MenuItem
                                                                    key={index}
                                                                    primaryText={label + "(s)"}
                                                                    value={measure}
                                                                />
                                                            );
                                                        })}
                                                </SelectField>,

                                            </div>

                                            <TextField
                                                label="Energy Limit (kWh)"
                                                disabled={true}
                                                style={styles.inputField}
                                            />

                                        </Collapse>

                                    </div>

                                </List>

                            </section>
                            : <div />
                        }
                    </section>

                    <div>
                        <Button
                            variant="contained"
                            className={this.props.classes.placeOrderButton}
                            disabled={this.state.submit_disabled}
                            onClick={this.handleSubmit}
                        >
                            Place order
                        </Button>

                    </div>

                </main>

                <Dialog
                    open={this.state.open_modal}
                    style={{ textAlign: "center", color: "#555" }}
                    fullWidth={true}
                >
                    {
                        this.state.order_placed ?
                            <CircularProgress className={this.props.classes.circularProgress} /> :
                            <div />
                    }

                    {this.state.order_success === false ?
                        <div
                            style={{
                                marginTop: "20px",
                                margin: "0 auto"
                            }}
                        >
                            <Cancel
                                nativeColor="red"
                                style={{ display: "block", width: "5em", height: "5em", margin: "0 auto" }}

                            />
                            <h3> Error placing this order </h3>

                            <Button
                                className={this.props.classes.orderPlacedButton}
                                onClick={this.handleCloseModal}
                                variant="contained"

                            >
                                Try Again
                            </Button>
                        </div>
                        : this.state.order_success === true ?
                            <div
                                style={{
                                    marginTop: "20px",
                                    margin: "0 auto"
                                }}
                            >
                                <CheckCircle
                                    nativeColor={green[500]}
                                    style={{ display: "block", width: "5em", height: "5em", margin: "0 auto" }}
                                />
                                <h3> Order Placed Successfully </h3>

                                <Button
                                    className={this.props.classes.orderPlacedButton}
                                    href="/"
                                    variant="contained"
                                >
                                    Go Home
                                </Button>
                            </div>

                            : <div />
                    }

                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(BuyOrder);