const prosumerList = {
  "12:34-Shop_1": {
    prosumer_id: "12:34-Shop_1",
    prosumer_state: "PROBE ON",
    last_message: "2018-05-22T22:43:08.783+0100",
    prosumer_user: "Mr. John"
  },
  "AB:24-Shop_2": {
    prosumer_id: "AB:24-Shop_2",
    prosumer_state: "PROBE OFF",
    last_message: "2018-05-22T22:43:08.783+0100",
    prosumer_user: "Mrs. Alabi"
  }
};

export default prosumerList;
