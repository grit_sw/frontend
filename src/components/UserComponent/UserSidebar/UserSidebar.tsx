import * as React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
import HomeIcon from "@material-ui/icons/Home";
// import InfoIcon from "@material-ui/icons/Info";
import PaymentIcon from "@material-ui/icons/Payment";
// import SettingsIcon from "@material-ui/icons/Settings";
import PersonIcon from "@material-ui/icons/Person";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import "./UserSidebar.css";
import { GetUserDetails } from "../../../services/api-requests/api-requests";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";

const styles = {
    sidebarSubtitle: {
        margin: "20px",
        color: "#8080808c",
        fontSize: "85%"
    },
    avatar: {
        width: 70,
        height: 70,
    },
    button: {
        color: "#fff",
        backgroundColor: "#373737",
    }
};

interface UserSidebarProps extends WithStyles<typeof styles> {
    close: Function;
}
interface UserSidebarState {
    config_user: string
}

class UserSidebar extends React.Component<UserSidebarProps, UserSidebarState> {
    constructor(props: UserSidebarProps) {
        super(props);
        this.state = {
            config_user: ''
        };
    }

    closeMenu = () => this.props.close();

    logout = () => {
        localStorage.clear();
        window.location.href = `${window.location.protocol}//${window.location.host}/`
    }

    componentWillMount() {
        GetUserDetails()
            .then((res) => {
                // everything is fine so do nothing
                console.log(res)
                this.setState({ config_user: res.data.data.configuration_user }, () => {
                    console.log(this.state.config_user)
                })
            }).catch(err => {
                localStorage.clear()
                // location.reload()

            })
    }

    render() {
        return (
            <div className="user-sidebar bodyy">

                <List component="nav">

                    <header style={{ display: "flex" }}>
                        <img
                            style={{ margin: "10px auto" }}
                            src={window.location.origin + "/img/logo.png"}
                        />
                    </header>

                    <Divider className="sidebar-divider" />

                    <section className="avatar-section centered">
                        <Avatar
                            src={window.location.origin + "/img/avatar.png"}
                            alt="Profile avatar"
                            className={this.props.classes.avatar}
                        />
                    </section>

                    <Link to="/" className="list-item-link" onClick={this.closeMenu}>
                        <ListItem button={true}>
                            <ListItemIcon children={<HomeIcon nativeColor="#757575" />} />
                            <ListItemText primary="Home" />
                        </ListItem>
                    </Link>

                    <Divider className="sidebar-divider" />

                    <ListSubheader className="list-subheader"> User </ListSubheader>

                    {/* <Link to="/view-plans" className="list-item-link" onClick={this.closeMenu}>
            <ListItem button={true}>
              // <ListItemIcon children={<InfoIcon nativeColor="#757575" />} />
              <ListItemText primary="My Meter(s)" />
            // </ListItem>
          </Link> */}

                    <Link to="/account" className="list-item-link" onClick={this.closeMenu}>
                        <ListItem button={true}>
                            <ListItemIcon children={<PersonIcon nativeColor="#757575" />} />
                            <ListItemText primary="My Account" />
                        </ListItem>
                    </Link>

                    <Link to="/buy-electricity" className="list-item-link" onClick={this.closeMenu}>
                        <ListItem button={true}>
                            <ListItemIcon children={<PaymentIcon nativeColor="#757575" />} />
                            <ListItemText primary="Pay for Electricity" />
                        </ListItem>
                    </Link>

                    {/* <Divider className="sidebar-divider" />

          <ListSubheader className="list-subheader"> Settings </ListSubheader>

          <Link to="/view-plans" className="list-item-link" onClick={this.closeMenu}>
            <ListItem button={true}>
              <ListItemIcon children={<SettingsIcon nativeColor="#757575" />} />
              <ListItemText primary="Settings" />
            </ListItem>
          </Link> */}

                    {this.state.config_user === 'true' ? (
                        <div>
                            <Divider className="sidebar-divider" />
                            <ListSubheader className="list-subheader">
                                Configurations
                            </ListSubheader>
                            <a
                                href="http://configuration.gtg.grit.systems/"
                                style={{ textDecoration: "none" }}
                            >
                                <ListItem>
                                    <ListItemIcon
                                        children={<AccountBalanceIcon nativeColor="#757575" />}
                                    />
                                    <ListItemText primary="Configurations" />
                                </ListItem>
                            </a>
                        </div>
                    ) : (
                            <div />
                        )
                    }

                </List>

                <div className="logout centered">
                    <Button
                        href="/login"
                        onClick={this.logout}
                        variant="contained"
                        size="medium"
                        classes={{ root: this.props.classes.button }}
                    >
                        Log Out
                    </Button>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(UserSidebar);
