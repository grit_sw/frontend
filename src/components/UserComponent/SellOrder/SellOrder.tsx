import * as React from "react";
import "./SellOrder.css"
import {
    SelectField,
    MenuItem,
    TimePicker,
} from "material-ui"
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBox from "@material-ui/core/Checkbox";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Cancel from "@material-ui/icons/Cancel";
import Close from "@material-ui/icons/Close";
import Timer from "@material-ui/icons/Timer";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpandLess from "@material-ui/icons/ExpandLess";
import { green, blue } from "@material-ui/core/colors"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import {
    GetAllBillingUnits,
    GetMyProsumerDetails,
    GetAllRenewFrequencies,
    CreateNewSellOrderSelf
} from "../../../services/api-requests/api-requests"

const styles = {
    inputField: {
        margin: "0 auto 20px",
        width: "80%",
        maxWidth: "500px",
        textAlign: "left" as "left"
    },
    checkBox: {
        margin: "20px auto",
        color: "#173d14",
        '&$checkBoxChecked': {
            color: "#173d14",
        },
    },
    checkBoxChecked: {},
    timeLimit: {
        width: "35%"
    },
    placeOrderButton: {
        marginTop: "20px",
        backgroundColor: green[500],
        color: "#fff"
    },
    orderPlacedButton: {
        margin: "20px"
    },
    circularProgress: {
        margin: "3em auto"
    },
    listTitle: {
        textAlign: "center" as "center"
    },
    radioGroup: {
        margin: "10px auto",
        display: "flex" as "flex",
        justifyContent: "space-evenly"
    },
    textFieldLabel: {
        color: green[500],
        '&$textFieldFocused': {
            color: green[500],
        },
    },
    textFieldFocused: {},
    textFieldUnderline: {
        '&:before': {
            borderBottomColor: "#ddd",
        },
        '&:after': {
            borderBottomColor: green[500],
        },
        '&&&&:hover:before': {
            borderBottomColor: green[500],
        }

    }
}

interface SellOrderProps extends WithStyles<typeof styles> {
}
interface SellOrderState {
    billing_rate: string
    billing_unit: any | null
    all_billing_units: Array<any>
    additional_options: boolean
    source: any | null
    all_sources: Array<any>
    schedule: boolean
    limits: boolean
    power_limit: number | undefined
    energy_limit: number | undefined
    repeat_schedule: boolean
    repeat_freq: any | null
    all_renew_freq: Array<any>
    start_time: any | undefined
    stop_time: any | undefined
    submit_disabled: boolean
    order_success: boolean | null
    order_placed: boolean
    open_modal: boolean
    error: string

}

class SellOrder extends React.Component<SellOrderProps, SellOrderState> {
    constructor(props: SellOrderProps) {
        super(props);
        this.state = {
            billing_rate: "",
            billing_unit: null,
            all_billing_units: [],
            additional_options: false,
            source: null,
            all_sources: [],
            schedule: false,
            limits: false,
            power_limit: undefined,
            energy_limit: undefined,
            repeat_schedule: false,
            repeat_freq: null,
            all_renew_freq: [],
            start_time: "",
            stop_time: "",
            submit_disabled: true,
            open_modal: false,
            order_placed: false,
            order_success: null,
            error: ""
        }
    }

    componentWillMount() {
        this.getBillingUnits();
        this.getSources();
        this.getRenewFreq();
    }

    getBillingUnits = () => {
        GetAllBillingUnits()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_billing_units: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getSources = () => {
        GetMyProsumerDetails()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_sources: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    getRenewFreq = () => {
        GetAllRenewFrequencies()
            .then(response => {
                console.log(response);
                let data = response.data.data;
                this.setState({
                    all_renew_freq: data
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    enableSubmit = () => {
        if (this.state.billing_rate && this.state.billing_unit && this.state.source) {
            this.setState({
                submit_disabled: false
            })
        } else {
            this.setState({
                submit_disabled: true
            })
        }
    }

    handleBillingRate = (event) => {
        this.setState({
            billing_rate: event.currentTarget.value
        }, () => {
            // console.log(value);
            this.enableSubmit();
        })
    }

    handleBillingUnit = (event, index, value) => {
        this.setState({
            billing_unit: value
        }, () => {
            // console.log(value);
            this.enableSubmit();
        })
    }

    handleSourceChange = (event, index, value) => {
        this.setState({
            source: value
        }, () => {
            // console.log(value);
            this.enableSubmit();
        })
    }

    handleAdditionalOptions = () => {
        this.setState({
            additional_options: !this.state.additional_options
        }, () => {
            if (!this.state.additional_options) {
                this.setState({
                    power_limit: undefined,
                    energy_limit: undefined,
                    repeat_schedule: false,
                    repeat_freq: null,
                    start_time: "",
                    stop_time: "",
                });
            }
        })
    }

    toggleSchedule = () => {
        this.setState({
            schedule: !this.state.schedule
        }, () => {
            // console.log(checked);
        })
    }

    toggleLimits = () => {
        this.setState({
            limits: !this.state.limits
        }, () => {
            // console.log(checked);
        })
    }

    handlePowerLimit = (event) => {
        this.setState({
            power_limit: event.currentTarget.value
        }, () => {
            // console.log(checked);
        })
    }

    handleEnergyPerDay = (event) => {
        this.setState({
            energy_limit: event.currentTarget.value
        }, () => {
            // console.log(checked);
        })
    }

    handleRepeatSchedule = (event, checked) => {
        this.setState({
            repeat_schedule: checked
        }, () => {
            if (!this.state.repeat_schedule) {
                this.setState({
                    repeat_freq: null
                })
            }
        })
    }

    handleRepeatFreq = (event, selected) => {
        this.setState({
            repeat_freq: selected
        }, () => {
            console.log(selected);
        })
    }

    handleStartTime = (event, time) => {
        console.log(time);
        this.setState({
            start_time: time.toJSON()
        })

    }

    handleStopTime = (event, time) => {
        console.log(time);
        this.setState({
            stop_time: time.toJSON()
        })
    }

    handleCloseModal = () => {
        this.setState({
            open_modal: false,
            order_placed: false,
            order_success: null
        })
    }

    handleSubmit = () => {
        this.setState({
            open_modal: true,
            order_placed: true
        })

        let body = {
            "billing_unit_id": this.state.billing_unit.billing_unit_id,
            "rate": this.state.billing_rate,
            "auto_renew": this.state.repeat_schedule,
            "power_limit": this.state.power_limit ?
                (this.state.power_limit > 0 ? this.state.power_limit : 0)
                : 0,
            "energy_limit": this.state.energy_limit ?
                (this.state.energy_limit > 0 ? this.state.energy_limit : 0)
                : 0,
            "renew_frequency": this.state.repeat_schedule ?
                (this.state.repeat_freq ? this.state.repeat_freq : null)
                : null,
            "start_date_time": this.state.start_time ?
                this.state.start_time
                : null,
            "end_date_time": this.state.stop_time ?
                this.state.stop_time
                : null,
            "user_group_id": this.state.source.user_group_id,
            "facility_id": this.state.source.facility_id,
            // Placeholder value for subnet id 
            "subnet_id": "string",
        }

        CreateNewSellOrderSelf(body)
            .then(response => {
                console.log(response);
                this.setState({
                    order_success: true,
                    order_placed: false
                })
            })
            .catch(err => {
                console.log(err.response);
                this.setState({
                    order_success: false,
                    order_placed: false
                })
            })

    }

    // Singled out this component because its line numbers were exceeding 120
    repeatScheduleComp = () => {
        return (
            <RadioGroup
                onChange={this.handleRepeatFreq}
                name="Repeat Frequency"
                row={true}
                value={this.state.repeat_freq}
                classes={{ row: this.props.classes.radioGroup }}
            >
                {(this.state.all_renew_freq)
                    .map((renewFreq, index) => {
                        let id = renewFreq.renew_frequency_id;
                        return (
                            <FormControlLabel
                                control={
                                    <Radio
                                        key={index}
                                        value={id}
                                        checked={id === this.state.repeat_freq}
                                        color="primary"
                                    />
                                }
                                key={index}
                                label={renewFreq.renew_frequency_name}
                            />

                        );
                    })}

            </RadioGroup>);

    }

    render() {
        return (
            <div className="create-sell-order page" style={{ minHeight: "100vh" }}>
                <header className="heading">
                    <h1> Sell Order </h1>
                    <p> Please fill the form provided below to place a sell order </p>
                </header>

                <main className="card">
                    <section>

                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose a billing unit"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleBillingUnit}
                            value={this.state.billing_unit}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >
                            {(this.state.all_billing_units).map((billingUnit, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        primaryText={billingUnit.billing_unit_name}
                                        value={billingUnit}
                                    />
                                );
                            })}

                        </SelectField>

                        <TextField
                            label="Rate (₦ per billing unit)"
                            onChange={this.handleBillingRate}
                            style={styles.inputField}
                            type="number"
                            InputProps={{
                                classes: {
                                    underline: this.props.classes.textFieldUnderline
                                }
                            }}
                            InputLabelProps={{
                                classes: {
                                    root: this.props.classes.textFieldLabel,
                                    focused: this.props.classes.textFieldFocused,
                                }
                            }}
                        />

                        <SelectField
                            style={styles.inputField}
                            floatingLabelText="Choose an energy source"
                            floatingLabelStyle={{ color: green[500] }}
                            onChange={this.handleSourceChange}
                            value={this.state.source}
                            selectedMenuItemStyle={{ color: blue[900] }}
                            menuItemStyle={{ textAlign: "center" }}
                        >

                            {(this.state.all_sources).map((source, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        primaryText={source.prosumer_name}
                                        value={source}
                                    />
                                );
                            })}

                        </SelectField>

                    </section>

                    <section>

                        {this.state.submit_disabled ? <div /> :
                            <FormControlLabel
                                control={
                                    <CheckBox
                                        checked={this.state.additional_options}
                                        onChange={this.handleAdditionalOptions}
                                        classes={{
                                            root: this.props.classes.checkBox,
                                            checked: this.props.classes.checkBoxChecked,
                                        }}
                                    />
                                }
                                label="Additional Options"

                            />
                        }

                        {
                            this.state.additional_options ?
                                <section>

                                    <List>
                                        <ListItem button={true} onClick={this.toggleSchedule}>
                                            <ListItemIcon>
                                                <Timer />
                                            </ListItemIcon>
                                            <ListItemText
                                                inset={true}
                                                primary="Schedule"
                                                classes={{
                                                    primary: this.props.classes.listTitle
                                                }}
                                            />
                                            {this.state.schedule ? <ExpandLess /> : <ExpandMore />}
                                        </ListItem>

                                        <Collapse in={this.state.schedule} timeout="auto" unmountOnExit={true}>
                                            <TimePicker
                                                floatingLabelText="Start Time"
                                                onChange={this.handleStartTime}
                                            />

                                            <TimePicker
                                                floatingLabelText="Stop Time"
                                                onChange={this.handleStopTime}
                                            />

                                            <div>
                                                {(this.state.stop_time && this.state.start_time) ?
                                                    <span>
                                                        <FormControlLabel
                                                            control={
                                                                <CheckBox
                                                                    checked={this.state.repeat_schedule}
                                                                    onChange={this.handleRepeatSchedule}
                                                                    color="primary"

                                                                />
                                                            }
                                                            label="Repeat"

                                                        />

                                                        {
                                                            this.state.repeat_schedule ?
                                                                <this.repeatScheduleComp />
                                                                : <div />
                                                        }
                                                    </span>

                                                    : <div />
                                                }
                                            </div>
                                        </Collapse>

                                        <div>
                                            <ListItem button={true} onClick={this.toggleLimits}>
                                                <ListItemIcon>
                                                    <Close />
                                                </ListItemIcon>
                                                <ListItemText
                                                    inset={true}
                                                    primary="Limits"
                                                    classes={{
                                                        primary: this.props.classes.listTitle
                                                    }}
                                                />
                                                {this.state.limits ? <ExpandLess /> : <ExpandMore />}
                                            </ListItem>

                                            <Collapse in={this.state.limits} timeout="auto" unmountOnExit={true}>
                                                {this.state.billing_unit.billing_unit_name === "Time" ?
                                                    <div>
                                                        <TextField
                                                            label="Power Limit (kW)"
                                                            style={styles.inputField}
                                                            value={this.state.power_limit}
                                                            onChange={this.handlePowerLimit}
                                                            disabled={
                                                                this.state.billing_unit.billing_unit_name
                                                                !== "Time"
                                                            }
                                                            type="number"
                                                        />
                                                        <TextField
                                                            label="Energy per Day (kWh/day)"
                                                            style={styles.inputField}
                                                            value={this.state.energy_limit}
                                                            onChange={this.handleEnergyPerDay}
                                                            type="number"
                                                        />

                                                        {/* <TextField
                                                                key={3}
                                                                label="Energy Limit (kWh)"
                                                                disabled={true}
                                                                style={styles.inputField}
                                                                /> */}
                                                    </div>
                                                    : <TextField
                                                        key={0}
                                                        label="Energy Limit (kWh)"
                                                        disabled={true}
                                                        style={styles.inputField}
                                                    />
                                                }

                                            </Collapse>

                                        </div>

                                    </List>

                                </section>
                                : <div />
                        }
                    </section>

                    <div>
                        <Button
                            variant="contained"
                            className={this.props.classes.placeOrderButton}
                            disabled={this.state.submit_disabled}
                            onClick={this.handleSubmit}
                        >
                            Place order
                        </Button>

                    </div>

                </main>

                <Dialog
                    open={this.state.open_modal}
                    style={{ textAlign: "center", color: "#555" }}
                    fullWidth={true}
                >

                    {
                        this.state.order_placed ?
                            <CircularProgress className={this.props.classes.circularProgress} /> :
                            <div />
                    }

                    {this.state.order_success === false ?
                        <div
                            style={{
                                marginTop: "20px",
                                margin: "0 auto"
                            }}
                        >
                            <Cancel
                                nativeColor="red"
                                style={{ display: "block", width: "5em", height: "5em", margin: "0 auto" }}
                            />
                            <h3> Error placing this order </h3>

                            <Button
                                className={this.props.classes.orderPlacedButton}
                                onClick={this.handleCloseModal}
                                variant="contained"

                            >
                                Try Again
                            </Button>

                        </div>
                        : this.state.order_success === true ?
                            <div
                                style={{
                                    marginTop: "20px",
                                    margin: "0 auto"
                                }}
                            >
                                <CheckCircle
                                    nativeColor={green[500]}
                                    style={{ display: "block", width: "5em", height: "5em", margin: "0 auto" }}
                                />
                                <h3> Order Placed Successfully </h3>

                                <Button
                                    className={this.props.classes.orderPlacedButton}
                                    href="/"
                                    variant="contained"
                                >
                                    Go Home
                                </Button>
                            </div>

                            : <div />
                    }

                </Dialog>

            </div>
        )
    }
}

export default withStyles(styles)(SellOrder);
