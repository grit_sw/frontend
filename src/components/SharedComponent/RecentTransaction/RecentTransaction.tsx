import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import FlatButton from 'material-ui/FlatButton';
import './RecentTransaction.css';
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import TransactionDetails from '../TransactionDetails/TransactionDetails';

interface RecentTransactionProps {
}
interface RecentTransactionState {
    show_details: boolean;
}

class RecentTransaction extends React.Component<RecentTransactionProps, RecentTransactionState> {
    constructor(props: RecentTransactionProps) {
        super(props);
        this.state = {
            show_details: false
        };
    }

    showRecentTransactionDetails = () => this.setState({ show_details: true });

    hideRecentTransactionDetails = () => this.setState({ show_details: false });

    render() {

        // const RecentTransacDetailsActions = [
        //     <FlatButton
        //         label="Cancel"
        //         key="button_1"
        //         primary={true}
        //         onClick={this.hideRecentTransactionDetails}
        //     />,
        //     <FlatButton
        //         label="Submit"
        //         key="button_2"
        //         primary={true}
        //         keyboardFocused={true}
        //         onClick={this.hideRecentTransactionDetails}
        //     />,
        // ];

        return (
            <div>
                <Card className="recent-transaction">
                    <CardHeader
                        className="header green-background"
                        title="Recent Completed Transactions"
                        subheader="completed Buy and Sell order"
                    />
                    <Divider />
                    <CardContent>
                        <div className="list">
                            <List>
                                <ListItem onClick={this.showRecentTransactionDetails}>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                            </List>
                        </div>
                    </CardContent>
                    <Divider />
                    <CardActions className="green-background" >
                        <FlatButton label="View All" />
                    </CardActions>
                </Card>

                {/* dialog for showing recent transactions details */}

                <Dialog
                    scroll="paper"
                    fullWidth={true}
                    open={this.state.show_details}
                    onClose={this.hideRecentTransactionDetails}
                >
                    <DialogTitle>Transaction Details </DialogTitle>
                    <TransactionDetails />
                </Dialog>
            </div>
        );
    }
}

export default RecentTransaction;