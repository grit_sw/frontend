import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import './PendingOrders.css';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import  ListItem from '@material-ui/core/ListItem';

interface PendingOrdersProps {
}
interface PendingOrdersState {
}

class PendingOrders extends React.Component<PendingOrdersProps, PendingOrdersState> {
    constructor(props: PendingOrdersProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <Card  className="pending-orders">
                    <CardHeader
                        className="header yellow-background"
                        title="Pending Orders"
                        subheader="Transactions yet to be completed"
                    />
                    <Divider />
                    <CardContent>
                        <div className="content">
                            <List>
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                            </List>
                        </div>
                    </CardContent>
                    <Divider />
                    <CardActions className="yellow-background">
                        <Button> View All </Button>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default PendingOrders;