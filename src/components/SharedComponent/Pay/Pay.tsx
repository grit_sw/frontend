import * as React from "react";
import PayWithPaystack from "../PayWithPaystack/PayWithPaystack";
import "./Pay.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { orange, blue } from "@material-ui/core/colors";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import { GetUserDetails } from "../../../services/api-requests/api-requests";

const styles = {
  errorStyle: {
    color: orange[500]
  },
  textFieldLabel: {
    color: orange[500],
    '&$textFieldFocused': {
      color: blue[500],
    },
  },
  textFieldFocused: {},
  textFieldUnderline: {
    '&:before': {
      borderBottomColor: orange[500],
    },
    '&:after': {
      borderBottomColor: blue[500],
    },
    '&&&&:hover:before': {
      borderBottomColor: orange[500],
    }

  }
};

interface PayProps extends WithStyles<typeof styles> {
  user_paid_for_details: any;
  selected_meter: any;
}
interface PayState {
  open: boolean;
  amount: number;
  enableButton: boolean;
  paystackkey: string;
  transaction_complete: boolean;
  metadata: any;
  user_details: any;
  tx_performed_by: any;
  user_details_loading: boolean;
}

const containerStyle = {
  display: "block",
  width: "200px",
  minHeight: "30%",
  marginTop: "100px",
  marginLeft: "calc(50% - 100px)"
};

const buttonStyle = {
  backgroundColor: "#581818",
  color: "white"
};

class Pay extends React.Component<PayProps, PayState> {
  constructor(props: PayProps) {
    super(props);
    this.state = {
      enableButton: false,
      open: false,
      amount: 0,
      metadata: {},
      paystackkey: "pk_test_585b053fa4f3fb743546f05cf235aeaec455d4c1",
      transaction_complete: false,
      user_details: undefined,
      tx_performed_by: undefined,
      user_details_loading: false
    };
  }

  componentWillMount() {
    this.getUserDetails();
  }

  getUserDetails = () => {
    this.setState({
      user_details_loading: true
    });
    GetUserDetails().then(
      axiosResponse => {
        console.log("user details", axiosResponse);
        if (
          axiosResponse &&
          axiosResponse.data &&
          axiosResponse.data.data &&
          this.props.user_paid_for_details
        ) {
          this.setState({
            user_details_loading: false,
            user_details: axiosResponse.data.data,
            tx_performed_by: axiosResponse.data.data.user_id,
          }, () => {
            // if (!this.props.selected_meter) {
            this.SetMetadata();
            // }
          });
        } else {
          this.setState({
            user_details_loading: false
          });
        }
      },
      err => {
        console.log(err);
        this.setState({
          user_details_loading: false
        });
      }
    );
  };

  handleAmountChange = (e) => {
    this.setState(
      {
        amount: parseInt(e.currentTarget.value, 10)
      },
      () => {
        this.setState({
          enableButton: this.state.amount > 0 ? true : false
        });
      }
    );
  };

  paystackCallback = (response1: any) => {
    console.log(response1);
    if (response1) {
      // let body = {
      //   reference: response1.reference,
      //   trxref: response1.trxref,
      //   group_id: "12345"
      // };

      // PaystackVerify(body).then(
      //   response2 => {
      //     console.log(response2);
      //   },
      //   err => {
      //     console.log(err);
      //   }
      // );

      this.setState({
        amount: 0,
        enableButton: false,
        transaction_complete: true
      });
    }
  };

  paystackOnClose = () => {
    console.log("closed paystack");
  };

  PaystackTransactionInputs = () => {
    return (
      <div>
        <div
          style={{
            fontSize: "50px",
            fontWeight: "bolder",
            color: "grey",
            textAlign: "center"
          }}
        >
          &#8358; {this.state.amount > 0 ? this.state.amount : 0}
          <p style={{ fontSize: "15px" }}>
            Receipt will be sent to {this.props.user_paid_for_details.email}
          </p>
        </div>
        <TextField
          label="Type in the amount in Naira"
          style={{ color: "black", width: "100%" }}
          type="number"
          onChange={this.handleAmountChange}
          InputProps={{
            classes: {
              underline: this.props.classes.textFieldUnderline
            }
          }}
          InputLabelProps={{
            classes: {
              root: this.props.classes.textFieldLabel,
              focused: this.props.classes.textFieldFocused,
            }
          }}
        />

        <br />
        <div style={{ marginTop: "20px" }}>
          <PayWithPaystack
            embed={false}
            email={this.props.user_paid_for_details.email}
            callback={this.paystackCallback}
            close={this.paystackOnClose}
            paystackkey={this.state.paystackkey}
            amount={this.state.amount * 100} // paystack works with kobo so multiply by 100
            disabled={!(this.state.amount > 0)}
            metadata={this.state.metadata}
          />
          <p style={{ fontSize: "12px", color: "red", textAlign: "center" }}>
            {this.state.amount <= 0 ? "!Amount must be greater than 0" : ""}
          </p>
        </div>
      </div>
    );
  };

  PaystackTransactionComplete = () => {
    return (
      <div style={{ fontSize: "30px", color: "grey", textAlign: "center" }}>
        <CheckCircleIcon
          style={{
            color: "royalblue !important",
            width: "60px",
            height: "60px"
          }}
        />
        <h3>
          <b> Transaction Completed </b>
        </h3>
        <Link to="/" className="sidebar-item-link">
          <Button
            style={buttonStyle}
            variant="contained"
          >
            Home
          </Button>
        </Link>
      </div>
    );
  };

  SetMetadata = () => {
    let metadata = {
      user_id: this.props.user_paid_for_details.user_id,
      group_id: this.props.user_paid_for_details.group_id,
      tx_performed_by: this.state.tx_performed_by,
      keypad_user: this.props.user_paid_for_details.keypad_user,
      phone_number: this.props.user_paid_for_details.phone_number,
      email: this.props.user_paid_for_details.email,
      full_name: this.props.user_paid_for_details.full_name,
      meter: this.props.selected_meter,
    }
    this.setState({ metadata: metadata }, () => {
      console.log("this.state", this.state);
    });
  }

  componentDidUpdate(prevProps: any) {
    if (
      this.props.selected_meter !== null &&
      this.props.selected_meter !== prevProps.selected_meter) {
      this.SetMetadata();
    }
  }

  render() {
    console.log("!this.state.user_details_loading", !this.state.user_details_loading);
    return (
      <div style={containerStyle}>
        {this.state.user_details && !this.state.user_details_loading ? (
          <div>
            {this.state.transaction_complete ? (
              <this.PaystackTransactionComplete />
            ) : (
                <this.PaystackTransactionInputs />
              )}
          </div>
        ) : (
            <div style={{ width: "100%", textAlign: "center", color: "grey" }}>
              <h2>Error loading user details.</h2>
              <Button
                style={{ backgroundColor: "purple", color: "#fff" }}
                onClick={this.getUserDetails}
                variant="contained"
              >
                Reload
              </Button>
            </div>
          )}

        {this.state.user_details_loading ? (
          <div style={{ width: "100%", textAlign: "center" }}>
            <CircularProgress />
          </div>
        ) : (
            <div />
          )}
      </div>
    );
  }
}

export default withStyles(styles)(Pay);
