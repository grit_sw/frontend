import * as React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { SearchForUsersRequest } from "../../../services/api-requests/api-requests";
import Pay from "../Pay/Pay";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";

const styles = {
  textFieldUnderline: {
    '&&&&:hover:before': {
      borderBottomColor: "#000080",
    }

  }
};

interface PayForUsersProps extends WithStyles<typeof styles> { }
interface PayForUsersState {
  users: Array<any>;
  searchQuery: string;
  selected_user: any;
}

class PayForUsers extends React.Component<PayForUsersProps, PayForUsersState> {
  constructor(props: PayForUsersProps) {
    super(props);
    this.state = {
      users: [],
      selected_user: null,
      searchQuery: ""
    };
  }

  startSearch = () => {
    SearchForUsersRequest(this.state.searchQuery)
      .then(axiosResponse => {
        console.log(axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          this.setState({
            users: axiosResponse.data.data
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleSearchChange = (e) => {
    this.setState({
      searchQuery: e.currentTarget.value
    });
  };

  selectUser = selectedUser => {
    this.setState({
      selected_user: selectedUser
    });
  };

  UserComponent = (props: { user: any }) => {
    return (
      <div
        style={{
          textAlign: "center",
          padding: "10px",
          boxShadow: "0px 0px 1px 1px #bdbababf",
          width: "250px",
          borderRadius: "5px",
          color: "#444343"
        }}
      >
        <p>{props.user.first_name}</p>
        <p>{props.user.last_name}</p>
        <p>{props.user.email}</p>
        <p>{props.user.role_name}</p>
        <p>{props.user.phone_number}</p>
        <Button
          style={{ color: "purple", outline: "1px solid purple" }}
          onClick={this.selectUser.bind(this, props.user)}
          variant="flat"
        >
          Pay
        </Button>
      </div>
    );
  };

  SearchForUsersComponent = () => {
    return (
      <div style={{ textAlign: "center" }}>
        <TextField
          style={{ margin: "10px" }}
          label="Search for Users to pay for"
          onChange={this.handleSearchChange}
          InputProps={{
            classes: {
              underline: this.props.classes.textFieldUnderline
            }
          }}
        />
        <br />

        <Button
          onClick={this.startSearch}
          style={{ color: "#fff", backgroundColor: "green" }}
          variant="contained"

        >
          Search
        </Button>
      </div>
    );
  };

  render() {
    return (
      <div style={{ minHeight: " 300px" }}>
        <this.SearchForUsersComponent />
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            width: "100%",
            justifyContent: "center"
          }}
        >
          {this.state.users.map(user => {
            return (
              <div key={user.id} style={{ margin: "10px" }}>
                <this.UserComponent user={user} />
                {this.state.selected_user &&
                  this.state.selected_user.id === user.id ? (
                    <div style={{ marginBottom: '100px' }}>
                      <Pay user_paid_for_details={this.state.selected_user} selected_meter={null} />
                    </div>
                  ) : (
                    <div />
                  )}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(PayForUsers);
