import Chart from 'chart.js';
import Button from '@material-ui/core/Button';
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import * as React from 'react';
import { Link } from 'react-router-dom';
import Account from '../Account/Account';
import './Home.css';

import { MyMeterInfo } from "../../../services/api-requests/api-requests";

Chart.defaults.global.responsive = true;

const styles = {
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginBottom: "2em",
        width: "30%",
        margin: "2em"
    }
};

interface HomeProps extends WithStyles<typeof styles> {
}
interface HomeState {
    render_buy_order: boolean;
    render_sell_order: boolean;
}
class Home extends React.Component<HomeProps, HomeState> {
    constructor(props: HomeProps) {
        super(props);
        this.state = {
            render_buy_order: true,
            render_sell_order: true
        };
    }

    componentDidMount() {
        this.getMyMeterInfo();
    }

    getMyMeterInfo = () => {
        MyMeterInfo()
            .then(axiosResponse => {
                console.log(axiosResponse);
                if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
                    this.setState({
                        render_buy_order: axiosResponse.data.data.has_consumer,
                        render_sell_order: axiosResponse.data.data.has_producer
                    });
                }
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    render() {
        return (
            <div className="home-page bodyy">
                <div className="header">
                    <h1>GRIT</h1>
                    <h1>TRANSACTIVE</h1>
                    <h2>GRID</h2>
                    <p>Never experience an electricity outage again</p>

                    {
                        (this.state.render_buy_order) ?
                            (
                                <Link to="/buy-order" style={{ textDecoration: "none" }}>
                                    <Button
                                        variant="contained"
                                        size="large"
                                        classes={{ root: this.props.classes.button }}
                                    >
                                        Create a Buy Order
                                    </Button>
                                </Link>
                            ) :
                            (
                                console.log("No Consumers for this user.")
                            )
                    }

                    {
                        (this.state.render_sell_order) ?
                            (
                                <Link to="/sell-order" style={{ textDecoration: "none" }}>
                                    <Button
                                        variant="contained"
                                        size="large"
                                        classes={{ root: this.props.classes.button }}
                                    >
                                        Create a Sell Order
                                    </Button>
                                </Link>
                            ) :
                            (
                                console.log("No producers for this user.")
                            )
                    }

                </div>

                <div>
                    <Account />
                </div>

            </div >
        );
    }
}

export default withStyles(styles)(Home);