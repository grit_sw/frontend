import { RaisedButton } from 'material-ui';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as React from 'react';
import { GetEnergyPlansInFacility, GetUserDetails } from '../../../services/api-requests/api-requests';
import ListOfFacilities from '../ListOfFacilities/ListOfFacilities';
import './BuyEnergyPlan.css';

interface BuyEnergyPlanProps {

}
interface BuyEnergyPlanState {
    facilities: any[];
    plans: any;
    selected_facilities: { facility_id: string, name: string } | null;
    createPlanFormOpened: boolean;
}

class BuyEnergyPlan extends React.Component<BuyEnergyPlanProps, BuyEnergyPlanState> {
    constructor(props: BuyEnergyPlanProps) {
        super(props);
        this.state = {
            facilities: [],
            plans: null,
            selected_facilities: null,
            createPlanFormOpened: false
        };
    }

    componentWillMount() {
        this.getUserDetails()
    }

    getUserDetails = () => {
        GetUserDetails()
            .then(response => {
                if (response && response.data && response.data.data && response.data.data.facilities) {
                    this.setState({
                        facilities: response.data.data.facilities
                    })
                }
                console.log(this.state)
            })
    }

    getPlansList = () => {
        if (this.state.selected_facilities) {
            GetEnergyPlansInFacility(this.state.selected_facilities.facility_id)
                .then(
                    res => {
                        console.log(res);
                        if (res['data']) {
                            this.setState({
                                plans: res['data'].data
                            }, () => {
                                console.log(this.state.plans)
                            })
                        }
                    }
                );
        }
    }

    openCreatePlanForm = () => {
        this.setState({ createPlanFormOpened: true });
    }

    closeCreatePlanForm = () => {
        this.setState({ createPlanFormOpened: false });
    }

    handleFacilityChange = (event, index, value) => {
        this.setState({
            selected_facilities: value
        }, () => {
            this.getPlansList()
        })
    }

    newPlanCreatedSuccessfully = () => {
        alert("plan created successfully");
        this.setState({
            createPlanFormOpened: false
        })
    }

    render() {

        return (
            <div className="planRoute">
                <div style={{ marginTop: '100px' }}>
                    <span style={{ color: '#ca1616', fontSize: '90%' }}>
                        <b>
                            Select the faculty:
                        </b>
                    </span>
                    <ListOfFacilities
                        handleChange={this.handleFacilityChange}
                        selected_facility={this.state.selected_facilities}
                    />
                </div>
                <br />
                <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                    <div>
                        {
                            (this.state.plans) ?
                                (
                                    <p className="pickPlanText">
                                        <b>
                                            Select a Plan to Buy order from
                                        </b>
                                    </p>
                                ) : <div />
                        }
                        <PlanDetails plans={this.state.plans} />
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

interface PlanDetailsProps {
    plans: any
}
function PlanDetails(props: PlanDetailsProps) {
    let tempArray: Array<any>
    tempArray = []
    for (let i in props.plans) {
        if (props.plans[i]) {
            tempArray.push(props.plans[i])
        }
    }
    console.log(tempArray)
    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {
                tempArray.map((plan) => {
                    return (
                        <Card key={plan.id} className="plan">
                            <CardHeader
                                title={plan.name}
                            />
                            <Divider />
                            <CardText>
                                <div className="feature-list">
                                    <ul>
                                        <li> power limit: {plan.power_limit}</li>
                                        <li> energy limit: {plan.energy_limit}</li>
                                        <li>rate: {plan.rate}</li>
                                    </ul>
                                </div>
                                <RaisedButton className="plan-button" label="Select" />
                            </CardText>
                        </Card>
                    )
                })
            }
        </div>
    )
}

export default BuyEnergyPlan;