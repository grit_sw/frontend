import * as React from 'react';
// import TextField from 'material-ui/TextField';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button"
import { orange, blue } from '@material-ui/core/colors';
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import './FacilityAddUserDialog.css';
import { PostNewInviteUrl } from '../../../services/api-requests/api-requests';

const styles = {
    underlineStyle: {
        borderColor: orange[500],
    },
    floatingLabelStyle: {
        color: orange[500],
    },
    inputGroup: {
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    input: {
        width: '50%',
        minWidth: '250px',
    },
    floatingLabelFocusStyle: {
        color: blue[500],
    },
    button: {
        color: "white",
        margin: "1em"
    }

};

interface FacilityAddUserDialogProps extends WithStyles<typeof styles> {
    selected_role_id: string;
    selected_facility_id: string;
    mfa_required: boolean;
    keypad_user: boolean;
    storeEmails: Function;
}
interface FacilityAddUserDialogState {
    email_list: Array<{ email: string, role: string }>;
    selected_role_id: string;
    selected_facility_id: string;
    mfa_required: boolean;
    keypad_user: boolean
}

class FacilityAddUserDialog extends React.Component<FacilityAddUserDialogProps, FacilityAddUserDialogState> {
    constructor(props: FacilityAddUserDialogProps) {
        super(props);
        this.state = {
            email_list: [],
            selected_role_id: props.selected_role_id,
            selected_facility_id: props.selected_facility_id,
            mfa_required: props.mfa_required,
            keypad_user: props.keypad_user
        };
        console.log(this.state.keypad_user)
    }

    handleEmailInputChange = (index: number, event: any) => {
        let email_list = this.state.email_list;
        email_list[index].email = event.target.value;
        this.setState({
            email_list: email_list
        }, () => {
            let userdetail = this.state.email_list;
            this.props.storeEmails(userdetail);
            console.log(this.props.storeEmails(userdetail))
        });
        console.log(index, event.target.value)
    }

    // UsersData = () => {
    //     let userdetail = this.state.email_list;
    //     this.props.storeEmails(userdetail);
    //     console.log(this.props.storeEmails(userdetail))
    // };

    submitEmails = () => {
        let body = {
            emails: this.state.email_list,
            role_id: this.state.selected_role_id.toString(),
            facility_id: this.state.selected_facility_id,
            mfa_required: this.state.mfa_required,
            keypad_user: this.state.keypad_user
        }
        PostNewInviteUrl(body)
            .then((response) => {
                console.log(response);
                // let data = response.data;
                // if (data.success) {
                // }
            })
            .catch(err => {
                console.log(err.response);
            });
    }

    addNewEmail = () => {
        console.log(this.state.email_list);
        this.setState({
            email_list: this.state.email_list.concat({ email: '', role: this.state.selected_role_id as string })
        }
            , () => {
                let userdetail = this.state.email_list;
                this.props.storeEmails(userdetail);
                console.log(this.props.storeEmails(userdetail))
            }

            // , () => {
            //     localStorage.setItem('new_emails', JSON.stringify(this.state.email_list));
            // }
        );
    }

    render() {
        return (
            <div className="add-user-dialog">
                <div id="email-input-container">
                    <p style={{ color: 'grey', fontSize: '80%' }}>
                        <b>Type in the email addresses of the users to be invited</b>
                    </p>
                    <div>
                        <div className={this.props.classes.inputGroup}>
                            {
                                this.state.email_list.map((item, index) => {
                                    return (
                                        <div key={index}>
                                            {/* <TextField
                                                style={{ width: '50%', minWidth: '250px' }}
                                                floatingLabelStyle={styles.floatingLabelStyle}
                                                floatingLabelShrinkStyle={styles.floatingLabelFocusStyle}
                                                underlineStyle={styles.underlineStyle}
                                                floatingLabelText="Email Address"
                                                inputStyle={{ color: 'white' }}
                                            /> */}
                                            <TextField
                                                id="email"
                                                variant="outlined"
                                                label="Email Address"
                                                classes={{ root: this.props.classes.input }}
                                                value={item.email}
                                                onChange={this.handleEmailInputChange.bind(this, index)}
                                                margin="normal"
                                                InputLabelProps={{ style: { color: "white" } }}
                                                InputProps={{ style: { color: "white" } }}
                                            />
                                        </div>
                                    );
                                })
                            }
                        </div>
                        <div style={{ marginTop: '10px', marginBottom: '20px' }}>
                            <Button
                                type="submit"
                                onClick={this.addNewEmail}
                                variant="text"
                                size="medium"
                                classes={{ root: this.props.classes.button }}
                            >
                                Add New Email
                            </Button>
                            {/*<Button
                                onClick={this.submitEmails}
                                variant="flat"
                                size="medium"
                                classes={{ root: this.props.classes.button }}
                            >
                                Submit
                            </Button>*/}
                        </div>
                        <div style={{ marginTop: '10px', marginBottom: '20px' }}>
                            <Button
                                onClick={this.submitEmails}
                                variant="contained"
                                size="medium"
                                style={{ backgroundColor: "green" }}
                                classes={{ root: this.props.classes.button }}
                            >
                                Submit
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(FacilityAddUserDialog);