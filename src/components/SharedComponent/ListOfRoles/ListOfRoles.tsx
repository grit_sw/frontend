// break this into smaller components later. to large and bulky

import * as React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { orange500, blue500 } from 'material-ui/styles/colors';

import './ListOfRoles.css';
import { GetNewInviteDetails } from '../../../services/api-requests/api-requests';

const styles = {
    underlineStyle: {
        borderColor: orange500,
    },
    floatingLabelStyle: {
        color: orange500,
    },
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: blue500,
    },
};

interface ListOfRolesProps {
    // facilities: Array<{ name: string, id: number | string }>;
    handleChange: any;
    selected_role: { id: string, name: string } | null;
}
interface ListOfRolesState {
    roles: Array<any>;
    selected_role: { id: string, name: string } | null;
    error_loading_role: boolean;
}

class ListOfRoles extends React.Component<ListOfRolesProps, ListOfRolesState> {
    constructor(props: ListOfRolesProps) {
        super(props);
        this.state = {
            roles: [],
            selected_role: null,
            error_loading_role: false
        };
    }

    componentWillMount() {
        this.getInviteDetails();
    }

    handleFacilityChange = (event, index, value) => {
        console.log(value);
        this.setState({
            selected_role: value
        });
    }

    getInviteDetails = () => {
        GetNewInviteDetails()
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log(data);
                    this.setState({
                        roles: data.data.roles,
                    });
                } else {
                    this.setState({
                        error_loading_role: true
                    });
                }
                console.log(response);
            }, err => {
                console.log(err);
            });
    }

    render() {
        return (
            <div>
                <SelectField
                    floatingLabelText="Select Roles"
                    value={this.props.selected_role}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    <MenuItem value="">none</MenuItem>
                    {
                        this.state.roles.map((role) => {
                            return (
                                <MenuItem
                                    key={role.id.toString()}
                                    value={role}
                                    primaryText={role.name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        );
    }
}

export default ListOfRoles;