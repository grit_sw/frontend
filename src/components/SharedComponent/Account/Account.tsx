import * as React from "react";
import "./Account.css";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import PaymentIcon from "@material-ui/icons/Payment";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import {
  GetAccountBalance,
  GetMyPinsRequest,
  GetValidatedPinsReference,
  EnsureAccountActivation,
} from "../../../services/api-requests/api-requests";

const styles = {
  icons: {
    marginTop: "80px",
    marginBottom: "-30px",
    width: "50px",
    height: "50px"
  }
};

interface AccountProps extends WithStyles<typeof styles> { }
interface AccountState {
  account: any;
  pinReferences: Array<string>;
}

class Account extends React.Component<AccountProps, AccountState> {
  constructor(props: AccountProps) {
    super(props);
    this.state = {
      account: {},
      pinReferences: []
    };
  }

  componentDidMount() {
    this.getUserAccountBalance();
    this.getMyPins();
    this.getPinReferences();
  }

  getMyPins = () => {
    GetMyPinsRequest()
      .then(axiosResponse => {
        console.log(axiosResponse);
      })
      .catch(err => {
        console.log(err);
      });
  };

  getPinReferences = () => {
    GetValidatedPinsReference()
      .then(axiosResponse => {
        let data = axiosResponse.data.data;
        console.log(data);
        if (data.length > 0) {
          this.setState({pinReferences: data}, () => {
            let activationData = {
              "reference_list": data
            }
            this.ensureAccountActivation(activationData);
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  getUserAccountBalance = () => {
    GetAccountBalance()
      .then(axiosResponse => {
        console.log(axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          this.setState({
            account: axiosResponse.data.data
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  ensureAccountActivation = (pinReferenceList) => {
    EnsureAccountActivation(pinReferenceList)
      .then(axiosResponse => {
        console.log(axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          this.setState({
            account: axiosResponse.data.data
          }, () => {
            console.log(this.state);
          });
        }
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  render() {
    return (
      <div>
        <div className="account-details-mini-container">
          <div className="payment-status account-balance">
            <h3>Account Balance</h3>
            <PaymentIcon nativeColor="blueviolet" classes={{root: this.props.classes.icons}} />
            <h3 className="actual-status">
              {this.state.account.available_balance} <span> Naira </span>
            </h3>
          </div>
          <div className="payment-status status">
            <h3>Ledger Balance</h3>
            <CheckCircleIcon nativeColor="crimson" classes={{root: this.props.classes.icons}}  />
            <h3 className="actual-status">
              {this.state.account.ledger_balance} <span> Naira </span>
            </h3>
          </div>
          <div className="payment-status ledger-balance">
            <h3>Pending Amount</h3>
            <PaymentIcon nativeColor="royalblue" classes={{root: this.props.classes.icons}} />
            <h3 className="actual-status">
              {this.state.account.pending_amount} <span> KWH </span>
            </h3>
          </div>
          {/* <div className="payment-status status">
                        <h3>Current Plan</h3>
                        <CheckCircleIcon className="check-status-icon status-icon" />
                        <h2 className="actual-status">Gold</h2>
                    </div> */}
        </div>
        {/* <div style={{ width: "100%", textAlign: "center" }}>
          <FlatButton
            className="view-payment-details-button"
            label="View Details"
          />
        </div> */}
      </div>
    );
  }
}

export default withStyles(styles)(Account);
