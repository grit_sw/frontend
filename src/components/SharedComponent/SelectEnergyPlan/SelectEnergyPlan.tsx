import * as React from "react";
import "./SelectEnergyPlan.css";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import { GetMyFacility, GetEnergyPlansInFacility } from "../../../services/api-requests/api-requests";
import { GetMyProsumerDetails, SaveEnergyPlan } from "../../../services/api-requests/api-requests";
import Button from "@material-ui/core/Button";
import { blue } from "@material-ui/core/colors";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";

const styles = {
    selectField: {
        width: "80%",
        maxWidth: "350px"
    },
    labelStyle: {
        color: "#3BB877"
    },
    underlineStyles: {
        borderColor: "#3BB877"
    },
    selectedItem: {
        color: blue[800]
    },
    cardStyles: {
        border: `1.2px solid ${blue[800]}`,
        width: "80%",
        maxWidth: "250px"
    },
    submitButton: {
        backgroundColor: "#3CB878",
        color: "#ffffff"
    }
}

interface SelectEnergyPlanProps extends WithStyles<typeof styles> {
}
interface SelectEnergyPlanState {
    facility_id: string,
    energy_plans: Array<any>
    selected_plan_name: string
    selected_plan_index: string
    selected_plan_id: string
    selected_plan: null | Array<any>
    submit_disabled: boolean
    selected_prosumer_index: number
    selected_prosumer_id: string
    prosumers: Array<any>
}

class SelectEnergyPlan extends React.Component<SelectEnergyPlanProps, SelectEnergyPlanState> {
    constructor(props: SelectEnergyPlanProps) {
        super(props);
        this.state = {
            facility_id: "",
            energy_plans: [],
            selected_plan_name: "",
            selected_plan_index: "",
            selected_plan_id: "",
            selected_plan: null,
            submit_disabled: true,
            selected_prosumer_id: "",
            selected_prosumer_index: 0,
            prosumers: []
        }
    }
    componentDidMount() {
        this.getFacility();
        this.getProsumerDetails();
    }

    handleSave = () => {
        this.setState({
            submit_disabled: true
        })
        let body = {
            prosumer_id: this.state.selected_prosumer_id,
            plan_id: this.state.selected_plan_id
        };
        SaveEnergyPlan(body)
            .then((response) => {
                console.log(response);
                alert("Plan Saved Sucessfully!");
            }, err => {
                console.log(err);
                alert("Error Saving Plan");
            })
    }

    handlePlanChange = (event, index, value) => {
        this.setState({
            selected_plan_name: value,
            selected_plan_index: index,
            selected_plan: this.state.energy_plans[index],
            selected_plan_id: this.state.energy_plans[index].plan_id,
            submit_disabled: false
        })
    }

    handleProsumerChange = (event, index, value) => {
        this.setState({
            selected_prosumer_index: index,
            selected_prosumer_id: this.state.prosumers[index].prosumer_id
        });
    }

    getProsumerDetails = () => {
        GetMyProsumerDetails()
            .then((response) => {
                console.log(response);
                let data = response.data;
                this.setState({
                    prosumers: data.data
                }, () => {
                    this.setState({
                        selected_prosumer_id: this.state.prosumers[0].prosumer_id,
                    });
                });
            }, err => {
                console.log(err);
            })
    }

    getFacility = () => {
        GetMyFacility()
            .then((response) => {
                console.log(response)
                let data = response.data;
                this.setState({
                    facility_id: data.data[0].facility_id
                }, () => {
                    this.getEnergyPlans();
                });
            }, err => {
                console.log(err);
            })
    }

    getEnergyPlans = () => {
        GetEnergyPlansInFacility(this.state.facility_id)
            .then((response) => {
                console.log(response);
                let data = response.data;
                this.setState({
                    energy_plans: data.data
                })
            }, err => {
                console.log(err);
            })
    }

    render() {
        return (
            <div className="select-energy-plan bodyy">
                <div className="heading">
                    <h1> Select an Energy Plan </h1>
                </div>
                <div className="field" >
                    <SelectField
                        className="dropdown-list"
                        onChange={this.handlePlanChange}
                        value={this.state.selected_plan_name}
                        style={styles.selectField}
                        floatingLabelFixed={true}
                        floatingLabelStyle={styles.labelStyle}
                        floatingLabelText="Choose Energy Plan"
                        underlineFocusStyle={styles.underlineStyles}
                        underlineStyle={styles.underlineStyles}
                        selectedMenuItemStyle={styles.selectedItem}
                    >
                        {this.state.energy_plans.map((plans, index) => {
                            return (
                                <MenuItem
                                    key={index}
                                    value={plans.name}
                                    primaryText={plans.name}
                                />
                            );
                        })}
                    </SelectField>
                </div>

                {this.state.selected_plan ? <SelectedPlanCard selected_plan={this.state.selected_plan} /> : <div />}

                {(this.state.prosumers.length > 1) ?
                    <div className="field" >
                        <SelectField
                            className="dropdown-list"
                            onChange={this.handleProsumerChange}
                            value={this.state.prosumers[this.state.selected_prosumer_index].prosumer_id}
                            style={styles.selectField}
                            floatingLabelFixed={true}
                            floatingLabelStyle={styles.labelStyle}
                            floatingLabelText="Choose a Meter"
                            underlineFocusStyle={styles.underlineStyles}
                            underlineStyle={styles.underlineStyles}
                            selectedMenuItemStyle={styles.selectedItem}
                        >
                            {this.state.prosumers.map((prosumers, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        value={prosumers.prosumer_id}
                                        primaryText={prosumers.local_id}
                                    />
                                );
                            })}
                        </SelectField>
                    </div> : <div />}

                <div className="submit">
                    <Button
                        variant="contained"
                        disabled={this.state.submit_disabled}
                        className="submit"
                        onClick={this.handleSave}
                    >
                        Save
                    </Button>
                </div>
            </div>
        );
    }
}

interface SelectedPlanCardProps {
    selected_plan: any
}
function SelectedPlanCard(props: SelectedPlanCardProps) {
    let selected_plan = props.selected_plan;
    return (
        <div className="card" style={styles.cardStyles}>
            <h2> {selected_plan.name} </h2>
            <p> Rate: <span className="rate"> ₦{selected_plan.rate}  </span> <em> KW/hr </em> </p>
            <p> Power Limit:
                <span>
                    {" " + selected_plan.power_limit}
                    <em> KW </em>
                </span>
            </p>
            <p> Energy Limit:
                <span>
                    {" " + selected_plan.energy_limit}
                    <em> KWhr </em>
                </span>
            </p>
        </div>
    );
}

export default withStyles(styles)(SelectEnergyPlan);
