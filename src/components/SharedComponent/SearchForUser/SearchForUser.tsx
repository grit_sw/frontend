import * as React from "react";
import { SearchForUsersRequest } from "../../../services/api-requests/api-requests";

interface UserSearchProps {
  users: Array<any>;
  handleChange: any;
  selected_user: { user_group_id: string, name: string } | null;
}

interface UserSearchState {
  users: Array<any>;
  searchQuery: string;
  selected_user: any;
  error: boolean;
  // autocompleteSearchDebounced: any;
}

class UserSearch extends React.Component<UserSearchProps, UserSearchState> {
  constructor(props: UserSearchProps) {
    super(props);
    this.state = {
      users: [],
      selected_user: null,
      searchQuery: "",
      error: false,
    };
    // this.autocompleteSearchDebounced = debounce(500, this.executeSearch);
  }

  executeSearch = () => {
    SearchForUsersRequest(this.state.searchQuery)
      .then(axiosResponse => {
        console.log(axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          this.setState({
            users: axiosResponse.data.data
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({
          error: true
        });    
      });
  };

  handleQueryChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      searchQuery: e.currentTarget.value
    });

  };

  selectUser = selectedUser => {
    this.setState({
      selected_user: selectedUser
    });
  };

  UserSearchComponent = () => {
    return (
      <div style={{ textAlign: "center" }}>
        {/* <TextField
          style={{ margin: "10px" }}
          floatingLabelText="Search for user"
          onChange={this.handleQueryChange}
        />
        <br />
        <RaisedButton
          onClick={this.executeSearch}
          label="search"
          labelColor="#fff"
          backgroundColor="green"
        /> */}
        {/* <Select
            // classes={classes}
            // styles={selectStyles}
            options={this.props.users}
            // components={components}
            value={this.state.searchQuery}
            onChange={this.props.handleChange}
            placeholder="Search for a user"
        /> */}
      </div>
    );
  };

  render() {
    return (
      <div style={{ minHeight: " 300px" }}>
        <this.UserSearchComponent />
        
        {/* <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            width: "100%",
            justifyContent: "center"
          }}
        >
              <div>
                <SelectField
                    floatingLabelText="Select User"
                    value={this.props.selected_user}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    {
                        this.state.facilities.map((facility) => {
                            return (
                                <MenuItem
                                    key={facility.facility_id.toString()}
                                    value={facility}
                                    primaryText={facility.facility_name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        </div> */}
      </div>
    );
  }
}

export default UserSearch;
