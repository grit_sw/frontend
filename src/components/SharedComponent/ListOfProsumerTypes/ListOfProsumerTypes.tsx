// break this into smaller components later. to large and bulky

import * as React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { green500, lightBlack } from 'material-ui/styles/colors';

import './ListOfProsumerTypes.css';

const styles = {
    underlineStyle: {
        borderColor: green500,
    },
    floatingLabelStyle: {
        display: 'flex',
        color: lightBlack,
        textAlign: "left",
    } as React.CSSProperties,
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: lightBlack,
    },
    itemStyles: {
        textAlign: "left"
    } as React.CSSProperties
}

interface ListOfProsumerTypesProps {
    prosumer_types: Array<any>;
    handleChange: any;
    selected_prosumer_type: any;
}
interface ListOfProsumerTypesState {
    prosumer_types: Array<any>;
    selected_prosumer_type: { prosumer_type_id: string, prosumer_type_url_name: string, prosumer_type_name: string };
    error: boolean;
}

class ListOfProsumerTypes extends React.Component<ListOfProsumerTypesProps, ListOfProsumerTypesState> {
    render() {
        return (
            <div>
                <SelectField
                    floatingLabelText="Select Meter Type"
                    floatingLabelStyle={styles.floatingLabelStyle}
                    value={this.props.selected_prosumer_type}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    {
                        this.props.prosumer_types.map((prosumerType, index) => {
                            return (
                                <MenuItem
                                    style={styles.itemStyles}
                                    key={index}
                                    value={prosumerType}
                                    primaryText={prosumerType.prosumer_type_name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        );
    }
}

export default ListOfProsumerTypes;