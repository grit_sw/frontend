// break this into smaller components later. to large and bulky

import * as React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { green500, lightBlack } from 'material-ui/styles/colors';

import './ListOfMeters.css';

const styles = {
    underlineStyle: {
        borderColor: green500,
    },
    floatingLabelStyle: {
        display: 'flex',
        color: lightBlack,
        textAlign: "left",
    } as React.CSSProperties,
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: lightBlack,
    },
    itemStyles: {
        textAlign: "left"
    } as React.CSSProperties
}

interface ListOfMetersProps {
    meters: Array<any>;
    handleChange: any;
    // selected_meter: { prosumer_group_id: string, name: string };
    selected_meter: any;
}

interface ListOfMetersState {
}

class ListOfMeters extends React.Component<ListOfMetersProps, ListOfMetersState> {
    render() {
        console.log("Meters");
        console.log(this.props.meters);
        return (
            <div>
                <SelectField
                    floatingLabelText="Select a meter"
                    floatingLabelStyle={styles.floatingLabelStyle}
                    value={this.props.selected_meter}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    {
                        this.props.meters.map((meter, index) => {
                            return (
                                <MenuItem
                                    style={styles.itemStyles}
                                    key={index}
                                    value={meter}
                                    primaryText={meter.name || meter.prosumer_name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        );
    }
}

export default ListOfMeters;
