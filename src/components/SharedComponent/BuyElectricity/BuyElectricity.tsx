import * as React from "react";
import "./BuyElectricity.css";
import Button from "@material-ui/core/Button"
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { GetUserDetails, GetMyProsumers } from "../../../services/api-requests/api-requests";
import Pay from "../Pay/Pay";
import ListOfMeters from "../ListOfMeters/ListOfMeters";

const styles = {
  containerStyle: {
    display: "block",
    width: "200px",
    minHeight: "30%",
    marginTop: "100px",
    marginLeft: "calc(50% - 100px)"
  },
  button: {
    color: "#fff",
    backgroundColor: "purple",
    margin: "1em"
  }
}

interface BuyElectricityProps extends WithStyles<typeof styles> { }
interface BuyElectricityState {
  user_details: any;
  meters: any;
  selected_meter: any;
  meter_error_message: string;
  user_details_loading: boolean;
  meter_details_loading: boolean;
  user_details_err: boolean;
  meter_details_err: boolean;
}

class BuyElectricity extends React.Component<
  BuyElectricityProps,
  BuyElectricityState
  > {
  constructor(props: BuyElectricityProps) {
    super(props);
    this.state = {
      user_details: undefined,
      meters: undefined,
      selected_meter: {},
      meter_error_message: "",
      user_details_loading: false,
      meter_details_loading: false,
      user_details_err: false,
      meter_details_err: false
    };
  }

  componentDidMount() {
    this.getUserDetails();
    this.getMyProsumers();
  }

  getUserDetails = () => {
    this.setState({
      user_details_loading: true,
      user_details_err: false
    });
    GetUserDetails().then(
      axiosResponse => {
        console.log("user details", axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          this.setState({
            user_details: axiosResponse.data.data,
            user_details_loading: false
          });
        } else {
          this.setState({
            user_details_loading: false
          });
        }
      },
      err => {
        console.log(err);
        this.setState({
          user_details_loading: false,
          user_details_err: true
        });
      }
    );
  };

  getMyProsumers = () => {
    this.setState({
      meter_details_loading: true,
      user_details_err: false
    });
    GetMyProsumers().then(
      axiosResponse => {
        console.log("Prosumer Details", axiosResponse);
        if (axiosResponse && axiosResponse.data && axiosResponse.data.data) {
          console.log("Prosumer Details", axiosResponse.data.data);
          this.setState({
            meters: axiosResponse.data.data,
            meter_details_loading: false
          });
        } else {
          this.setState({
            meter_details_loading: false
          });
        }
      },
      err => {
        console.log(err.response);
        // if (err.response.status === 404) {
        //   this.setState({
        //     meter_details_err: false
        //   });
        // } else {
        this.setState({
          meter_error_message: err.response.data.message,
          meter_details_err: true
        });
        }
      // }
    );
  };

  handleMeterChange = (event, index, value) => {
    this.setState({
      selected_meter: value
      // payment_disabled: false
    })
  }

  RefreshUsers = () => {
    this.getUserDetails();
  }

  RefreshProsumers = () => {
    this.getMyProsumers();
  }

render() {
    return (
      <div style={styles.containerStyle} className="buy-electricity bodyy">
        {
          this.state.meters && 
          !this.state.meter_details_loading ? (
          <div>
            <ListOfMeters
                meters={this.state.meters}
                handleChange={this.handleMeterChange}
                selected_meter={this.state.selected_meter}
            />
          </div>
        ) : (
            <div />
          )
        }
        {
          this.state.user_details && 
          !this.state.user_details_loading ? (
          <div>
            <Pay user_paid_for_details={this.state.user_details} selected_meter={this.state.selected_meter} />
          </div>
        ) : (
            <div />
          )
        }

        {this.state.user_details_err ? (
          <div style={{ width: "100%", textAlign: "center", color: "grey" }}>
            <h2>Error loading user details.</h2>
            <Button
              type="submit"
              variant="contained"
              size="medium"
              onClick={this.RefreshUsers}
              classes={{ root: this.props.classes.button }}
            >
              Refresh
            </Button>
          </div>
        ) : (
            <div />
          )}

        {this.state.meter_details_err ? (
          <div style={{ width: "100%", textAlign: "center", color: "grey" }}>
            <h2>{this.state.meter_error_message}</h2>
            <Button
              type="submit"
              variant="contained"
              size="medium"
              onClick={this.RefreshProsumers}
              classes={{ root: this.props.classes.button }}
            >
              Refresh
            </Button>
          </div>
        ) : (
            <div />
          )}

        {this.state.user_details_loading ? (
          <div style={{ width: "100%", textAlign: "center" }}>
            <CircularProgress />
          </div>
        ) : (
            <div />
          )}
      </div>
    );
  }
}

export default withStyles(styles)(BuyElectricity);
