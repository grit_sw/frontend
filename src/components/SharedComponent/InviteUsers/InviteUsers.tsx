// break this into smaller components later. to large and bulky
// import CircularProgress from '@material-ui/core/CircularProgress';
// import Dialog from 'material-ui/Dialog';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import { blue, orange } from '@material-ui/core/colors';
import Switch from "@material-ui/core/Switch";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import * as React from 'react';
import { GetNewInviteDetails } from '../../../services/api-requests/api-requests';
import AddUserDialog from '../AddUserDialog/AddUserDialog';
import ListOfFacilities from '../ListOfFacilities/ListOfFacilities';
import ListOfRoles from '../ListOfRoles/ListOfRoles';
import './InviteUsers.css';

const styles = {
    underlineStyle: {
        borderColor: orange[500],
    },
    floatingLabelStyle: {
        color: orange[500],
    },
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: blue[500],
    },
    addEmailsButton: {
        backgroundColor: '#9c1212',
        color: "white"
    },
    closeButton: {
        color: "red",
        margin: "1em",
        display: "flex",
        justifyContent: "flex-end"
    }
};

interface InviteUsersProps extends WithStyles<typeof styles> {
}
interface InviteUsersState {
    roles: Array<any>;
    facilities: Array<any>;
    selected_facility: { facility_id: string, name: string } | null;
    selected_role: { id: string, name: string } | null;
    mfa_required: boolean;
    current_email: { index: number, email: string };
    email_list: Array<{ email: string }>;
    error_loading_facilities: boolean;
    open_add_emails: boolean;
    keypad_user: boolean;
}

class InviteUsers extends React.Component<InviteUsersProps, InviteUsersState> {
    constructor(props: InviteUsersProps) {
        super(props);
        this.state = {
            roles: [],
            facilities: [],
            selected_facility: null,
            selected_role: null,
            mfa_required: false,
            email_list: [],
            current_email: { index: 0, email: '' },
            error_loading_facilities: false,
            open_add_emails: false,
            keypad_user: false
        };
        this.getInviteDetails();
    }

    handleOpenModal = () => {
        this.setState({ open_add_emails: true });
    }
    handleCloseModal = () => {
        this.setState({ open_add_emails: false });
    }

    handleRolesChange = (event, index, value) => {
        console.log(value);
        this.setState({
            selected_role: value
        });
    }

    handleMfaChange = (event, isInputChecked: boolean) => {
        this.setState({
            mfa_required: isInputChecked
        });
    }

    handleKeypadUserChange = (event, isInputChecked: boolean) => {
        this.setState({
            keypad_user: isInputChecked
        });
    }

    handleFacilityChange = (event, index, value) => {
        console.log(value);
        this.setState({
            selected_facility: value
        });
    }

    getInviteDetails = () => {
        GetNewInviteDetails()
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log(data);
                    this.setState({
                        facilities: data.data.facility,
                        roles: data.data.roles,
                        error_loading_facilities: false
                    });
                } else {
                    this.setState({
                        error_loading_facilities: true
                    });
                }
                console.log(response);
            }, err => {
                console.log(err);
            });
    }

    render() {
        // if (!(this.state.facilities && this.state.facilities.length > 0)) {
        //     return (
        //         <div style={{ margin: '200px', textAlign: 'center' }}>
        //             <CircularProgress size={60} thickness={7} />
        //         </div>
        //     );
        // }

        // const dialogActions = [
        //     (
        //         // <FlatButton
        //         //     label="Cancel"
        //         //     primary={true}
        //         //     key="1"
        //         //     onClick={this.handleCloseModal}
        //         // />
        //         <Button
        //             key="1"
        //             onClick={this.handleCloseModal}
        //         >
        //             Cancel
        //         </Button>
        //     )
        // ];

        return (
            <div className="invite-users bodyy">
                <Dialog
                    fullWidth={true}
                    scroll="paper"
                    open={this.state.open_add_emails}
                // contentStyle={{ width: '100%', maxWidth: '600px', margin: 'auto' }}
                >
                    <DialogTitle>
                        Write out the email Adresses
                    </DialogTitle>
                    <DialogContent>
                        <div>
                            {
                                (this.state.selected_role && this.state.selected_facility) ?
                                    (
                                        <AddUserDialog
                                            selected_role_id={(this.state.selected_role.id) as string}
                                            selected_facility_id={this.state.selected_facility.facility_id}
                                            mfa_required={this.state.mfa_required}
                                            keypad_user={this.state.keypad_user}
                                        />
                                    ) :
                                    (
                                        <div>
                                            <p>You have to select a facility and a role</p>
                                        </div>
                                    )
                            }

                        </div>
                            
                        <Button
                            key="1"
                            onClick={this.handleCloseModal}
                            className={this.props.classes.closeButton}
                        >
                            Close
                        </Button>

                    </DialogContent>

                </Dialog>

                <div className="user-invite-details">
                    <div style={{ color: 'grey', fontSize: '90%', textAlign: 'center' }}>
                        <p><b>Invite users to signup by filling in their details in the form below</b></p>
                    </div>
                    <br />
                    <Divider />
                    <br />
                    <div style={styles.groupInput}>
                        <div style={{ margin: '20px', width: '85%', minWidth: '300px' }}>
                            <ListOfRoles
                                // roles={this.state.roles}
                                handleChange={this.handleRolesChange}
                                selected_role={this.state.selected_role}
                            />
                            <p style={{ color: '#808080de', fontSize: '75%' }}>
                                Select the role of the users you will be creating
                            </p>
                        </div>
                        <div style={{ margin: '20px', width: '85%', minWidth: '300px' }}>

                            <ListOfFacilities
                                // facilities={this.state.facilities}
                                handleChange={this.handleFacilityChange}
                                selected_facility={this.state.selected_facility}
                            />

                            <p style={{ color: '#808080de', fontSize: '75%' }}>
                                Select the facility users will be using
                            </p>
                        </div>
                        <div style={{ margin: '20px', width: 'auto', minWidth: '200px', textAlign: 'center' }}>
                            <FormControlLabel
                                control={<Switch value={this.state.mfa_required} />}
                                labelPlacement="start"
                                label="Use Multifactor Authentication"
                                onChange={this.handleMfaChange}
                            />
                        </div>

                        <div style={{ margin: '20px', width: 'auto', minWidth: '200px', textAlign: 'center' }}>

                            <FormControlLabel
                                control={<Switch value={this.state.keypad_user} />}
                                label="Keypad User"
                                labelPlacement="start"
                                onChange={this.handleKeypadUserChange}
                            />
                        </div>
                    </div>
                    <br />
                    <div style={{ width: '100%', textAlign: 'center' }}>
                        <Button
                            className={this.props.classes.addEmailsButton}
                            onClick={this.handleOpenModal}
                        >
                            Start adding emails
                        </Button>
                    </div>
                </div >
            </div >
        );
    }
}

// interface ListOfRolesProps {
//     roles: Array<{ name: string, id: number | string }>;
//     handleChange: any;
//     selected_role: { id: string | number, name: string } | null;
// }
// function ListOfRoles(props: ListOfRolesProps) {
//     return (
//         <div>
//             <SelectField
//                 floatingLabelText="Role?"
//                 value={props.selected_role}
//                 style={{ width: '50%' }}
//                 onChange={props.handleChange}
//                 underlineStyle={styles.underlineStyle}
//                 selectedMenuItemStyle={styles.floatingLabelStyle}
//             >
//                 {
//                     props.roles.map((role) => {
//                         return (
//                             <MenuItem
//                                 key={role.id.toString()}
//                                 value={role}
//                                 primaryText={role.name}
//                             />
//                         );
//                     })
//                 }
//             </SelectField>
//         </div>
//     );
// }

// interface ListOfFacilitiesProps {
//     facilities: Array<{ name: string, id: number | string }>;
//     handleChange: any;
//     selected_facility: { id: string, name: string } | null;
// }
// function ListOfFacilities(props: ListOfFacilitiesProps) {
//     return (
//         <div>
//             <SelectField
//                 floatingLabelText="Facilty?"
//                 value={props.selected_facility}
//                 onChange={props.handleChange}
//                 style={{ width: '50%' }}
//                 underlineStyle={styles.underlineStyle}
//                 selectedMenuItemStyle={styles.floatingLabelStyle}
//             >
//                 {
//                     props.facilities.map((facility) => {
//                         return (
//                             <MenuItem
//                                 key={facility.id.toString()}
//                                 value={facility}
//                                 primaryText={facility.name}
//                             />
//                         );
//                     })
//                 }
//             </SelectField>
//         </div>
//     );
// }

// function ListOfEmailsEntered(props: { emails: Array<{ email: string }> }) {
//     const emails = props.emails;
//     return (
//         <List>
//             {
//                 emails.map((item, index) => {
//                     return (
//                         <div key={index.toString()}>
//                             <ListItem>
//                                 <span
//                                     style={{ marginRight: '20px', float: 'left', color: 'grey' }}
//                                 >
//                                     {index + 1}
//                                 </span>
//                                 {item.email}
//                             </ListItem>
//                         </div>
//                     );
//                 })
//             }
//         </List>
//     );
// }

export default withStyles(styles)(InviteUsers);