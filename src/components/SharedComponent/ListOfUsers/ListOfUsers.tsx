// break this into smaller components later. to large and bulky

import * as React from "react";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { green500, lightBlack } from 'material-ui/styles/colors';

const styles = {
  underlineStyle: {
    borderColor: green500,
  },
  floatingLabelStyle: {
    display: 'flex',
    color: lightBlack,
    textAlign: "left",
  } as React.CSSProperties,
  groupInput: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap' as 'wrap',
    justifyContent: 'center',
    textAlign: 'center' as 'center'
  },
  floatingLabelFocusStyle: {
    color: lightBlack,
  },
  itemStyles: {
    textAlign: "left"
  } as React.CSSProperties
}

interface ListOfUsersProps {
  users: Array<any>;
  handleChange: any;
  selected_user: object;
}
interface ListOfUsersState {
}

class ListOfUsers extends React.Component<ListOfUsersProps, ListOfUsersState> {
  render() {
    return (
      <div>
        <SelectField
          floatingLabelText="Select a user"
          floatingLabelStyle={styles.floatingLabelStyle}
          value={this.props.selected_user}
          onChange={this.props.handleChange}
          style={{ width: "50%" }}
          underlineStyle={styles.underlineStyle}
          selectedMenuItemStyle={styles.floatingLabelStyle}
        >
          {this.props.users.map((user, index) => {
            return (
              <MenuItem
                style={styles.itemStyles}
                key={index}
                value={user}
                primaryText={user.full_name}
              />
            );
          })}
        </SelectField>
      </div>
    );
  }
}

export default ListOfUsers;
