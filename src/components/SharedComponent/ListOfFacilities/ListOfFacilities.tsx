// break this into smaller components later. to large and bulky

import * as React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { green500, lightBlack } from 'material-ui/styles/colors';

import './ListOfFacilities.css';
// import { GetNewInviteDetails } from '../../../services/api-requests/api-requests';
import { GetAllFacilitiesList } from '../../../services/api-requests/api-requests';

const styles = {
    underlineStyle: {
        borderColor: green500,
    },
    floatingLabelStyle: {
        display: 'flex',
        color: lightBlack,
        textAlign: "left",
    } as React.CSSProperties,
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: lightBlack,
    },
    itemStyles: {
        textAlign: "left"
    } as React.CSSProperties
}

interface ListOfFacilitiesProps {
    // facilities: Array<{ name: string, id: number | string }>;
    handleChange: any;
    selected_facility: { facility_id: string, name: string } | null;
}

interface ListOfFacilitiesState {
    facilities: Array<any>;
    selected_facility: { id: string, name: string } | null;
    error_loading_facilities: boolean;
}

class ListOfFacilities extends React.Component<ListOfFacilitiesProps, ListOfFacilitiesState> {
    constructor(props: ListOfFacilitiesProps) {
        super(props);
        this.state = {
            facilities: [],
            selected_facility: null,
            error_loading_facilities: false
        };
    }

    componentWillMount() {
        this.getInviteDetails();
    }

    handleFacilityChange = (event, index, value) => {
        console.log(value);
        this.setState({
            selected_facility: value
        });
    }

    getInviteDetails = () => {
        GetAllFacilitiesList()
            .then((response) => {
                let data = response['data'];
                if (data.success && data.data) {
                    console.log("All Facilities");
                    console.log(data);
                    this.setState({
                        facilities: data.data,
                    });
                } else {
                    this.setState({
                        error_loading_facilities: true
                    });
                }
                console.log(response);
            }, err => {
                console.log(err);
            });
    }

    render() {
        return (
            <div>
                <SelectField
                    floatingLabelText="Select Facilty"
                    floatingLabelStyle={styles.floatingLabelStyle}
                    value={this.props.selected_facility}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    {
                        this.state.facilities.map((facility) => {
                            return (
                                <MenuItem
                                    style={styles.itemStyles}
                                    key={facility.facility_id.toString()}
                                    value={facility}
                                    primaryText={facility.facility_name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        );
    }
}

export default ListOfFacilities;