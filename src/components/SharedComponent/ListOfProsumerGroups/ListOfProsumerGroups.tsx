// break this into smaller components later. to large and bulky

import * as React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { green500, lightBlack } from 'material-ui/styles/colors';

import './ListOfProsumerGroups.css';

const styles = {
    underlineStyle: {
        borderColor: green500,
    },
    floatingLabelStyle: {
        display: 'flex',
        color: lightBlack,
        textAlign: "left",
    } as React.CSSProperties,
    groupInput: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap' as 'wrap',
        justifyContent: 'center',
        textAlign: 'center' as 'center'
    },
    floatingLabelFocusStyle: {
        color: lightBlack,
    },
    itemStyles: {
        textAlign: "left"
    } as React.CSSProperties
}

interface ListOfProsumerGroupsProps {
    prosumer_groups: Array<any>;
    handleChange: any;
    selected_prosumer_group: any;
}

interface ListOfProsumerGroupsState {
}

class ListOfProsumerGroups extends React.Component<ListOfProsumerGroupsProps, ListOfProsumerGroupsState> {
    render() {
        return (
            <div>
                <SelectField
                    floatingLabelText="Select a meter group"
                    floatingLabelStyle={styles.floatingLabelStyle}
                    value={this.props.selected_prosumer_group}
                    onChange={this.props.handleChange}
                    style={{ width: '50%' }}
                    underlineStyle={styles.underlineStyle}
                    selectedMenuItemStyle={styles.floatingLabelStyle}
                >
                    {
                        this.props.prosumer_groups.map((prosumerGroup, index) => {
                            return (
                                <MenuItem
                                    style={styles.itemStyles}
                                    key={index}
                                    value={prosumerGroup}
                                    primaryText={prosumerGroup.name}
                                />
                            );
                        })
                    }
                </SelectField>
            </div>
        );
    }
}

export default ListOfProsumerGroups;