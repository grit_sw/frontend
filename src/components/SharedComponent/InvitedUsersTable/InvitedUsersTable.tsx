import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow  from '@material-ui/core/TableRow';
import TableCell  from '@material-ui/core/TableCell';
import * as React from 'react';
import './AllUsersTable.css';
// import axios from 'axios';
// import { USER_TOKEN, GetAllUserInFacility, GetAllUsers } from '../../config';

interface AllUsersTableProps {
    users: Array<any>;
}

interface AllUsersTableState {
    pagination: number;
    users: Array<any>;
    error_loading_users: Boolean;
}

class AllUsersTable extends React.Component<AllUsersTableProps, AllUsersTableState> {
    constructor(props: AllUsersTableProps) {
        super(props);
        this.state = {
            pagination: 1,
            users: [],
            error_loading_users: false
        };
    }

    render() {
        console.log(this.props.users);
        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>No</TableCell>
                            <TableCell>First Name</TableCell>
                            <TableCell>Last Name</TableCell>
                            <TableCell>Role</TableCell>
                            <TableCell>Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.props.users.map((user, index) => {
                                if (!(user.invite_clicked)) {
                                    return (
                                        <TableRow key={user.email}>
                                            <TableCell>{index}</TableCell>
                                            <TableCell>{user.first_name}</TableCell>
                                            <TableCell>{user.last_name}</TableCell>
                                            <TableCell>{user.role_name}</TableCell>
                                            <TableCell>{user.email}</TableCell>
                                        </TableRow>
                                    );
                                } else {
                                    return (<div />);
                                }
                            })
                        }
                    </TableBody>
                </Table>
            </div>
                    );
                }
            }
            
export default AllUsersTable;