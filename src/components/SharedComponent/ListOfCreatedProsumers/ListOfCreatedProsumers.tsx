import Button from "@material-ui/core/Button"
import * as React from "react";
import { DeleteProsumerRequest, GetProsumers } from "../../../services/api-requests/api-requests";
import "./ListOfCreatedProsumers.css";

interface ListOfCreatedProsumersProps { }
interface ListOfCreatedProsumersState {
  prosumers: Array<any>;
}

class ListOfCreatedProsumers extends React.Component<
  ListOfCreatedProsumersProps,
  ListOfCreatedProsumersState
  > {
  constructor(props: ListOfCreatedProsumersProps) {
    super(props);
    this.state = {
      prosumers: []
    };
  }

  componentWillMount() {
    this.getProsumersList();
  }

  getProsumersList = () => {
    GetProsumers().then(
      axiosResponse => {
        console.log("prosumer list", axiosResponse);
        let data = axiosResponse["data"];
        if (data && data.data) {
          this.setState({
            prosumers: data.data
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  };

  // EditProsumerComponent = () => {
  //   return (
  //     <div>
  //       <div style={{ padding: "10px" }}>
  //         <this.ProsumerTypes />
  //         <ListOfFacilities
  //           handleChange={this.handleFacilityChange}
  //           selected_facility={this.state.selected_facilities}
  //         />
  //         <TextField
  //           style={{ margin: "10px" }}
  //           floatingLabelText="Prosumer name"
  //           onChange={this.prosumerNameChange}
  //         />
  //         <TextField
  //           style={{ margin: "10px" }}
  //           floatingLabelText="Probe id"
  //           onChange={this.prosumerProbeChange}
  //         />
  //       </div>
  //       <div>
  //         <RaisedButton
  //           backgroundColor="green"
  //           labelColor="#fff"
  //           label="submit"
  //           onClick={this.submitProsumer}
  //         />
  //       </div>
  //     </div>
  //   );
  // };

  deleteProsumer(prosumerName: string, facilityId: string) {
    if (confirm("Are you sure you want to delete this prosumer")) {
      DeleteProsumerRequest(prosumerName, facilityId)
        .then(axiosRespone => {
          console.log(axiosRespone);
          alert("Prosumer deleted");
          this.getProsumersList();
        })
        .catch(err => {
          alert("error deleting prosumer");
          console.log(err);
        });
    }
  }

  render() {
    return (
      <div className="created-prosumers-list">
        <p style={{ color: "#353535", fontSize: "90%", textAlign: "center" }}>
          List Of Prosumers
        </p>
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            width: "95%",
            margin: "5px",
            marginTop: "20px"
          }}
        >
          {this.state.prosumers.map(prosumer => {
            return (
              <div
                style={{
                  width: "45%",
                  border: "1px solid grey",
                  borderRadius: "3px"
                }}
                key={prosumer.prosumer_id}
              >
                <div
                  style={{
                    margin: "20px",
                    color: "#3a3838"
                  }}
                >
                  <p>
                    Name: <b>{prosumer.prosumer_name}</b>
                  </p>
                  <p>
                    Type: <b>{prosumer.prosumer_type}</b>
                  </p>
                  <p>
                    Created: <b>{prosumer.date_created}</b>
                  </p>
                  <p>
                    Connected: <b>{prosumer.connected ? "true" : "false"}</b>
                  </p>
                  <p>
                    Assigned: <b>{prosumer.assigned ? "true" : "false"}</b>
                  </p>
                  <Button
                    style={{ margin: "20px" }}
                    variant="flat"
                  >
                    View Details
                  </Button>

                  <Button
                    style={{ margin: "20px" }}
                    variant="flat"
                    onClick={this.deleteProsumer.bind(
                      this,
                      prosumer.prosumer_name,
                      prosumer.facility_id
                    )}
                  >
                    Delete
                  </Button>

                </div>
              </div>
            );
          })}
          <div />
        </div>
      </div>
    );
  }
}

export default ListOfCreatedProsumers;
