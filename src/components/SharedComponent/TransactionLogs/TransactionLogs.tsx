import * as React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import CloudDownload from '@material-ui/icons/CloudDownload';
import './TransactionLogs.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from "@material-ui/core/Button";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
// import Fab from '@material-ui/core/Fab';

const styles = {

    button: {
        backgroundColor: "grey",
        color: "white",
        margin: "5px"
    }
}

interface TransactionLogsProps extends WithStyles<typeof styles> {
}
interface TransactionLogsState {
}

class TransactionLogs extends React.Component<TransactionLogsProps, TransactionLogsState> {
    constructor(props: TransactionLogsProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <div style={{ padding: '10px' }}>
                    <div style={{ textAlign: 'center' }}>
                        <p style={{ color: 'grey', fontSize: '90%' }}>Download logs</p>
                        <Button
                            className={this.props.classes.button}
                        // icon={<CloudDownload />}
                        >
                            <CloudDownload />
                            Download logs as PDF
                        </Button>
                        {/* <Fab variant="extended" aria-label="Delete" className={classes.fab}>
                            <NavigationIcon className={classes.extendedIcon} />
                            Extended
                        </Fab>
                        <Fab variant="extended" aria-label="Delete" className={classes.fab}>
                            <NavigationIcon className={classes.extendedIcon} />
                            Extended
                         </Fab> */}

                        <Button
                            className={this.props.classes.button}
                        // icon={<CloudDownload />}
                        >
                            <CloudDownload />
                            Download logs as Excel
                        </Button>
                    </div>
                    <Table className="transaction-table">
                        <TableHead >
                            <TableRow>
                                <TableCell>Transaction</TableCell>
                                <TableCell>Date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody  >
                            <TableRow>
                                <TableCell>Sell Order</TableCell>
                                <TableCell>12/8/2018</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Buy Electricity</TableCell>
                                <TableCell>12/8/2018</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Buy Order</TableCell>
                                <TableCell>12/8/2018</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Sell Order</TableCell>
                                <TableCell>12/8/2018</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Buy Order</TableCell>
                                <TableCell>12/8/2018</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default withStyles(styles)(TransactionLogs);