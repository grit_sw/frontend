import * as React from 'react';
import './Footer.css';

let today = new Date();
let year = today.getFullYear();

interface FooterProps {
}
interface FooterState {
    open: boolean;
}

class Footer extends React.Component<FooterProps, FooterState> {
    constructor(props: FooterProps) {
        super(props);

    }

    render() {
        return (
            <div>
                <div className="footer-app-bar">
                    <div className="footer-content">
                        <p>©  GRIT Systems Engineering {" " + year}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;