import * as React from 'react';
import { Card, CardHeader, CardActions, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import './ActiveOrders.css';
import Divider from 'material-ui/Divider';
import { List, ListItem } from 'material-ui/List';

interface ActiveOrdersProps {
}
interface ActiveOrdersState {
}

class ActiveOrders extends React.Component<ActiveOrdersProps, ActiveOrdersState> {
    constructor(props: ActiveOrdersProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader
                        className="active-orders-header"
                        title="Active Orders"
                        subtitle="Transactions yet to be completed"
                    />
                    <Divider />
                    <CardText>
                        <div className="active-orders">
                            <List>
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                                <Divider />
                                <ListItem>
                                    <div className="row">
                                        <div className="col-1">
                                            You sold 1000 unit
                                    </div>
                                        <div className="col-2">
                                            5/4/2018
                                    </div>
                                        <div className="col-2">
                                            Transaction Completed
                                    </div>
                                    </div>
                                </ListItem>
                            </List>
                        </div>
                    </CardText>
                    <Divider />
                    <CardActions className="active-orders-header">
                        <FlatButton label="View All" />
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default ActiveOrders;