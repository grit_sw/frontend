import * as React from "react";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import UserSidebar from "../../components/UserComponent/UserSidebar/UserSidebar";
import BuyElectricity from "../../components/SharedComponent/BuyElectricity/BuyElectricity";
import BuyOrder from "../../components/UserComponent/BuyOrder/BuyOrder";
import SellOrder from "../../components/UserComponent/SellOrder/SellOrder";
import Account from "../../components/SharedComponent/Account/Account";
import { Switch, Route } from "react-router-dom";
import Home from "../../components/SharedComponent/Home/Home";
import Footer from "../../components/SharedComponent/Footer/Footer";
import "./UserDashboard.css";

const styles = {
  appBar: {
    flexGrow: 1,
    backgroundColor: "#fff",
    color: "#000"
  },
  menuIcon: {
    marginLeft: "-0.5em",
    marginRight: "0.5em"
  },
  grow: {
    flexGrow: 1,
  }
}

interface UserDashboardProps extends WithStyles<typeof styles> {
}
interface UserDashboardState {
  open: boolean;
}

class UserDashboard extends React.Component<
  UserDashboardProps,
  UserDashboardState
  > {
  constructor(props: UserDashboardProps) {
    super(props);
    this.state = { open: false };
  }

  handleMenuToggle = () => this.setState({ open: !this.state.open });

  handleClose = () => this.setState({ open: false });

  render() {
    return (
      <div className="user-dashboard">

        <div className="side-nav">
          <Drawer
            open={this.state.open}
            onClose={this.handleClose}
            onEscapeKeyDown={this.handleClose}
            className="side-nav"
          >
            <UserSidebar close={this.handleClose} />
          </Drawer>
        </div>

        <div className="nav-content">
          <AppBar
            position="sticky"
            color="default"
            classes={{ root: this.props.classes.appBar }}
          >
            <Toolbar>
              <IconButton onClick={this.handleMenuToggle} disableRipple={false}>
                <MenuIcon
                  titleAccess="Menu"
                  nativeColor="#000"
                  classes={{ root: this.props.classes.menuIcon }}
                />
              </IconButton>

              <div className={this.props.classes.grow}>
                <img src={window.location.origin + "/img/logo.png"} />
              </div>

              <Avatar
                src={window.location.origin + "/img/avatar.png"}
                alt="Profile picture"
              />

              <Typography
                style={{ margin: "0.5em" }}
                variant="subtitle1"
                className="welcome-message"
              >
                Hello,
                <b> {" " + localStorage.getItem("user_name")} </b>
              </Typography>
            </Toolbar>

          </AppBar>

          <Switch>
            <Route
              exact={true}
              path="/account"
              name="Account"
              component={Account}
            />
            <Route
              exact={true}
              path="/buy-order"
              name="Buy Order"
              component={BuyOrder}
            />
            <Route
              exact={true}
              path="/sell-order"
              name="Sell Order"
              component={SellOrder}
            />
            <Route
              exact={true}
              path="/buy-electricity"
              name="Buy Electricity"
              component={BuyElectricity}
            />
            <Route path="/" name="Home" component={Home} />
          </Switch>

        </div>

        <div className="footer-component">
          <Footer />
        </div>

      </div>
    );
  }
}

export default withStyles(styles)(UserDashboard);
