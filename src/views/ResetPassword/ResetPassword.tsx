import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"
import Footer from '../../components/SharedComponent/Footer/Footer';
import './ResetPassword.css';

const styles = {
    underlineStyle: {
        borderColor: green[500],
    },
    floatingLabelShrinkStyle: {
        color: green[500],
    },
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        margin: "3em auto"
    }
};

interface ResetPasswordProps extends WithStyles<typeof styles> {
}
interface ResetPasswordState {
    password: string;
    confirm_password: string;
    // error_message: string;
    // success: boolean;
}

class ResetPassword extends React.Component<ResetPasswordProps, ResetPasswordState> {

    constructor(props: ResetPasswordProps) {
        super(props);
        this.state = {
            password: '',
            confirm_password: "",
            // error_message: '',
            // success: false

        };

    }

    componentWillMount() {

        ValidatorForm.addValidationRule("confirmPassword", (value) => {
            console.log("kefg");
            if (value === this.state.password) {
                return true;
            } else {
                return false;
            }
        });

    }

    handlePasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            password: e.currentTarget.value
        });
    }

    handleConfirmPasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            confirm_password: e.currentTarget.value
        });
    }

    render() {
        return (
            <div className="reset-password bodyy">
                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">
                    <span style={{ 'color': 'grey', 'margin': '5px' }}>
                        <h2> Reset Password </h2>
                        <div style={{ display: 'flex' }}>
                            <img
                                style={{ margin: 'auto', marginTop: '10px', marginBottom: '10px' }}
                                src={window.location.origin + '/img/logo.png'}
                            />
                        </div>
                    </span>
                    <br />
                    <div style={{ "color": "red", "textAlign": "center" }}>
                        {/* <p>{this.state.error_message}</p> */}
                    </div>
                    <br />

                    <ValidatorForm
                    // onSubmit={this.submitData}
                    >

                        <TextValidator
                            className="input"
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Password"
                            inputStyle={{ color: 'black' }}
                            name="password"
                            type="password"
                            value={this.state.password}
                            validators={["required"]}
                            errorMessages={"This field is required"}
                            onChange={this.handlePasswordChange}
                        />

                        <br />
                        <br />
                        <TextValidator
                            className="input"
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Confirm Password"
                            inputStyle={{ color: 'black' }}
                            name="confirm password"
                            type="password"
                            value={this.state.confirm_password}
                            validators={["required", "confirmPassword"]}
                            errorMessages={["This field is required", "Password Mismatch"]}
                            onChange={this.handleConfirmPasswordChange}
                        />

                        <div>
                            <Button
                                type="submit"
                                variant="contained"
                                size="small"
                                classes={{ root: this.props.classes.button }}
                            >
                                Reset Password
                            </Button>
                        </div>
                    </ValidatorForm>

                </div>
                <div className="footer">
                    <Footer />
                </div>

            </div>
        );
    }
}

export default withStyles(styles)(ResetPassword);
