import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import './Signup.css';
import { SignupUser } from '../../services/api-requests/api-requests';
import Footer from '../../components/SharedComponent/Footer/Footer';

const styles = {
    floatingLabelStyle: {
        color: "grey",
    },
    underlineStyle: {
        borderColor: green[500],
    },
    floatingLabelShrinkStyle: {
        color: green[500],
    },
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    containedButton: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginTop: "3.5em"
    },
    flatButton: {
        color: "#3BB877",
    },
    existingAccount: {
        marginTop: "2em",
        fontSize: '90%', 
        color: 'grey' 
    }

};

interface SignupProps extends WithStyles<typeof styles> {
}
interface SignupState {
    password: string;
    first_name: string;
    last_name: string;
    confirm_password: string;
    phone_no: string;
    submit_disabled: boolean;
    error_message: string;
    token: string;
}

class Signup extends React.Component<SignupProps, SignupState>  {

    constructor(props: SignupProps) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            confirm_password: '',
            phone_no: '',
            password: '',
            submit_disabled: true,
            error_message: '',
            token: ""
        };

    }

    componentWillMount() {

        ValidatorForm.addValidationRule("confirmPassword", (value) => {
            if (value === this.state.password) {
                return true;
            } else {
                return false;
            }
        });

        ValidatorForm.addValidationRule("isPhoneNo", (value) => {
            var rePhoneNo = /^(\+234|0)[0-9]{10}$/;

            if (rePhoneNo.test(value)) {
                return true;
            } else {
                return false;
            }
        });

        this.getToken();

    }

    getToken = () => {
        localStorage.clear();

        let splitUrl = window.location.search.split("=");
        let token = splitUrl[1];

        this.setState({
            token: token
        })
    }

    enableSubmit = () => {

        this.setState({
            error_message: '',
            submit_disabled: false
        });
    }

    sendSignupData = (e: React.MouseEvent<{}>) => {
        e.preventDefault();

        this.setState({
            submit_disabled: true
        });

        let body = {
            confirm_password: this.state.confirm_password,
            password: this.state.password,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            phone_number: this.state.phone_no,
            token: this.state.token
        }
        SignupUser(body)
            .then((response) => {
                console.log(response);
                console.log(response.status);

                if (response.status === 201) {
                    const token = response.data.token;
                    console.log(token);
                    localStorage.setItem('user_token', token);
                    localStorage.setItem('user_role_id', response.data.data.role_id);
                    localStorage.setItem('user_name', response.data.first_name);
                    window.location.href = `${window.location.protocol}//${window.location.host}/`
                }

                if ((response.status === 200) && (response.data.require_mfa)) {
                    const token = response.data.token;
                    console.log(token);
                    localStorage.setItem('user_mfa_token', token);
                    console.log(localStorage.getItem("user_mfa_token"));
                    window.location.href = `${window.location.protocol}//${window.location.host}/download-authy`
                    // window.location.href = `${window.location.protocol}//${window.location.host}/mfa`
                }
            })
            .catch(err => {
                console.log(err);
                console.log(err.status);

            });
    }

    handlePasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            password: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    handleFirstNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            first_name: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    handleLastNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            last_name: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    handleConfirmPasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            confirm_password: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    handlePhoneNumberChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            phone_no: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    render() {
        return (
            <div className="signup bodyy">
                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">
                    <span style={{ 'color': 'grey', 'margin': '5px' }}>
                        <h2><b>Signup</b></h2>
                        <div style={{ display: 'flex' }}>
                            <img
                                style={{ margin: 'auto', marginTop: '10px', marginBottom: '10px' }}
                                src={window.location.origin + '/img/logo.png'}
                            />
                        </div>
                    </span>
                    <br />
                    <div style={{ color: 'red', textAlign: 'center' }}>
                        <p>{this.state.error_message}</p>
                    </div>
                    <br />
                    <ValidatorForm
                        onSubmit={this.sendSignupData}
                    >
                        <TextValidator
                            floatingLabelStyle={styles.floatingLabelStyle}
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="First name"
                            inputStyle={{ color: 'black' }}
                            className="input"
                            onChange={this.handleFirstNameChange}
                            name="First name"
                            value={this.state.first_name}
                            validators={["required"]}
                            errorMessages={["This field is required"]}
                        />
                        <br />
                        <br />
                        <TextValidator
                            name="Last name"
                            className="input"
                            floatingLabelStyle={styles.floatingLabelStyle}
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Last name"
                            inputStyle={{ color: 'black' }}
                            onChange={this.handleLastNameChange}
                            value={this.state.last_name}
                            validators={["required"]}
                            errorMessages={["This field is required"]}
                        />
                        <br />
                        <br />
                        <TextValidator
                            floatingLabelStyle={styles.floatingLabelStyle}
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Phone number"
                            type="phone"
                            inputStyle={{ color: 'black' }}
                            className="input"
                            onChange={this.handlePhoneNumberChange}
                            name="Phone number"
                            value={this.state.phone_no}
                            validators={["required", "isPhoneNo"]}
                            errorMessages={["This field is required", "Invalid Phone Number"]}
                        />
                        <br />
                        <br />
                        <TextValidator
                            floatingLabelStyle={styles.floatingLabelStyle}
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Password"
                            inputStyle={{ color: 'black' }}
                            type="password"
                            className="input"
                            onChange={this.handlePasswordChange}
                            name="Password"
                            value={this.state.password}
                            validators={["required"]}
                            errorMessages={["This field is required"]}
                        />
                        <br />
                        <br />
                        <TextValidator
                            floatingLabelStyle={styles.floatingLabelStyle}
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Confirm Password"
                            inputStyle={{ color: 'black' }}
                            type="password"
                            className="input"
                            onChange={this.handleConfirmPasswordChange}
                            name="Confirm Password"
                            value={this.state.confirm_password}
                            validators={["required", "confirmPassword"]}
                            errorMessages={["This field is required", "Password Mismatch"]}
                        />
                        <br />
                        <br />
                        <br />

                        <Button
                            disabled={this.state.submit_disabled}
                            type="submit"
                            variant="contained"
                            size="medium"
                            classes={{ root: this.props.classes.containedButton }}
                        >
                            Sign Up
                        </Button>

                        <div>

                            <p style={styles.existingAccount}>Already have an account?</p>

                            <Button
                                href="/login"
                                variant="flat"
                                fullWidth={true}
                                disableRipple={false}
                                classes={{ root: this.props.classes.flatButton }}
                            >
                                Login
                            </Button>

                        </div>
                    </ValidatorForm>

                </div>
                <div className="footer">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Signup);