import * as React from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import Footer from "../../components/SharedComponent/Footer/Footer";
import "./DownloadAuthy.css"

const styles = {
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginLeft: "78%"
    }
};

interface DownloadAuthyProps extends WithStyles<typeof styles> {
}
interface DownloadAuthyState {
    user_id: string;
    password: string;
    submit_disabled: boolean;
    error_message: string;
}

class DownloadAuthy extends React.Component<DownloadAuthyProps, DownloadAuthyState> {
    render() {
        return (

            <div className="download-authy">

                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">

                    <div className="header">
                        <h2> MFA with Authy <img src={window.location.origin + "/img/authy_logo"} /></h2>
                        <img src={window.location.origin + "/img/logo.png"} />
                    </div>

                    <div className="message">

                        <p> Please download, install and register with Authy on your mobile device to proceed. </p>

                        <div className="download-links">
                            <a href="https://itunes.apple.com/us/app/authy/id494168017">
                                <img src={window.location.origin + "/img/app-store.svg"} alt="Authy AppStore" />
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.authy.authy">
                                <img src={window.location.origin + "/img/google-play.svg"} alt="Authy PlayStore" />
                            </a>
                        </div>
                    </div>

                    <Button
                        type="submit"
                        variant="contained"
                        size="small"
                        classes={{ root: this.props.classes.button }}
                    >
                        Next
                    </Button>
                </div>

                <Footer />
            </div>

        );
    }
}

export default withStyles(styles)(DownloadAuthy);