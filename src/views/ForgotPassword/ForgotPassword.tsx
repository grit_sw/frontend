import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Footer from '../../components/SharedComponent/Footer/Footer';
import './ForgotPassword.css';

const styles = {
    underlineStyle: {
        borderColor: green[500],
    },
    floatingLabelShrinkStyle: {
        color: green[500],
    },
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginTop: "2em"
    }

};

interface ForgotPasswordProps extends WithStyles<typeof styles> {
}
interface ForgotPasswordState {
    email: string;
    submit_disabled: boolean;
    error_message: string;
    success: boolean;
}

class ForgotPassword extends React.Component<ForgotPasswordProps, ForgotPasswordState> {

    constructor(props: ForgotPasswordProps) {
        super(props);
        this.state = {
            email: '',
            submit_disabled: true,
            error_message: '',
            success: false

        };

    }

    handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            email: e.currentTarget.value
        }, () => {
            if (this.state.email) {
                this.setState({
                    submit_disabled: false
                });
            } else {
                this.setState({
                    submit_disabled: true
                });
            }
        });
    }

    submitData = () => {
        this.setState({
            success: true
        });
    }

    render() {
        return (
            <div className="forgot-password bodyy">
                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">
                    <span style={{ 'color': 'grey', 'margin': '5px' }}>
                        <h2> Forgot Password </h2>
                        <div style={{ display: 'flex' }}>
                            <img
                                style={{ margin: 'auto', marginTop: '10px', marginBottom: '10px' }}
                                src={window.location.origin + '/img/logo.png'}
                            />
                        </div>
                    </span>
                    <br />
                    <div style={{ "color": "red", "textAlign": "center" }}>
                        <p>{this.state.error_message}</p>
                    </div>
                    <br />

                    {(this.state.success) ?
                        <div>
                            <p style={{ "marginBottom": "4em" }}>
                                Please click the link sent to your mail to reset your password.
                            </p>

                            <div>
                                <Button
                                    // disabled={this.state.submit_disabled}
                                    href="/"
                                    type="submit"
                                    variant="contained"
                                    size="small"
                                    classes={{ root: this.props.classes.button }}
                                >
                                    Continue
                                </Button>
                            </div>
                        </div>
                        :

                        <ValidatorForm
                            onSubmit={this.submitData}
                        >

                            <TextValidator
                                className="input"
                                floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                                underlineStyle={styles.underlineStyle}
                                underlineFocusStyle={styles.underlineStyle}
                                floatingLabelText="Enter your email address"
                                inputStyle={{ color: 'black' }}
                                name="email"
                                value={this.state.email}
                                validators={["isEmail", "required"]}
                                errorMessages={["Invalid Email Address", "This field is required"]}
                                onChange={this.handleEmailChange}
                            />

                            <div>
                                <Button
                                    disabled={this.state.submit_disabled}
                                    type="submit"
                                    variant="contained"
                                    size="small"
                                    classes={{ root: this.props.classes.button }}
                                >
                                    Submit
                                </Button>
                            </div>

                        </ValidatorForm>
                    }

                </div>
                <div className="footer">
                    <Footer />
                </div>

            </div>
        );
    }
}

export default withStyles(styles)(ForgotPassword);
