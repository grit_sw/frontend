import * as  React from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import "./MfaWithAuthy.css"
import { AuthyAuthentication } from "../../services/api-requests/api-requests";
import Footer from "../../components/SharedComponent/Footer/Footer";

const styles = {
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginTop: "3.5em"
    }
};

interface MfaWithAuthyProps extends WithStyles<typeof styles> {
}
interface MfaWithAuthyState {
    authy_code: string;
    submit_disabled: boolean;
    error_message: string;
}

const formStyles = {
    underlineStyle: {
        borderColor: green[500],
    },
    floatingLabelShrinkStyle: {
        color: green[500],
    },
};

class MfaWithAuthy extends React.Component<MfaWithAuthyProps, MfaWithAuthyState> {
    constructor(props: MfaWithAuthyProps) {
        super(props);
        this.state = {
            authy_code: "",
            submit_disabled: true,
            error_message: ""
        };

    }

    enableSubmit = () => {
        this.setState({
            error_message: "",
            submit_disabled: false
        });
    }

    handleChange = (e: React.FormEvent<HTMLInputElement>) => {

        this.setState({
            authy_code: e.currentTarget.value
        }, () => { this.enableSubmit() })

    }

    submitData = (e: React.MouseEvent<{}>) => {
        this.setState({
            submit_disabled: true
        });

        let body = {
            authy_code: this.state.authy_code
        }

        AuthyAuthentication(body)
            .then((response) => {
                console.log(response);
                let data = response.data;
                if (data.success) {
                    this.authenticationSuccessful(data);
                }
            })
            .catch(err => {
                console.log(body);
                console.log(err);

                if (err.response.status === 401) {
                    this.setState({
                        error_message: err.response.data.message
                    });
                }
            });
    }

    authenticationSuccessful = (userData: any) => {
        if (userData && userData.token && userData.data && userData.data.role_id) {
            const token = userData.token;
            localStorage.setItem('user_token', token);
            localStorage.setItem('user_role_id', userData.data.role_id);
            localStorage.setItem('user_name', userData.data.first_name);
            window.location.href = `${window.location.protocol}//${window.location.host}`;
        }
    }
    render() {
        return (
            // Used bodyy cos I a css file somewhere is styling it. Intended to be changed back
            <div className="mfa bodyy">

                <div className="header">
                    <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                        <Toolbar>
                            <Typography color="inherit" variant="title">
                                GRIT Transactive Grid
                        </Typography>
                        </Toolbar>
                    </AppBar>

                </div>

                <div className="card">
                    <h2> MFA with Authy <img src={window.location.origin + '/img/authy_logo'} /> </h2>
                    <img src={window.location.origin + '/img/logo.png'} />

                    <span style={{ display: "block", color: "red", margin: "50px" }}>
                        {this.state.error_message}
                    </span>
                    <div className="form">
                        <div>
                            <ValidatorForm
                                onSubmit={this.submitData}
                            >
                                <TextValidator
                                    floatingLabelText="Input the MFA code from your phone"
                                    fullWidth={true}
                                    floatingLabelStyle={{ color: "#808080" }}
                                    floatingLabelShrinkStyle={formStyles.floatingLabelShrinkStyle}
                                    underlineStyle={formStyles.underlineStyle}
                                    underlineFocusStyle={formStyles.underlineStyle}
                                    inputStyle={{ color: "black" }}
                                    onChange={this.handleChange}
                                    name="mfa-code"
                                    value={this.state.authy_code}
                                    validators={["required", 'matchRegexp:^[0-9]*$']}
                                    errorMessages={["This field is empty", "Invalid Code"]}
                                />

                                <Button
                                    disabled={this.state.submit_disabled}
                                    type="submit"
                                    variant="contained"
                                    size="small"
                                    classes={{ root: this.props.classes.button }}
                                >
                                    Submit
                                </Button>

                            </ValidatorForm>
                        </div>
                    </div>
                </div>

                <div>
                    <Footer />
                </div>

            </div >
        );
    }

}

export default withStyles(styles)(MfaWithAuthy);
