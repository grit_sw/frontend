import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { ValidatorForm } from "react-material-ui-form-validator";
import { ResendInviteEmail } from '../../services/api-requests/api-requests';
import Footer from '../../components/SharedComponent/Footer/Footer';
import './ResendInvite.css';

const styles = {
    // floatingLabelStyle: {
    //     color: "grey",
    // },
    // underlineStyle: {
    //     borderColor: green[500],
    // },
    // floatingLabelShrinkStyle: {
    //     color: green[500],
    // },
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },  
    containedButton: {
        color: "#fff",
        backgroundColor: "#3BB877",
        margin: "3.5em auto"
    },
    flatButton: {
        color: "#3BB877",
    },
    existingAccount: {
        marginTop: "2em",
        fontSize: '90%', 
        color: 'grey' 
    }
};

interface ResendInviteProps extends WithStyles<typeof styles> {
}
interface ResendInviteState {
    token: string;
    submit_disabled: boolean;
}

class ResendInvite extends React.Component<ResendInviteProps, ResendInviteState>  {

    constructor(props: ResendInviteProps) {
        super(props);

        this.state = {
            token: "",
            submit_disabled: false
        };

    }

    componentWillMount() {

        this.getToken();

    }

    getToken = () => {
        localStorage.clear();

        let splitUrl = window.location.search.split("=");
        let token = splitUrl[1];
        console.log("token");
        console.log(token);
        this.setState({
            token: token
        })
    }

    sendResendInviteData = (e: React.MouseEvent<{}>) => {
        e.preventDefault();

        this.setState({
            submit_disabled: true
        });

        ResendInviteEmail(this.state.token)
            .then((response) => {
                console.log(response);
                console.log(response.status);

                if (response.status === 201) {
                    console.log(response.data);
                }
            })
            .catch(err => {
                console.log(err);
                console.log(err.status);

            });
    }

    render() {
        return (
            <div className="resend-invite bodyy">
                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">
                    <span style={{ 'color': 'grey', 'margin': '5px' }}>
                        <p><b>Your invitation link has expired.</b></p>
                        <div style={{ display: 'flex' }}>
                            <img
                                style={{ margin: 'auto', marginTop: '10px', marginBottom: '10px' }}
                                src={window.location.origin + '/img/logo.png'}
                            />
                        </div>
                    </span>
                    <br />
                    <br />
                    <ValidatorForm
                        onSubmit={this.sendResendInviteData}
                    >

                        <Button
                            // disabled={this.state.submit_disabled}
                            type="submit"
                            variant="contained"
                            size="small"
                            classes={{ root: this.props.classes.containedButton }}
                        >
                            Resend Invite
                        </Button>

                        <div>

                            <p style={styles.existingAccount}>Already have an account?</p>                     

                            <Button
                                href="/login"
                                variant="flat"
                                fullWidth={true}
                                disableRipple={false}
                                classes={{ root: this.props.classes.flatButton }}
                            >
                                Login
                            </Button>

                        </div>
                    </ValidatorForm>

                </div>
                <div className="footer">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(ResendInvite);
