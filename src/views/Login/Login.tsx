import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import green from "@material-ui/core/colors/green"
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"
import Footer from '../../components/SharedComponent/Footer/Footer';
import { LoginUser } from '../../services/api-requests/api-requests';
import './Login.css';

const styles = {
    underlineStyle: {
        borderColor: green[500],
    },
    floatingLabelShrinkStyle: {
        color: green[500],
    },
    forgotPassword: {
        textAlign: "center" as "center",
        color: "grey"
    },
    appBar: {
        color: "#fff",
        backgroundColor: "#3BB877",
        fontSize: "2em",
        fontWeight: 400
    },
    button: {
        color: "#fff",
        backgroundColor: "#3BB877",
        marginBottom: "2em"
    }
};

interface LoginProps extends WithStyles<typeof styles> {
}
interface LoginState {
    user_id: string;
    password: string;
    submit_disabled: boolean;
    error_message: string;
}

class Login extends React.Component<LoginProps, LoginState> {

    constructor(props: LoginProps) {
        super(props);
        this.state = {
            user_id: '',
            password: '',
            submit_disabled: true,
            error_message: '',

        };

    }

    componentWillMount() {
        ValidatorForm.addValidationRule("isEmailOrPhoneNo", (value) => {
            var reEmail = /[^@]+@[^@]+\.[^@]+/;
            var rePhoneNo = /^(\+234|0)[0-9]{10}$/;

            if ((reEmail.test(value)) || (rePhoneNo.test(value))) {
                return true;
            } else {
                return false;
            }
        });
    }

    enableSubmit = () => {
        this.setState({
            error_message: '',
            submit_disabled: false,
        });
    }

    submitData = (e: React.MouseEvent<{}>) => {
        e.preventDefault();
        this.setState({
            submit_disabled: true
        });
        let body = {
            user_id: this.state.user_id,
            password: this.state.password
        }
        LoginUser(body)
            .then((response) => {
                console.log(response);
                let data = response.data;
                if (data.success) {

                    if (response.status === 201) {
                        this.loginSuccessful(data);
                    }

                    if ((response.status === 200) && (response.data.require_mfa)) {
                        const token = response.data.token;
                        console.log(token);
                        localStorage.setItem('user_mfa_token', token);
                        window.location.href = `${window.location.protocol}//${window.location.host}/mfa`
                    }
                }
            })
            .catch(err => {
                console.log(err);
                console.log(err.response);

                this.setState({
                    submit_disabled: true,
                });

                if (err.response && err.response.status === 401) {
                    this.setState({
                        error_message: err.response.data.message
                    })
                }

            });
    }

    loginSuccessful = (userData: any) => {
        if (userData && userData.token && userData.data && userData.data.role_id) {
            const token = userData.token;
            localStorage.setItem('user_token', token);
            localStorage.setItem('user_role_id', userData.data.role_id);
            localStorage.setItem('user_name', userData.data.first_name);
            window.location.href = `${window.location.protocol}//${window.location.host}`;
        }
    }

    handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            user_id: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    handlePasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            password: e.currentTarget.value
        }, () => {
            this.enableSubmit();
        });
    }

    render() {
        return (
            // Used bodyy cos I a css file somewhere is styling it. Intended to be changed back to body
            <div className="login bodyy">

                <AppBar color="default" position="sticky" classes={{ root: this.props.classes.appBar }}>
                    <Toolbar>
                        <Typography color="inherit" variant="title">
                            GRIT Transactive Grid
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className="card">
                    <span style={{ 'color': 'grey', 'margin': '5px' }}>
                        <h2>
                             <b>Login</b> 
                        </h2>

                        <div style={{ display: 'flex' }}>
                            <img
                                style={{ margin: 'auto', marginTop: '10px', marginBottom: '10px' }}
                                src={window.location.origin + '/img/logo.png'}
                            />
                        </div>
                    </span>
                    <br />
                    <div style={{ "color": "red", "textAlign": "center" }}>
                        <p>{this.state.error_message}</p>
                    </div>
                    <br />
                    <ValidatorForm
                        onSubmit={this.submitData}
                    >

                        <TextValidator
                            className="input"
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Email Address / Phone number"
                            inputStyle={{ color: 'black' }}
                            name="email"
                            value={this.state.user_id}
                            validators={["isEmailOrPhoneNo", "required"]}
                            errorMessages={["Invalid Email Address or Phone Number", "This field is required"]}
                            onChange={this.handleEmailChange}
                        />

                        <br />
                        <br />
                        <TextValidator
                            className="input"
                            floatingLabelShrinkStyle={styles.floatingLabelShrinkStyle}
                            underlineStyle={styles.underlineStyle}
                            underlineFocusStyle={styles.underlineStyle}
                            floatingLabelText="Password"
                            inputStyle={{ color: 'black' }}
                            name="password"
                            type="password"
                            value={this.state.password}
                            validators={["required"]}
                            errorMessages={"This field is required"}
                            onChange={this.handlePasswordChange}
                        />
                        <br />
                        <br />
                        <br />

                            <Button
                                disabled={this.state.submit_disabled}
                                type="submit"
                                variant="contained"
                                size="small"
                                classes={{ root: this.props.classes.button }}
                            >
                                Log In
                            </Button>

                    </ValidatorForm>

                    <a
                        style={styles.forgotPassword}
                        href="/forgot-password"
                    >
                        Forgot Password?
                    </a>

                </div>
                <div className="footer">
                    <Footer />
                </div>

            </div >
        );
    }
}

export default withStyles(styles)(Login);