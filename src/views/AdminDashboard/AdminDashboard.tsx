import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import AdminSidebar from "../../components/AdminComponent/AdminSidebar/AdminSidebar";
import AllUserPlans from "../../components/AdminComponent/AllUserEnergyPlans/AllUserEnergyPlans";
import AllUsers from "../../components/AdminComponent/AllUsers/AllUsers";
import BuyElectricity from "../../components/SharedComponent/BuyElectricity/BuyElectricity";
import BuyEnergyPlan from "../../components/SharedComponent/BuyEnergyPlan/BuyEnergyPlan";
import CreateEnergyPlan from "../../components/AdminComponent/CreateEnergyPlan/CreateEnergyPlan";
import Footer from "../../components/SharedComponent/Footer/Footer";
import Home from "../../components/SharedComponent/Home/Home";
import InviteUsers from "../../components/SharedComponent/InviteUsers/InviteUsers";
import ManageMeterGroups from "../../components/AdminComponent/ManageMeterGroups/ManageMeterGroups";
import MeterRegistration from "../../components/AdminComponent/MeterRegistration/MeterRegistration";
import PayForUsers from "../../components/SharedComponent/PayForUsers/PayForUsers";
import Prosumers from "../../components/AdminComponent/Prosumers/Prosumers";
import Account from "../../components/SharedComponent/Account/Account";
import BuyOrder from "../../components/AdminComponent/BuyOrder/BuyOrder";
import SellOrder from "../../components/AdminComponent/SellOrder/SellOrder";
import "./AdminDashboard.css";
import OnBoarding from "../../components/OnBoarding/OnBoarding";

const styles = {
  appBar: {
    flexGrow: 1,
    backgroundColor: "#fff",
    color: "#000"
  },
  menuIcon: {
    marginLeft: "-0.5em",
    marginRight: "0.5em"
  },
  grow: {
    flexGrow: 1
  }
};

interface AdminDashboardProps extends WithStyles<typeof styles> {}
interface AdminDashboardState {
  open: boolean;
  grit_admin: boolean;
}

class AdminDashboard extends React.Component<
  AdminDashboardProps,
  AdminDashboardState
> {
  constructor(props: AdminDashboardProps) {
    super(props);
    this.state = {
      open: false,
      grit_admin: false
    };
  }

  handleMenuToggle = () => this.setState({ open: !this.state.open });

  handleClose = () => this.setState({ open: false });

  componentWillMount() {
    let userRoleId = localStorage.getItem("user_role_id");
    console.log(userRoleId);
    if (userRoleId === "5") {
      this.setState({
        grit_admin: true
      });
    }
  }

  render() {
    return (
      <div className="admin-dashboard">
        <div className="side-nav">
          <Drawer
            open={this.state.open}
            onClose={this.handleClose}
            onEscapeKeyDown={this.handleClose}
            className="side-nav"
          >
            <AdminSidebar close={this.handleClose} />
          </Drawer>
        </div>

        <div className="nav-content">
          <AppBar
            position="sticky"
            color="default"
            classes={{ root: this.props.classes.appBar }}
          >
            <Toolbar>
              <IconButton onClick={this.handleMenuToggle} disableRipple={false}>
                <MenuIcon
                  titleAccess="Menu"
                  nativeColor="#000"
                  classes={{ root: this.props.classes.menuIcon }}
                />
              </IconButton>

              <div className={this.props.classes.grow}>
                <img src={window.location.origin + "/img/logo.png"} />
              </div>

              <Avatar
                src={window.location.origin + "/img/avatar.png"}
                alt="Profile picture"
              />

              <Typography
                style={{ margin: "0.5em" }}
                variant="subtitle1"
                className="welcome-message"
              >
                Hello,
                <b> {" " + localStorage.getItem("user_name")} </b>
              </Typography>
            </Toolbar>
          </AppBar>

          <Switch>
            <Route
              exact={true}
              path="/all-users"
              name="All Users"
              component={AllUsers}
            />
            <Route
              exact={true}
              path="/create-facilities"
              name="Create Facilities"
              component={OnBoarding}
            />
            <Route
              exact={true}
              path="/prosumers"
              name="Prosumers"
              component={Prosumers}
            />
            <Route
              exact={true}
              path="/create-plans"
              name="Create Energy Plan"
              component={CreateEnergyPlan}
            />
            {this.state.grit_admin ? (
              <Route
                exact={true}
                path="/register-meter"
                name="Register Meter"
                component={MeterRegistration}
              />
            ) : (
              <span />
            )}
            <Route
              exact={true}
              path="/view-plans"
              name="View Energy Plan"
              component={AllUserPlans}
            />
            <Route
              exact={true}
              path="/buy-order"
              name="Buy Order"
              component={BuyOrder}
            />
            <Route
              exact={true}
              path="/sell-order"
              name="Sell Order"
              component={SellOrder}
            />
            <Route
              exact={true}
              path="/account"
              name="Account"
              component={Account}
            />
            <Route
              exact={true}
              path="/pay-for-users"
              name="Pay for Users"
              component={PayForUsers}
            />
            <Route
              exact={true}
              path="/buy-plan"
              name="Buy Plan"
              component={BuyEnergyPlan}
            />
            <Route
              exact={true}
              path="/invite-users"
              name="Invite Users"
              component={InviteUsers}
            />
            <Route
              exact={true}
              path="/buy-electricity"
              name="Buy Electricity"
              component={BuyElectricity}
            />
            <Route
              exact={true}
              path="/manage-meter-groups"
              name="Manage Meter Groups"
              component={ManageMeterGroups}
            />
            <Route path="/" name="Home" component={Home} />
          </Switch>
        </div>

        <div className="footer-component">
          <Footer />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(AdminDashboard);
