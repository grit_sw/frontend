// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import lightBaseTheme from "material-ui/styles/baseThemes/lightBaseTheme";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import { GetUserDetails } from "./services/api-requests/api-requests";
import AdminDashboard from "./views/AdminDashboard/AdminDashboard";
import DownloadAuthy from "./views/DownloadAuthy/DownloadAuthy";
import ForgotPassword from "./views/ForgotPassword/ForgotPassword";
import Login from "./views/Login/Login";
import MfaWithAuthy from "./views/MfaWithAuthy/MfaWithAuthy";
import ResendInvite from "./views/ResendInvite/ResendInvite";
import ResetPassword from "./views/ResetPassword/ResetPassword";
import Signup from "./views/Signup/Signup";
import UserDashboard from "./views/UserDashboard/UserDashboard";

const verifyTokenByGettingUserDetails = () => {
  GetUserDetails()
    .then(res => {
      // everything is fine so do nothing
      console.log(res);
    })
    .catch(err => {
      localStorage.clear();
      // location.reload()
    });
};

interface SetRouteProps {
  user_token: string | null;
  user_role_id: string | null;
}

function SetRoutes(props: SetRouteProps) {
  const userToken = props["user_token"];
  const userRole = props["user_role_id"];
  verifyTokenByGettingUserDetails();

  if (userToken && (userRole === "5" || userRole === "4" || userRole === "3")) {
    return (
      <Switch>
        <Route path="/" name="Admin Dashboard" component={AdminDashboard} />
      </Switch>
    );
  } else if (userToken && (userRole === "2" || userRole === "1")) {
    return (
      <Switch>
        <Route path="/" name="User Dashboard" component={UserDashboard} />
      </Switch>
    );
  } else {
    return (
      <Switch>
        <Route path="/login" name="Login" component={Login} />
        <Route path="/signup" name="Signup" component={Signup} />
        <Route
          path="/resend-invite"
          name="Resend Invite"
          component={ResendInvite}
        />
        <Route
          path="/forgot-password"
          name="Forgot Password"
          component={ForgotPassword}
        />
        <Route
          path="/reset-password"
          name="Reset Password"
          component={ResetPassword}
        />
        <Route
          path="/download-authy"
          name="Download Authy"
          component={DownloadAuthy}
        />
        <Route path="/mfa" name="MFA" component={MfaWithAuthy} />
        <Route path="/" name="Login" component={Login} />
      </Switch>
    );
  }
}

function setCookie(name: string, value: string | null, domain: string) {
  // domain = domain || ".mydomain.com";

  var cookieStr =
    name.toString() + "=" + JSON.stringify(value) + "; domain=" + domain;
  document.cookie = cookieStr;
}

class App extends React.Component {
  userToken: string | null = localStorage.getItem("user_token");
  userRoleId: string | null = localStorage.getItem("user_role_id");
  userName: string | null = localStorage.getItem("user_name");
  // get user_token from localstorage
  componentWillMount() {
    const userToken = localStorage.getItem("user_token");
    const userRole = localStorage.getItem("user_role_id");
    const userName = localStorage.getItem("user_name");

    setCookie("userToken", userToken, ".grit.systems");
    setCookie("userRole", userRole, ".grit.systems");
    setCookie("userName", userName, ".grit.systems");
    // setCookie("userToken", userToken, ".mydomain.com");
    // setCookie("userRole", userRole, ".mydomain.com");
    // setCookie("userName", userName, ".mydomain.com");
  }

  render() {
    // console.log(cookie.load('userRole'))
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <BrowserRouter>
          <SetRoutes
            user_token={this.userToken}
            user_role_id={this.userRoleId}
          />
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
}

export default App;
