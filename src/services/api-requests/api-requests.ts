import Axios from "axios";

const USER_TOKEN = localStorage.getItem("user_token");
const MFA_TOKEN = localStorage.getItem("user_mfa_token");

const axiosConfigMfa = {
  headers: {
    "X-API-KEY": MFA_TOKEN,
    "Access-Control-Allow-Origin": "https://configuration.gtg.grit.systems",
    "Access-Control-Allow-Methods": "GET",
    "Access-Control-Allow-Headers": "X-Requested-With",
    "Content-Security-Policy": "default-src 'unsafe-inline' *",
    "X-Content-Security-Policy": "default-src 'unsafe-inline' *",
    "X-WebKit-CSP": "default-src 'unsafe-inline' *"
  }
};

const axiosConfig = {
  headers: {
    "X-API-KEY": USER_TOKEN,
    "Access-Control-Allow-Origin": "https://configuration.gtg.grit.systems",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
    "Access-Control-Allow-Headers": "X-Requested-With",
    "Content-Security-Policy": "default-src 'unsafe-inline' *",
    "X-Content-Security-Policy": "default-src 'unsafe-inline' *",
    "X-WebKit-CSP": "default-src 'unsafe-inline' *"
  }
};

const MyBaseUrl = process.env.REACT_APP_BACKEND_SERVER || "http://dev-backend.gtg.grit.systems:5500";

const countryUrl = "https://api.printful.com/countries"

export const GritLogo = "public/logo.jpg";

export const GetMyFacility = () => {
  return Axios.get(`${MyBaseUrl}/facility/me/`, axiosConfig);
};

export const GetMyProsumerDetails = () => {
  return Axios.get(`${MyBaseUrl}/prosumers/me/`, axiosConfig);
};

export const getUsersInFacility = (facilityId: string) => {
  return Axios.get(`${MyBaseUrl}/users/all/${facilityId}/`, axiosConfig);
};

export const LoginUser = body => {
  console.log(MyBaseUrl);
  return Axios.post(`${MyBaseUrl}/auth/login/`, body, axiosConfig);
};

export const SignupUser = body => {
  return Axios.post(`${MyBaseUrl}/auth/signup/`, body, axiosConfig);
};

export const ResendInviteEmail = (token: string) => {
  return Axios.get(`${MyBaseUrl}/invite/resend/?key=${token}`, axiosConfig);
};

export const PaystackVerify = body => {
  return Axios.post(`${MyBaseUrl}/paystack/verify/`, body, axiosConfig);
};

export const PostNewInviteUrl = body => {
  return Axios.post(`${MyBaseUrl}/invite/new/`, body, axiosConfig);
};

export const GetAllUsersInFacility = (facilityId: string) => {
  return Axios.get(
    `${MyBaseUrl}/users/in-facility/${facilityId}/`,
    axiosConfig
  );
};

export const GetAllUsersInRoles = (roleId: string) => {
  return Axios.get(`${MyBaseUrl}/users/by-role/${roleId}/`, axiosConfig);
};

export const GetAllUsers = () => {
  return Axios.get(`${MyBaseUrl}/users/all/`, axiosConfig);
};

export const PostCreateNewPlans = body => {
  return Axios.post(`${MyBaseUrl}/energy_plans/new/`, body, axiosConfig);
};

export const SaveEnergyPlan = body => {
  return Axios.post(
    `${MyBaseUrl}/prosumers/save_energy_plan/`,
    body,
    axiosConfig
  );
};

export const GetEnergyPlansInFacility = (facilityId: String) => {
  return Axios.get(`${MyBaseUrl}/energy_plans/all/${facilityId}/`, axiosConfig);
};

export const GetAllUsersEnergyPlans = () => {
  return Axios.get(`${MyBaseUrl}/energy_plans/all/`, axiosConfig);
};

export const PostNewEnergyPlans = body => {
  return Axios.post(`${MyBaseUrl}/energy_plans/new/`, body, axiosConfig);
};

export const GetNewInviteDetails = () => {
  return Axios.get(`${MyBaseUrl}/invite/new/`, axiosConfig);
};

export const GetUserFacilitiesList = () => {
  return Axios.get(`${MyBaseUrl}/facility/me/`, axiosConfig);
};

export const GetAllFacilitiesList = () => {
  return Axios.get(`${MyBaseUrl}/facility/all/`, axiosConfig);
};

export const GetUserDetails = () => {
  return Axios.get(`${MyBaseUrl}/users/me/`, axiosConfig);
};

export const GetUsersofRoleinFacility = (
  roleId: string,
  facilityId: string
) => {
  return Axios.get(
    `${MyBaseUrl}/users/by-role-facility/${roleId}/${facilityId}/`,
    axiosConfig
  );
};

export const GetProsumers = () => {
  return Axios.get(`${MyBaseUrl}/prosumers/all/`, axiosConfig);
};

export const GetMyProsumers = () => {
  return Axios.get(`${MyBaseUrl}/prosumers/me/`, axiosConfig);
};

export const GetUserProsumers = (groupId: string) => {
  return Axios.get(`${MyBaseUrl}/prosumers/user/${groupId}/`, axiosConfig);
};

export const GetProsumersTypes = () => {
  return Axios.get(`${MyBaseUrl}/prosumer-type/all/`, axiosConfig);
};

export const GetProsumerClasses = () => {
  return Axios.get(`${MyBaseUrl}/prosumer-class/all/`, axiosConfig);
};

export const CreateNewProsumerClass = body => {
  return Axios.post(`${MyBaseUrl}/prosumer-class/new/`, body, axiosConfig);
};

export const DeleteProsumerRequest = (
  prosumerName: string,
  facilityId: string
) => {
  return Axios.delete(
    `${MyBaseUrl}/prosumers/delete-prosumer/${prosumerName}/${facilityId}/`,
    axiosConfig
  );
};

export const RefreshProbeRequest = (probeId: string) => {
  return Axios.post(
    `${MyBaseUrl}/prosumers/refresh-probe/${probeId}/`,
    {},
    axiosConfig
  );
};

export const ProsumerTurnOffRequest = (prosumerId: string) => {
  return Axios.post(
    `${MyBaseUrl}/prosumer-switch/one-off/${prosumerId}/`,
    {},
    axiosConfig
  );
};

export const ProsumerTurnOnRequest = (prosumerId: string) => {
  return Axios.post(
    `${MyBaseUrl}/prosumer-switch/one-on/${prosumerId}/`,
    {},
    axiosConfig
  );
};

export const CreateNewProsumerType = body => {
  return Axios.post(`${MyBaseUrl}/prosumer-type/new/`, body, axiosConfig);
};

export const CreateNewProsumerGroup = body => {
  return Axios.post(`${MyBaseUrl}/prosumer-group/new/`, body, axiosConfig);
};

export const AddClientAdminToProsumerGroup = body => {
  return Axios.post(
    `${MyBaseUrl}/prosumer-group/add-client-admin/`,
    body,
    axiosConfig
  );
};

export const AddProsumerToProsumerGroup = body => {
  return Axios.post(
    `${MyBaseUrl}/prosumer-group/assign-prosumer/`,
    body,
    axiosConfig
  );
};

export const GetProsumerGroups = (facilityId: string) => {
  return Axios.get(
    `${MyBaseUrl}/prosumer-group/all/${facilityId}/`,
    axiosConfig
  );
};

export const registerNewProsumer = (body: object) => {
  return Axios.post(`${MyBaseUrl}/prosumers/new/`, body, axiosConfig);
};

export const GetMetersByType = (prosumerTypeUrlName: string) => {
  return Axios.get(`${MyBaseUrl}/${prosumerTypeUrlName}/all/`, axiosConfig);
};

// Mark for deletion
export const CreateNewProsumers = (
  prosumers: { name: string; type_id: string }[],
  probeId: string,
  facilityId: string
) => {
  const body = {
    prosumers: prosumers,
    probe_id: probeId,
    facility_id: facilityId
  };
  return Axios.post(`${MyBaseUrl}/prosumers/new/`, body, axiosConfig);
};

export const GetAccountBalance = () => {
  return Axios.get(`${MyBaseUrl}/account/my-account-balance/`, axiosConfig);
};

export const EnsureAccountActivation = body => {
  return Axios.post(
    `${MyBaseUrl}/account/ensure-activation/`,
    body,
    axiosConfig
  );
};

export const MyMeterInfo = () => {
  return Axios.get(`${MyBaseUrl}/prosumers/my-info/`, axiosConfig);
};

export const SearchForUsersRequest = (query: string) => {
  // return Axios.get(`${MyBaseUrl}/users/all/`, axiosConfig);
  return Axios.get(`${MyBaseUrl}/users/search/?query=${query}`, axiosConfig);
};

export const AuthyAuthentication = body => {
  console.log(MFA_TOKEN + "api_request");
  return Axios.post(`${MyBaseUrl}/authy/verify/`, body, axiosConfigMfa);
};

export const GetMyPinsRequest = () => {
  return Axios.get(`${MyBaseUrl}/pin/my-pins/`, axiosConfig);
};

export const GetValidatedPinsReference = () => {
  return Axios.get(`${MyBaseUrl}/pin/validated-references/`, axiosConfig);
};

export const CreateNewBuyOrderSelf = body => {
  return Axios.post(`${MyBaseUrl}/buy/self-new-order/`, body, axiosConfig);
};

export const CreateNewSellOrderSelf = body => {
  console.log(axiosConfig);
  return Axios.post(`${MyBaseUrl}/sell/self-new-order/`, body, axiosConfig);
};

export const CreateNewBuyOrderAdmin = body => {
  return Axios.post(`${MyBaseUrl}/buy/admin-new-order/`, body, axiosConfig);
};

export const CreateNewSellOrderAdmin = body => {
  console.log(axiosConfig);
  return Axios.post(`${MyBaseUrl}/sell/admin-new-order/`, body, axiosConfig);
};

export const GetAllBillingUnits = () => {
  return Axios.get(`${MyBaseUrl}/billing-unit/all/`, axiosConfig);
};

export const GetAllBillingMethods = () => {
  return Axios.get(`${MyBaseUrl}/billing-method/view-all/`, axiosConfig);
};

export const GetAllRenewFrequencies = () => {
  return Axios.get(`${MyBaseUrl}/renew-frequency/view-all/`, axiosConfig);
};

export const GetAllTimeSpanMeasures = () => {
  return Axios.get(`${MyBaseUrl}/time-span-measure/view-all/`, axiosConfig);
};

export const GetSellOrdersInSubnet = (subnetId: string) => {
  return Axios.get(`${MyBaseUrl}/sell/by-subnet/${subnetId}/`, axiosConfig);
};

export const GetSubnetsByFacility = (facilityId: string) => {
  return Axios.get(
    `${MyBaseUrl}/subnet/facility-has/${facilityId}/`,
    axiosConfig
  );
};

export const GetSellOrdersInSubnetByBillingUnit = (subnetId: string, billingUnitId: string) => {
  return Axios.get(`${MyBaseUrl}/sell/by-billing-subnet/${subnetId}/${billingUnitId}/`, axiosConfig);
}

export const Countries = () => {
  return Axios.get(`${countryUrl}`);
}

export const facilitiesInvite = () => {
  return Axios.get(`${MyBaseUrl}/facility/invite/`, axiosConfig)
}

export const NewFacilities = body => {
  return Axios.post(`${MyBaseUrl}/facility/new/`, body, axiosConfig)
}
