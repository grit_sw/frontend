import * as React from 'react';
import * as ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import './index.css';

import App from './App';
ReactDOM.render(
    <App />,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();

// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
// import ResetPassword from './views/ResetPassword/ResetPassword'
// ReactDOM.render(
//     <MuiThemeProvider> 
//         <ResetPassword/> 
//     </MuiThemeProvider>,
//     document.getElementById('root') as HTMLElement
// );
// registerServiceWorker();