const styles = {
	card: {
		position: 'relative',
		width: 190,
		height: 75
	},

	topLeft: {
		position: 'absolute',
		fontSize: 'small',
		top: 2,
		left: 5,
		fontWeight: 'bold'
	},

	topRight: {
		position: 'absolute',
		fontSize: 'small',
		top: 2,
		right: 5,
		width: 63.333,
		textOverflow: 'ellipsis',
		fontWeight: 'bold'
	},

	buttomLeft: {
		position: 'absolute',
		fontSize: 'smallest',
		bottom: 2,
		left: 5,
		width: 63.333,
		// textOverflow: 'ellipsis',
		fontWeight: 'bold'
	},

	buttomRight: {
		position: 'absolute',
		fontSize: 'small',
		bottom: 0,
		right: 5,
		width: 63.333,
		textOverflow: 'ellipsis',
		fontWeight: 'bold'
	},

	content: {
		position: 'relative',
	}
};

export default styles;
