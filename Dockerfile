# build environment
FROM node:10.8-alpine as node
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# ENV PATH /usr/src/app/node_modules/.bin:$PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
# COPY package*.json ./
RUN npm install
RUN npm install react-scripts@1.1.1 -g
RUN npm install react-event-listener@0.5.3
COPY package.json ./
RUN npm install
RUN npm install -g serve
ENV REACT_APP_BACKEND_SERVER=http://dos.grit.systems:5500
COPY . .
RUN npm run build

# stage 2
FROM nginx:1.13
COPY --from=node /usr/src/app/build /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
